title: Ensure Tor is strong for years to come: become a monthly donor
---
author: alsmith
---
pub_date: 2022-11-29
---
categories:

fundraising
---
summary: As a 501(c)(3) nonprofit, the Tor Project relies on external support to fight for your privacy online. One of the most impactful ways to power Tor is to become a [Defender of Privacy with a monthly donation](https://torproject.org/donate/donate-bp2-pbp).
---
body:

Tor is powered by community. Just as the privacy the Tor network provides is made possible by a decentralized network of volunteers running relays—the Tor Project is made possible by a wide variety of supporters.

As a 501(c)(3) nonprofit, the Tor Project relies on external support to fight for your privacy online. We will always build and offer free software. It’s part of our mission and our vision of a better internet. We don’t harvest or sell your data. We don’t charge you to use what we build. That means that everyone in the world can use Tor. It also means that if you have the ability, [making a monthly donation](https://torproject.org/donate/donate-bp2-pbp) to Tor ensures its availability for others.

By becoming a [Defender of Privacy with a monthly donation](https://torproject.org/donate/donate-bp2-pbp), you make it possible for Tor to plan for the years ahead with confidence. Consistent support ensures that the Tor Project is sustainable and stable—and every donation, no matter the amount, helps.

I recently asked our Defenders of Privacy with the longest-running monthly donations why they donate regularly to better understand why Tor matters to them, and why donating monthly is a priority. One anonymous supporter put it this way:

> (a) Privacy is near and dear to my heart. Living in Switzerland, that's maybe just a luxury or makes it a little harder for people to track me, but there are countries in this world where people are less fortunate. Where privacy can mean protection from oppression and/or prosecution.
> 
> (b) I'm also aware that projects like these take a lot of time and effort, often from very smart people who could make a lot of money working in a big corporation, who choose to do this. So, if I can help a little, I [will] gladly do that.
> 
> (c) I only do this for very few organizations, Mozilla being another one for example, also because they are providing me with a free product that works well and has a privacy focus, like you guys.

Beyond supporting our mission, when you [become a monthly donor](https://torproject.org/donate/donate-bp2-pbp) of any amount, you'll receive an exclusive [Defenders of Privacy patch](monthly-giving-patch.jpg) + gifts from the Tor Project throughout the year. **Plus, brand new Tor stickers will come to you in January if you become a donor today!**

Take the step to power privacy online for everyone: [become a Defender of Privacy with a monthly gift](https://torproject.org/donate/donate-bp2-pbp).
