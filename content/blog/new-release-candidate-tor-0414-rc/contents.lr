title: New release candidate: Tor 0.4.1.4-rc
---
pub_date: 2019-07-26
---
author: nickm
---
tags: release candidate
---
categories: releases
---
_html_body:

<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for 0.4.1.4-rc from <a href="https://www.torproject.org/download/tor/">the usual place on the website</a>. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely in the next month or so.</p>
<p>Remember, this is just a release candidate: you should only run this if you'd like to find and report bugs.</p>
<p>Tor 0.4.1.4-rc fixes a few bugs from previous versions of Tor, and updates to a new list of fallback directories. If no new bugs are found, the next release in the 0.4.1.x series should be stable.</p>
<h2>Changes in version 0.4.1.4-rc - 2019-07-25</h2>
<ul>
<li>Major bugfixes (circuit build, guard):
<ul>
<li>When considering upgrading circuits from "waiting for guard" to "open", always ignore circuits that are marked for close. Otherwise, we can end up in the situation where a subsystem is notified that a closing circuit has just opened, leading to undesirable behavior. Fixes bug <a href="https://bugs.torproject.org/30871">30871</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor features (continuous integration):
<ul>
<li>Our Travis configuration now uses Chutney to run some network integration tests automatically. Closes ticket <a href="https://bugs.torproject.org/29280">29280</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (fallback directory list):
<ul>
<li>Replace the 157 fallbacks originally introduced in Tor 0.3.5.6-rc in December 2018 (of which ~122 were still functional), with a list of 148 fallbacks (70 new, 78 existing, 79 removed) generated in June 2019. Closes ticket <a href="https://bugs.torproject.org/28795">28795</a>.</li>
</ul>
</li>
<li>Minor bugfixes (circuit padding):
<ul>
<li>On relays, properly check that a padding machine is absent before logging a warning about it being absent. Fixes bug <a href="https://bugs.torproject.org/30649">30649</a>; bugfix on 0.4.1.1-alpha.</li>
<li>Add two NULL checks in unreachable places to silence Coverity (CID 144729 and 1447291) and better future-proof ourselves. Fixes bug <a href="https://bugs.torproject.org/31024">31024</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (crash on exit):
<ul>
<li>Avoid a set of possible code paths that could try to use freed memory in routerlist_free() while Tor was exiting. Fixes bug <a href="https://bugs.torproject.org/31003">31003</a>; bugfix on 0.1.2.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Fix a conflict between the flag used for messaging-domain log messages, and the LD_NO_MOCK testing flag. Fixes bug <a href="https://bugs.torproject.org/31080">31080</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory leaks):
<ul>
<li>Fix a trivial memory leak when parsing an invalid value from a download schedule in the configuration. Fixes bug <a href="https://bugs.torproject.org/30894">30894</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Remove some dead code from circpad_machine_remove_token() to fix some Coverity warnings (CID 1447298). Fixes bug <a href="https://bugs.torproject.org/31027">31027</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
</ul>

