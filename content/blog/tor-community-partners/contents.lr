title: Supporting our community
---
author: isabela
---
pub_date: 2022-03-04
---
categories: partners
---
summary: Learn about Tor's community parters, how we're working together, and how you can support them too.
---
body:

The goal to help internet users privately access an uncensored web has united lots of different people from different places. Fostering this community is very important to us. That is why we decided to write this post about some of our community partners, how we are supporting them, and how you can support them too.

For example, the birth of Tor itself relied on community support. Our project was first sponsored by the [Electronic Frontier Foundation (EFF)](https://www.eff.org/). To return this support, we have helped our community in any way we can—we’re proud to have been the fiscal sponsor for [Library Freedom Project](https://libraryfreedom.org/) and to have supported [Open Observatory of Network Interference](https://ooni.org), once a team that was part of the Tor Project. Now both of these projects have moved on to become their own organizations, doing amazing work to build a cohort of librarian privacy advocates and to measure and report on internet censorship!

In the recent past, we’ve weathered some difficult times–but we’ve turned a corner and are in a sustainable and solid position. Part of the Tor Project’s recovery is a result of your support. Because you believe in this project and community, we have been able to invest in fundraising and operations staff that work together on something we affectionately call the “money machine.” This cross-organizational group handles the behind-the-scenes details of identifying, securing, and managing the resources that make Tor, and the Tor Project, possible.

With the “money machine” concept we’ve developed at the Tor Project—**we envisioned that our fundraising and operations infrastructure would benefit more than just the organization, that it would allow us to give back to our community as well.** We imagined that we would be more successful if we could fight for human rights and privacy online in an interconnected, interdependent way–as a community, together.

Over the last year, part of this vision has come to life!

We are happy to share that now the Tor Project has been able to earn grants that include partners, or “subgrantees,” that are paid to help Tor complete a project. **We would like to take the opportunity to highlight these partners, the work we’re doing together, and invite you to support them too.**

💜 [Guardian Project](https://guardianproject.info/)

Guardian Project creates easy to use secure apps, open-source software libraries, and customized solutions that can be used around the world by any person looking to protect their communications and personal data from unjust intrusion, interception, and monitoring.

We have been working alongside Guardian Project for a long time, and have several opportunities to improve Tor, Orbot, and Onion Browser in upcoming and ongoing projects. For example:

-   With Guardian Project and Tails, we’re working together to deliver trainings and conduct user research on Tor Browser, Orbot, Onion Browser, and Tails. ([Sponsor 30](https://gitlab.torproject.org/tpo/team/-/wikis/sponsors-2022#empowering-communities-in-the-global-south-to-bypass-censorship-s30))
    
-   Guardian Project is driving work to integrate Tor + Snowflake/obfs4 capabilities into mobile applications. ([Sponsor 96](https://gitlab.torproject.org/groups/tpo/-/milestones/24#tab-issues))
    
-   We’re working together to build region-specific outreach, distribution, and user support for the ecosystem of Tor apps for communities in China, Hong Kong, and Tibet. ([Sponsor 96](https://gitlab.torproject.org/groups/tpo/-/milestones/24#tab-issues))
    
-   We’re working together to develop our [“VPN-like” app](https://www.youtube.com/watch?v=mNhIjtXuVzk&t=1333s) for Android, based on Orbot. ([Sponsor 101](https://gitlab.torproject.org/groups/tpo/-/milestones/32#tab-issues))

💜 [Tails](https://tails.boum.org/)

Tails is a nonprofit organization whose mission is to empower people worldwide by developing and distributing an operating system that protects from surveillance and censorship. Tails makes digital security tools accessible to everyone, whenever they need it: activists, journalists, freedom fighters, and ultimately, you, whenever you need extra privacy in this digital world.

Like with Guardian Project, we’re working with our friends at Tails to deliver trainings and conduct user research on Tor Browser, Orbot, Onion Browser, and Tails. ([Sponsor 30](https://gitlab.torproject.org/tpo/team/-/wikis/sponsors-2022#empowering-communities-in-the-global-south-to-bypass-censorship-s30))

💜 [OnionShare](https://onionshare.org/)

OnionShare is an open source tool that lets you securely and anonymously share files, host websites, and chat with friends using the Tor network.

Micah Lee, developer of OnionShare, is improving automatic censorship detection and integrating Tor+Snowflake/obfs4 capability into OnionShare as part of our work to improve an ecosystem of Tor apps for censorship circumvention purposes. ([Sponsor 96](https://gitlab.torproject.org/groups/tpo/-/milestones/24#tab-issues))

💜 [The Calyx Institute](https://calyxinstitute.org/)

The Calyx Institute is a nonprofit education and research organization devoted to studying, testing and developing and implementing privacy technology and tools to promote free speech, free expression, civic engagement and privacy rights on the internet and in the mobile communications industry.

Like with OnionShare, the Calyx Institute is integrating Tor into CalyxOS as part of our work to improve an ecosystem of Tor apps for censorship circumvention purposes. ([Sponsor 96](https://gitlab.torproject.org/groups/tpo/-/milestones/24#tab-issues))

**We have also been able to give financial support to organizations that are always working by our side, like:**

💜 [LEAP](https://leap.se)

LEAP builds a simple, easy-to-use VPN and works with trusted service providers (like Riseup) to build and brand their VPN services.

💜 [Localization Lab](https://www.localizationlab.org/)

Localization Lab makes FLOSS technology accessible through collaboration with developers, organizations, end users, and communities in need and helps us make Tor Browser and Tor documentation available in as many languages as possible.

💜 [Riseup](https://riseup.net/)

Riseup provides online communication tools for people and groups working on liberatory social change. Riseup is a project to create democratic alternatives and practice self-determination by controlling our own secure means of communications.

We share this news because Tor is not created alone. We are part of a community. The same way we have been supported by you and other members of this community, we want to return that support by sharing the amazing skills from our “money machine” team. We hope to continue to serve our community this way.

Please join us in following these organizations, testing out the tools they offer, volunteering in their communities, and supporting them with regular donations!
