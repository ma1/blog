title: Tor projects win two Access Innovation Awards
---
pub_date: 2013-01-08
---
author: arma
---
tags:

internet censorship
trip report
---
categories:

circumvention
reports
---
_html_body:

<p>In December I attended the award ceremony for the <a href="https://www.accessnow.org/blog/2012/12/11/first-annual-access-innovation-awards-prize-winners-announced" rel="nofollow">2012 Access Innovation Awards</a>. Their finalists included three projects that Tor maintains or co-maintains: <a href="https://ooni.torproject.org/" rel="nofollow">OONI</a> (a framework for writing open network censorship measurement tests, and for making the results available in an open way; see its <a href="https://gitweb.torproject.org/ooni-probe.git" rel="nofollow">git repo</a>), <a href="https://crypto.stanford.edu/flashproxy/" rel="nofollow">Flash Proxy</a> (a creative way to let people run <a href="https://www.torproject.org/docs/bridges" rel="nofollow">Tor bridges</a> in their browser just by visiting a website; see its <a href="https://gitweb.torproject.org/flashproxy.git" rel="nofollow">git repo</a>), and <a href="https://www.eff.org/https-everywhere" rel="nofollow">HTTPS Everywhere</a> (a Firefox extension to force https connections for websites that support https but don't use it by default; see its <a href="https://gitweb.torproject.org/https-everywhere.git" rel="nofollow">git repo</a>). Of these, OONI and Flash Proxy ended up being winners in their respective categories.</p>

<p>(The Access Innovation Prize gave $100,000 across 5 categories to individuals, organizations and networks who submitted "the best actionable ideas on how to use information technology to promote and enable human rights and deliver social good.")</p>

<p>I'm happy that people are recognizing some of the <a href="https://www.torproject.org/getinvolved/volunteer#Projects" rel="nofollow">cool projects</a> that Tor works on. But more than that, it's interesting to watch how our projects are integrating into the broader community. The HTTPS Everywhere website is hosted by EFF, and the tool is co-developed by EFF and The Tor Project. The Flash Proxy submission was actually submitted by the New America Foundation's <a href="http://oti.newamerica.net/" rel="nofollow">OTI</a> group, where they plan to work on integrating the Flash Proxy badge into a Facebook app (don't worry, they're splitting the prize money with David Fifield, the main Flash Proxy developer). We (Tor) wouldn't be able to have this reach if we didn't have these other organizations working on our topics too.</p>

<p>More generally, the line around what counts as a Tor project has been getting blurrier over the years. We're a community based around <a href="http://en.wikipedia.org/wiki/Free_software" rel="nofollow">free</a> software and transparent design and development, and when we encounter somebody else who is "doing it right", we embrace them and offer whatever resources we can to help them succeed. One nice example is Nathan Freitas of <a href="https://guardianproject.info/" rel="nofollow">Guardian</a> — he started maintaining an Android version of Tor, and we liked how he was doing it, so we called him a Tor developer. When Nathan uses his Tor affiliation to get funders to listen to him about mobile security issues, everybody wins. Similarly, while David Fifield spends most of his time being a Stanford grad student, he's made such progress on Flash Proxy that we're paying a <a href="https://www.torproject.org/about/jobs-pluggabletransport" rel="nofollow">second Flash Proxy developer</a> to help him with <a href="https://lists.torproject.org/pipermail/tor-dev/2012-December/004254.html" rel="nofollow">Windows deployment</a>. It's great that OTI is working in this <a href="https://www.torproject.org/docs/pluggable-transports" rel="nofollow">pluggable transport</a> space too. There's no shortage of hard problems and development tasks to go around.</p>

<p>I'm glad I attended the awards evening. I confess that I was worried it would be full of media hype, with journalists lined up to write hasty and shallow articles. (I suppose I'm jaded by being followed around at hacker conferences by journalists with quotas and deadlines. But I was particularly worried this time because both OONI and Flash Proxy are young projects, and we'd be wisest to make some real progress before drawing mainstream media attention to them.) Instead, the attendees were a variety of enthusiastic, not-very-technical New York City residents. Most of them hadn't heard of Tor and had only a very general understanding of Internet privacy and censorship issues; so I had my work cut out for me in terms of advocacy and awareness-building. Highlights included conversations with people from <a href="http://www.cpj.org/" rel="nofollow">Committee to Protect Journalists</a>, <a href="http://www.hrw.org/" rel="nofollow">Human Rights Watch</a>, and several similar organizations — these are exactly the sort of orgs that needs the neutral censorship measurement results that OONI aims to provide.</p>

