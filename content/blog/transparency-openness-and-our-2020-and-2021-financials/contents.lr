title: Transparency, Openness, and Our 2020-2021 Financials
---
author: alsmith
---
pub_date: 2022-11-03
---
categories: reports
---
summary:
The Tor Project's 2020-2021 federal tax filings and audited financial statements are now available.
---
body:
Every year, as required by U.S. federal law for 501(c)(3) nonprofits, the Tor Project completes a Form 990, and as required by contractual obligations and state regulations, an independent audit of our financial statements. After completing standard audits for 2020-2021,* our [federal tax filings (Form 990)](https://www.torproject.org/static/findoc/2020-2021-TheTorProject-PublicDisclosureForm990.pdf?h=65316bf7) and [audited financial statements](https://www.torproject.org/static/findoc/2020-2021-TheTorProject-AuditedFinancialStatement.pdf?h=c7b5458f) are both now available. We upload [all of our tax documents](https://www.torproject.org/about/reports/) and publish a blog post about these documents in order to be transparent.

Transparency for a privacy project is not a contradiction: privacy is about choice, and we choose to be transparent in order to build trust and a stronger community. In this post, we aim to be very clear about where the Tor Project’s money comes from and what we do with it. This is how we operate in all aspects of our work: we show you all of our projects, in [source code](https://gitweb.torproject.org/), and in periodic project and team [reports](https://lists.torproject.org/pipermail/tor-project/), and in collaborations with [researchers](https://blog.torproject.org/tor-heart-pets-and-privacy-research-community) who help assess and improve Tor. Transparency also means being clear about our values, promises, and priorities as laid out in our [social contract](https://gitweb.torproject.org/community/policies.git/plain/social_contract.txt).

This year’s version of the financial transparency post is a bit different than past iterations: we hope that by expanding each subsection of this post and adding more detail, you will have an even better understanding of the Tor Project’s funding, and that you will be able to read the [audited financial statements and Form 990](https://torproject.org/about/reports) on your own should you have more questions.

_*Note: Our fiscal years run from July through June instead of following the calendar year. We made this change in 2017 so that our fundraising season (December) falls in the middle of our fiscal year, allowing us to better plan our budgets._

## Fiscal Year 2020-2021 Summary

The audit and tax filing forms we discuss in this post cover July 1, 2020 to June 30, 2021.

It’s no surprise that the Tor Project faced many uncertainties in our funding sources at the beginning of 2020 when the pandemic changed the world. Directly prior to the beginning of the 2020-2021 fiscal year, we saw a sharp decrease in individual donations. 

Despite this difficulty, throughout the end of 2020, you helped us break fundraising records with the [2020 Bug Smash Fund](https://blog.torproject.org/tors-bug-smash-fund-86k-raised) and the 2020 [year-end campaign](https://blog.torproject.org/use-a-mask-use-tor-thank-you/). Then, in the spring of 2021, we held an [auction of a generative art piece we called Dreaming at Dusk](https://foundation.app/torproject/dreaming-at-dusk-35855), created by artist Itzel Yard ([ixshells](https://foundation.app/ixshells)) and derived from the private key of the first onion service, Dusk. [PleasrDAO](https://pleasr.org/) won this auction and raised approximately $1.7M in unrestricted funds for the Tor Project.

We ended the fiscal year in a strong position, thanks to your support. Below, we’ll talk more about the details of the Revenue and Expenses throughout the fiscal year.


## Revenue & Support
The Tor Project’s **Revenue and Support** in fiscal year 2020-2021, as listed in the **audited financial statement**, was $7,825,375.

![Screenshot showing the Revenue and Support section of the Tor Project's 2020-2021 audited financial statements](revenue-and-support.png)

In our **Form 990**, our **Revenue** is listed as $7,412,081. 

![Screenshot showing the Revenue and Support section of the Tor Project's 2020-2021 IRS Form 990](990-revenue.png)

**Why is the “revenue” category different on the Form 990 and the audited financials?** This is not an error. The different revenue totals reflect differences in rules about how you must report tax revenue to the IRS and how you must report revenue in audited financial statements. The audited financial statements _includes_ and _excludes_ revenue categories that the Form 990 does not, per tax reporting rules: audited financial statements **include** in-kind contributions ($797,597), and **excludes** a (now forgiven) Paycheck Protection Program loan from the U.S. Small Business Administration ($384,303). 

A brief aside: let’s talk about in-kind contributions--the value of which is included in the audited financial statements, and excluded in the Form 990--and how we derive that value. In-kind contributions are donated services or goods, like translation completed by volunteers, website hosting, donated hardware, and contributed patches. This year, we counted $797,597 in donated services: that’s an estimated thousands of hours of software development, 1,364,631 words translated, and cloud hosting services for 23 servers. Right now, this estimation does not include relay operator time and cost--but it should. We are working to better estimate both the hours of software development and how to represent the contributions made by relay operators. Stay tuned for the coming years! Clearly, Tor would not be possible without you. Thank you.

For the purposes of the following subsections of this blog post, **we’ll be looking at the Revenue total following the Form 990**, and breaking this total into the following categories:

* U.S. government (38.17% of total revenue)
* Individual donations (36.22% of total revenue)
* Private foundations (16.15% of total revenue)
* Non-U.S. government (5.07% of total revenue)
* Corporate (2.89% of total revenue)
* Other (1.51% of total revenue)

![Pie chart showing segments of revenue for the Tor Project](chart-revenue.png)

### U.S. Government Support
During this fiscal year, about 38% of our revenue came from U.S. government funds. This is 15% down from the previous fiscal year, and in line with our long-term goal to diversify our funding in order to rely less on one single source. Over time the percentage of funds from different sources will fluctuate--in the current fiscal year, this percentage will go up based on the timing of new projects. 

We get a lot of questions (and see a lot of [FUD](https://en.wikipedia.org/wiki/Fear,_uncertainty_and_doubt)) about how the U.S. government funds the Tor Project, so we want to make this as clear as possible and show you where to find this information in the future in these publicly-available documents.

Let’s talk specifically about which parts of the U.S. government support Tor, and what kind of projects they fund. Below, you can see a screenshot from the Tor Project’s fiscal year 2020-2021 Form 990 on page 39, where we’ve listed all of our U.S. government funders. You will find text like this in our recent Form 990s: 

![Screenshot of the Tor Project's Form 990 Funding Sources section](funding-sources.png)

Now, we’ll break down which projects are funded by each entity and link you to places in [GitLab](https://gitlab.torproject.org) where we organize the work associated with this funding.

**[U.S. State Department Bureau of Democracy, Human Rights, and Labor](https://www.state.gov/about-us-bureau-of-democracy-human-rights-and-labor/) ($1,532,843)**
* Project: [Empowering Communities in the Global South to Bypass Censorship](https://gitlab.torproject.org/groups/tpo/-/issues/?scope=all&state=opened&label_name%5B%5D=Sponsor%2030)
    * _Description_: This ongoing project’s goal is to empower human rights defenders in the Global South by improving censorship event detection and reporting, ensuring users have the best options for their needs to bypass censorship, and informing human rights defenders when censorship is happening and how to bypass it. [Guardian Project and Tails are our subgrantees on this project](https://blog.torproject.org/tor-community-partners/).
* Project: [Making the Tor network faster & more reliable for users in Internet-repressive places](https://gitlab.torproject.org/tpo/team/-/wikis/sponsors-2022#making-the-tor-network-faster-more-reliable-for-users-in-internet-repressive-places-s61)
    * _Description_: This ongoing project’s objectives are to streamline the tuning of the network; deploy smarter methods for balancing traffic; evaluate and implement promising performance and scalability research; and to proactively detect, diagnose, and resolve user-facing performance issues.
* Some of this funding passed through the Tor Project to [Open Observatory of Network Interference](https://ooni.org/).

**[National Science Foundation](https://www.nsf.gov/) + Georgetown University ($224,617)**
* Project: [Expanding Research Frontiers with a Next-Generation Anonymous Communication Experimentation (ACE) Framework](https://gitlab.torproject.org/legacy/trac/-/wikis/org/sponsors/Sponsor38)
    * _Description:_ This ongoing project’s goal is to develop a scalable and mature deterministic network simulator, capable of quickly and accurately simulating large networks such as Tor. This project builds on the[ Shadow Simulator](https://shadow.github.io/).

**[Institute of Museum and Library Science](https://www.imls.gov/) + New York University ($96,569)**
* This funding passed through the Tor Project to [Library Freedom Project](https://libraryfreedom.org/) to deliver the Library Freedom Institute. 

**[Defense Advanced Research Projects Agency](https://www.darpa.mil/) + Georgetown University ($570,497)**
* Project: [Reliable Anonymous Communication Evading Censors and Repressors (RACECAR)](https://gitlab.torproject.org/groups/tpo/-/milestones/10)
    * _Description_: This ongoing project’s goal is to understand how to obfuscate communication in the presence of an adversary that controls the entire network,  by hiding all communications inside traffic generated by common applications.

**[U.S. Small Business Administration](https://www.sba.gov/) ($384,303)**
* We received a (now forgiven) loan from the U.S. Small Business Administration through the Paycheck Protection Program (PPP). PPP loans were designed to provide a direct incentive for small businesses to keep their workers on payroll during the first waves of economic impact caused by the COVID-19 pandemic. As of March 18, 2021, [the loan has been forgiven](https://projects.propublica.org/coronavirus/bailouts/loans/the-tor-project-inc-7451627200).

**[National Institutes of Health](https://www.nih.gov/) + University of Pittsburgh ($20,421)**
* This funding passed through the Tor Project to [Library Freedom Project](https://libraryfreedom.org/) to deliver a virtual health literacy and privacy series of four webinars for public library workers.

For even more about how government funding works for the Tor Project, consider reading our [previous financial transparency posts](https://blog.torproject.org/category/tags/form-990), as well as [Roger’s thorough comments](https://blog.torproject.org/transparency-openness-and-our-2014-financials#comment-151396) on these posts.


### Individual Donations
The next largest category of contributions at 36% of the total revenue was individual donors. In the 2020-2021 fiscal year, supporters gave $2,684,390 to the Tor Project in the form of one-time gifts and monthly donations. These gifts came in all different forms (including ten different cryptocurrencies, which are then converted to USD), and come together to equal our greatest individual fundraising year in our history. 

Individual contributions come in many forms: some people donate $5 to the Tor Project one time, some donate $100 every month, and some make large gifts annually. The common thread is that individual donations are [unrestricted funds](https://www.cof.org/content/glossary-philanthropic-terms#u), and a very important kind of support. Unrestricted funds allow us to respond to censorship events, develop our tools in a more agile way, and ensure we have reserves to keep Tor strong in case of emergencies.

One reason this category is much higher than previous years is that it includes the results of an NFT auction we held in mid-May 2021. Specifically, we commissioned artist Itzel Yard ([ixshells](https://foundation.app/ixshells)) to create[ a generative art piece we called Dreaming at Dusk](https://en.wikipedia.org/wiki/Non-fungible_token) derived from the private key of the first onion service, Dusk. We then held an[ auction](https://foundation.app/torproject/dreaming-at-dusk-35855) of the piece on[ Foundation](https://foundation.app). This auction resulted in a final bid of 500 Ethereum (ETH), roughly $2M USD at the time of the auction, with the proceeds going towards our work to improve and promote Tor. We also donated a portion of the proceeds to the Munduruku Wakoborũn Women's Association, a grassroots indigenous organization in Pará, Brazil.

### Private Foundations + Corporations
Of the revenue in fiscal year 2020-2021, about 16% came from foundations and other nonprofits, and then another 3% from corporations. 

Some of these contributions are in the form of restricted grants, which means we pick a project that is on our roadmap, we propose it to a funder, they agree that this project is important, and we are funded to complete these projects. Some examples in this category include [Zcash Community Grants](https://zcashcommunitygrants.org/)’ support of our first year of [Arti](https://blog.torproject.org/arti_100_released/), [Digital Defenders](https://www.digitaldefenders.org/)’ support to write Onion Guides for easily setting up onion services (see the results of this project in [English](http://xmrhfasfg5suueegrnc4gsgyi2tyclcy5oz7f5drnrodmdtob6t2ioyd.onion/static/images/outreach/print/onion-guide-fanzine-EN.pdf) / [Spanish](https://community.torproject.org/static/images/outreach/print/onion-guide-fanzine-ES.pdf) / [Portuguese](https://community.torproject.org/static/images/outreach/print/onion-guide-fanzine-PT_BR.pdf)), and the [Bertha Foundation](https://berthafoundation.org/story/bertha-challenge-2021-activist-fellows/)’s support for a fellow within the Tor Project who worked with environmental organizations and climate and Indigenous activists to strengthen their digital security.

Also in this category are [unrestricted funds](https://www.cof.org/content/glossary-philanthropic-terms#u), like support from [Media Democracy Fund](https://mediademocracyfund.org/), [Craig Newmark Philanthropies](https://craignewmarkphilanthropies.org/), and [Ford Foundation](https://www.fordfoundation.org/). These unrestricted funds are not tied to a specific project.

Unrestricted funds also include contributions from corporations, and is where you will find [membership dues](https://torproject.org/about/membership) from our members. In the 2020-2021 fiscal year, you’ll see contributions from our members during this time. We haven’t listed every single entity you will see in our Form 990 in this blog post, but we hope you have a better understanding of what you might find in the Form 990. 

## Non-U.S. Governments
Sida is a government agency working on behalf of the Swedish parliament and government, with the mission to reduce poverty in the world. Sida supports the Tor Project so that we can carry out user research, localization, user support, usability improvements, training for communities, and training for trainers. This support is how we are able to [meet our users and to carry out the user experience research we do to improve all Tor tools](https://blog.torproject.org/reaching-people-where-they-are/).

## Other
$111,919 of our revenue is from and for the Privacy Enhancing Technologies Symposium. Tor supports the yearly PETS conference by providing structure and financial continuity.

## Expenses
OK, we’ve told you how we get funding (and which documents to look at to learn more). Now what do we do with that money? Based on the Form 990, our expenses were $3,987,743 in fiscal year 2020-2021.

![Screenshot showing the Tor Project's Form 990 expenses](990-expenses.png)

Like I discussed in the Revenue section above, the expenses listed in our Form 990 and audited financial statements are different. Based on the audited financial statements, our expenses (page 4) were $4,782,017. 

![Screenshot showing the Tor Project's audited financial statements expenses](functional-expenses.png)

**Why the difference?** The Form 990 does not include interest we’ve earned or the in-kind contributions and the audited financial statements does. For the rest of this blog post, as above, we’ll be looking at the Expenses total following the Form 990.

We break our expenses into three main categories: 
* **Administration**: costs associated with organizational administration, like salary for our Executive Director, office supplies, business insurance costs;
* **Fundraising**: costs associated with the fundraising program, like salary for fundraising staff, tools we use for fundraising, bank fees, postal mail supplies, swag; and 
* **Program services**: costs associated with making Tor and supporting the people who use it, including application, network, UX, metrics, and community staff salaries; contractor salaries; and IT costs. If you want to learn more about the work considered “Program services,” you should read our [list of sponsored projects](tpo/team/-/wikis/sponsors-2022) on our wiki.

In the 2020-2021 fiscal year, **87.2% of our expenses were associated with program services**. That means that a very significant portion of our budget goes directly into building Tor and making it better. Next comes fundraising at 7.3% and administration at 5.4%. 

![Pie chart showing the breakdown of each category of expenses](chart-expenses.png)

Ultimately, like [Roger has written in many past versions of this blog post](https://blog.torproject.org/author/arma/), it’s very important to remember the big picture: Tor's budget is modest considering  global impact. And it’s also critical to remember that our annual revenue is utterly dwarfed by what our adversaries spend to make the world a more dangerous and less free place.

In closing, we are extremely grateful for all of our donors and supporters. You make this work possible, and we hope this expanded version of our financial transparency post sheds more light on how the Tor Project raises money and how we spend it. Remember, that beyond [making a donation](https://donate.torproject.org), there are other ways to get involved, including [volunteering](https://community.torproject.org/) and [running a Tor relay](https://community.torproject.org/relay/)!
