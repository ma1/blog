title: July 2009 Progress Report
---
pub_date: 2009-08-10
---
author: phobos
---
tags:

progress report
anonymity advocacy
tor browser bundle
bug fixes
security fixes
anonymity fixes
stable release
---
categories:

advocacy
applications
releases
reports
---
_html_body:

<p><strong>New releases</strong></p>

<p>On July 8th, we released <a href="https://blog.torproject.org/blog/vidalia-0115-released" rel="nofollow">Vidalia 0.1.15.</a>.</p>

<p>On July 8th, we updated the Tor 0.2.0.35-stable bundles with the new Vidalia to fix an ssl issue and the Firefox Torbutton extension installation for OS X users.</p>

<p>On July 8th, we released <a href="https://blog.torproject.org/blog/tor-02117rc-released" rel="nofollow">Tor 0.2.1.17-rc</a>.</p>

<p><a href="https://blog.torproject.org/blog/tor-browser-bundle-123-and-124-released" rel="nofollow">Tor Browser Bundle 1.2.3</a> was released on July 8, 2009.<br />
<a href="https://blog.torproject.org/blog/tor-browser-bundle-123-and-124-released" rel="nofollow">TBB 1.2.3</a> was replaced by 1.2.4 on July 11, 2009<br />
<a href="https://blog.torproject.org/blog/tor-browser-bundle-125-and-126-released" rel="nofollow">TBB 1.2.5</a> was released on July 25th.  It solely included an update to Tor 0.2.1.18 .<br />
<a href="https://blog.torproject.org/blog/tor-browser-bundle-125-and-126-released" rel="nofollow">TBB 1.2.6</a> was released on July 28th.  It solely included an update to Tor 0.2.1.19.</p>

<p>On July 24th, we released <a href="https://blog.torproject.org/blog/tor-02118-and-02119-released-stable" rel="nofollow">Tor 0.2.1.18</a>.  </p>

<p>On July 28th, we released <a href="https://blog.torproject.org/blog/tor-02118-and-02119-released-stable" rel="nofollow">Tor 0.2.1.19</a>.  </p>

<p><strong>Make Tor a better tool for users in censored countries</strong></p>

<p>Tor 0.2.1.18 is our new stable. That is, this is the first stable release<br />
of the 0.2.1.x branch. The 0.2.0.x branch went stable in July of 2008.<br />
From the 0.2.1.18 release:</p>

<p>If the bridge config line doesn't specify a port, assume 443.<br />
This makes bridge lines a bit smaller and easier for users to<br />
understand. </p>

<p>If we're using bridges and our network goes away, be more willing<br />
to forgive our bridges and try again when we get an application<br />
request. </p>

<p><strong>Architecture and technical design docs for Tor enhancements related to blocking-resistance.</strong></p>

<p>Proposal 166 details four steps we're taking to safely collect data<br />
about Tor's network performance and network usage: 1) directory client<br />
counts by country, 2) entry guard client counts by country, 3) relay<br />
cell statistics, and 4) exit traffic by port and volume.<br />
<a href="https://git.torproject.org/checkout/tor/master/doc/spec/proposals/166-statistics-extra-info-docs.txt" rel="nofollow">https://git.torproject.org/checkout/tor/master/doc/spec/proposals/166-s…</a></p>

<p><strong>Hide Tor's network signature</strong></p>

<p>Part of the reason why Tor might be especially slow in Iran could<br />
be that they're doing deep packet inspection (DPI) to throttle SSL<br />
connections. Tor's strategy of looking like SSL might turn out to be a<br />
bad move in this case. It's hard to tell whether the SSL throttling is<br />
actually happening, of course, because we get plenty of mixed information<br />
from our sources in Iran. But if it *is* happening, we should start<br />
investigating traffic obfuscation approaches that a) don't look like SSL,<br />
but b) don't look recognizably like any other protocol.</p>

<p>Some other Iran circumvention developers have come up with a patch to<br />
obfuscate ssh traffic:<br />
<a href="http://github.com/brl/obfuscated-openssh/tree/master" rel="nofollow">http://github.com/brl/obfuscated-openssh/tree/master</a><br />
<a href="http://c-skills.blogspot.com/2008/12/sshv2-trickery.html" rel="nofollow">http://c-skills.blogspot.com/2008/12/sshv2-trickery.html</a></p>

<p>Sometime soon we should start looking at designs to super-encrypt the<br />
Tor link traffic in this way.</p>

<p><strong>Grow the Tor network and user base. Outreach</strong></p>

<p>On July 1st, Andrew gave a detailed Tor talk at the National Cyber Forensics and Training Alliance.  Andrew's blog about the event is at <a href="https://blog.torproject.org/blog/visit-ncfta" rel="nofollow">https://blog.torproject.org/blog/visit-ncfta</a>.</p>

<p>On July 7th, Andrew was a panelist for the CIMA/NED discussion on Iran and the Role of New Media, <a href="http://cima.ned.org/events/new-media-in-iran.html" rel="nofollow">http://cima.ned.org/events/new-media-in-iran.html</a>.  Andrew's blog about the event  is at <a href="https://blog.torproject.org/blog/cimaned-panel-iran-and-new-media" rel="nofollow">https://blog.torproject.org/blog/cimaned-panel-iran-and-new-media</a>.</p>

<p>On July 15th, Andrew presented Tor at Webinno22, <a href="http://www.webinnovatorsgroup.com/2009/07/06/the-webinno22-demo-companies/" rel="nofollow">http://www.webinnovatorsgroup.com/2009/07/06/the-webinno22-demo-compani…</a>.  Further discussions about online privacy startups and business deals with various investors and their seed companies are continuing since this event.</p>

<p>More press interviews and articles:</p>

<p>Iran activists work to elude crackdown on Internet, <a href="http://www.google.com/hostednews/ap/article/ALeqM5hTf-p6Iy3sWHK8BRR58npGosLC3AD99L01QO0" rel="nofollow">http://www.google.com/hostednews/ap/article/ALeqM5hTf-p6Iy3sWHK8BRR58np…</a> </p>

<p><a href="http://blog.taragana.com/n/iran-government-builds-internet-walls-but-activists-still-slip-around-in-political-turmoil-119968/" rel="nofollow">http://blog.taragana.com/n/iran-government-builds-internet-walls-but-ac…</a> </p>

<p>Twitter and Facebook Help Protestors Connect, <a href="http://www.outloud.com/2009/issue96/protest.html" rel="nofollow">http://www.outloud.com/2009/issue96/protest.html</a></p>

<p>US set to hike aid aimed at Iranians, <a href="http://www.boston.com/news/nation/washington/articles/2009/07/26/us_to_increase_funding_for_hackivists_aiding_iranians/" rel="nofollow">http://www.boston.com/news/nation/washington/articles/2009/07/26/us_to_…</a> </p>

<p>Senate OKs funds to thwart Iran Web censors , <a href="http://www.washingtontimes.com/news/2009/jul/26/senate-help-iran-dodge-internet-censorship/" rel="nofollow">http://www.washingtontimes.com/news/2009/jul/26/senate-help-iran-dodge-…</a></p>

<p>We wrote a follow-up blog post about the number of people using Tor<br />
from Iran and China in June:<br />
<a href="https://blog.torproject.org/blog/measuring-tor-and-iran-part-two" rel="nofollow">https://blog.torproject.org/blog/measuring-tor-and-iran-part-two</a></p>

<p>On July 1-5, Roger, Jake, Mike, and Damian attended Toorcamp in rural<br />
Washington State. Roger did a talk on current attacks and vulnerabilities<br />
in Tor.<br />
<a href="http://www.toorcamp.org/content/B4" rel="nofollow">http://www.toorcamp.org/content/B4</a></p>

<p>On July 21-23, Roger attended a workshop in DC at the National Academy of<br />
Sciences. The workshop focused on the combination of Usability, Privacy,<br />
and Security, and where future funding should concentrate.</p>

<p>On July 31, Roger gave a Defcon talk on the current state of Tor's<br />
performance challenges and how we're addressing them:<br />
<a href="http://defcon.org/html/defcon-17/dc-17-speakers.html#Dingledine" rel="nofollow">http://defcon.org/html/defcon-17/dc-17-speakers.html#Dingledine</a><br />
<a href="http://freehaven.net/~arma/slides-dc09.pdf" rel="nofollow">http://freehaven.net/~arma/slides-dc09.pdf</a></p>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong></p>

<p><a href="https://blog.torproject.org/blog/tor-browser-bundle-123-and-124-released" rel="nofollow">Tor Browser Bundle 1.2.3</a> was released on July 8, 2009.<br />
<a href="https://blog.torproject.org/blog/tor-browser-bundle-123-and-124-released" rel="nofollow">TBB 1.2.3</a> was replaced by 1.2.4 on July 11, 2009<br />
<a href="https://blog.torproject.org/blog/tor-browser-bundle-125-and-126-released" rel="nofollow">TBB 1.2.5</a> was released on July 25th.  It solely included an update to Tor 0.2.1.18 .<br />
<a href="https://blog.torproject.org/blog/tor-browser-bundle-125-and-126-released" rel="nofollow">TBB 1.2.6</a> was released on July 28th.  It solely included an update to Tor 0.2.1.19.</p>

<p>Upgraded many programs in Incognito to address security concerns and general bugfixes.</p>

<p><strong>Bridges</strong></p>

<p>Updated geoip database.  From the 0.2.1.18 release:</p>

<p>If the bridge config line doesn't specify a port, assume 443.<br />
This makes bridge lines a bit smaller and easier for users to<br />
understand. </p>

<p>If we're using bridges and our network goes away, be more willing<br />
to forgive our bridges and try again when we get an application<br />
request. </p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong></p>

<p>From the 0.2.1.18 release:<br />
Network status consensus documents and votes now contain bandwidth<br />
information for each relay. Clients use the bandwidth values<br />
in the consensus, rather than the bandwidth values in each<br />
relay descriptor. This approach opens the door to more accurate<br />
bandwidth estimates once the directory authorities start doing<br />
active measurements. Implements part of proposal 141. </p>

<p>When building a consensus, do not include routers that are down.<br />
This cuts down 30% to 40% on consensus size. Implements proposal<br />
138. </p>

<p>Authorities now vote for the Stable flag for any router whose<br />
weighted mean time between failure (MTBF) is at least 5 days, regardless of the mean MTBF. </p>

<p>The main 2009 remaining performance changes are, in order of importance:<br />
- Get the bwauthority scripts into place so authorities are voting on<br />
  more accurate bandwidths.<br />
- Write a proposal for capping the circuit window much lower, and<br />
  implement it, and backport it to 0.2.1.x.<br />
- Proposal 151: Mike's plan to track circuit build times and give up on<br />
  the slow ones.<br />
- Write a proposal for refilling our bandwidth buckets intra-second.<br />
  Consider deploying in 0.2.2.x.<br />
- Figure out what we can do for a less fair round-robin between active<br />
  circuits. My intuition is heading towards "we don't know what effect<br />
  each possible change will make, and our other changes are going to<br />
  have big effects, so we shouldn't deploy anything here quite yet."<br />
- Get enough authorities upgraded that our bug 969 fixes ("voting wrong<br />
  on wfu and mtbf") take effect.</p>

<p><strong>More reliable (e.g. split) download mechanism.</strong></p>

<p>We have a new Volunteer, Jon, working on maintaining and expanding the list of tor mirrors.  Jon has contacted all mirror maintainers and updated the mirrors list.  Three were removed, two added, and seven updated with new information.  There are 39 active mirrors.</p>

<p><strong>Translations</strong></p>

<p>10 Polish website updates<br />
7 French website updates<br />
1 Chinese website updates<br />
German torbutton translations updated<br />
Finnish torbutton translations updated<br />
Generate translation infrastructure for our email auto-responder.<br />
Ukrainian torbutton translation started<br />
Start of a Thai torbutton translation<br />
Spanish torbutton translation<br />
Ukrainian check.torproject.org translation<br />
Thai check.torproject.org translation</p>

<p>Our Google Summer of Code student, Runa, created a set of scripts to allow translators to translate our website content through the translation web portal.  This will greatly simplify the process used to translate the website.</p>

---
_comments:

<a id="comment-2025"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2025" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2009</p>
    </div>
    <a href="#comment-2025">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2025" class="permalink" rel="bookmark">state 0.2.1.19</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since 0.2.1.19 in the state-file a lot of 'EntryGuard' entrys gets<br />
listed and unlisted. Additional these unlisted servers are NOT  EntryGuard's.<br />
Why??</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2026"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2026" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2009</p>
    </div>
    <a href="#comment-2026">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2026" class="permalink" rel="bookmark">&quot;using anyway&quot; ?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"[warn]  Requested exit node ''xxxxx" is in ExcludeNodes or ExcludeExitNodes.. using anyway"</p>
<p>Why "using anyway" ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2027"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2027" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 11, 2009</p>
    </div>
    <a href="#comment-2027">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2027" class="permalink" rel="bookmark">My guess is that its not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My guess is that its not used for circuits of General purpose.<br />
So its relatively safe.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2112"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2112" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2009</p>
    </div>
    <a href="#comment-2112">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2112" class="permalink" rel="bookmark">Could you perhaps give an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could you perhaps give an update on when Tor will be available for Android<br />
(and perhaps other mobile platforms)?<br />
I could only find a passing mention in the January report,<br />
yet it seems an overwhelmingly important step.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2132"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2132" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2112" class="permalink" rel="bookmark">Could you perhaps give an</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2132">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2132" class="permalink" rel="bookmark">Tor on Android</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can compile tor on Android right now (same with iphone and winCE for that matter).  However, there still needs to be research into how safe it is to use Tor on a phone.  The phone environments and operating systems are sufficiently different from laptop/desktop/server operating systems that simply porting isn't so easy.  Getting Tor into the Android Market, or Apple App Store is an entirely different challenge.</p>
<p>Yes Tor may run on your phone now.  However, if your applications don't support SOCKS5, or leak data all over the place, you're no safer than before.</p>
<p>Getting Tor fully supported on Android, iPhone, and WinCE is on the roadmap for the next 3 years.  As this is an un-funded project, it's being done by volunteers with free time, so it'll get done when it's done.</p>
<p>As phones become more important, I'm concerned that my cell provider can see, record, filter/censor all of my Internet traffic on the phone.  More and more people use their phone heavily, some as a near replacement for a netbook or laptop.  I agree that this is important, but right now it's a volunteer effort.  Feel free to join us.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2134"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2134" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 16, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-2134">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2134" class="permalink" rel="bookmark">Thanks for the quick answer</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the quick answer - not being talented enough, I'll be patient, not rush my smartphone purchase, and consider a donation :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2516"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2516" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Alastair Beresford (not verified)</span> said:</p>
      <p class="date-time">September 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-2516">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2516" class="permalink" rel="bookmark">Tor now available on Android Marketplace</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We've recently released a pure Java implementation of Tor for the Android which is now available from the Android Marketplace. It makes extensive use of the OnionCoffee Tor implementation and could undoubtedly do with further improvements; we've made the source code available for all under GPL v2 and are happy to accept updates. More info at:</p>
<p><a href="http://www.cl.cam.ac.uk/research/dtg/android/tor/" rel="nofollow">http://www.cl.cam.ac.uk/research/dtg/android/tor/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2531"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2531" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2516" class="permalink" rel="bookmark">Tor now available on Android Marketplace</a> by <span>Alastair Beresford (not verified)</span></p>
    <a href="#comment-2531">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2531" class="permalink" rel="bookmark">re: tor on android</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>onioncoffee is a research project that is abandoned.  Consider it a proof of concept more than anything. One should not rely upon it for actual anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-2126"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2126" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2009</p>
    </div>
    <a href="#comment-2126">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2126" class="permalink" rel="bookmark">EntryGuard ?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>#Since 0.2.1.19 in the state-file a lot of 'EntryGuard' entrys gets<br />
#listed and unlisted. Additional these unlisted servers are NOT #EntryGuard's.<br />
#Why??</p>
<p>Thats the difference between "state"-file 2.0.35 and 0.2.1.19. The same like at my pc.<br />
i dont know the reason for.<br />
Whats the reason for?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3114"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3114" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hii (not verified)</span> said:</p>
      <p class="date-time">November 13, 2009</p>
    </div>
    <a href="#comment-3114">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3114" class="permalink" rel="bookmark">hmm</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you, you answered the question I have been searching for which was whether or not to place keywords when blog commenting. <a href="http://www.hayda.net" rel="nofollow">mirc</a> . <a href="http://www.hayda.net" rel="nofollow">chat</a> . <a href="http://www.hayda.net" rel="nofollow">cinsel sohbet</a>. <a href="http://www.hayda.net" rel="nofollow">cinsellik sohbet </a> . <a href="http://www.hayda.net/" rel="nofollow">http://www.hayda.net/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
