title: TorBirdy 0.2.2 is released
---
pub_date: 2017-04-03
---
author: sukhbir
---
tags:

TorBirdy
Thunderbird
icedove
email
---
_html_body:

<p>We are pleased to announce the eighth beta release of TorBirdy: TorBirdy 0.2.2. This release adds support for Thunderbird 52 and also features improved security configuration settings for Thunderbird. All users are encouraged to update.</p>

<p>If you are using TorBirdy for the first time, visit the <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy" rel="nofollow">wiki</a> to get started.</p>

<p>There are currently no known leaks in TorBirdy but please note that we are still in beta, so the usual caveats apply.</p>

<p>Here is the complete changelog since v0.2.1:</p>

<blockquote><p>
    0.2.2, 03 April 2017<br />
 * Bug <a href="https://bugs.torproject.org/20751" rel="nofollow">20751</a>: Enforce stronger ciphers in TorBirdy<br />
 * Bug <a href="https://bugs.torproject.org/6958" rel="nofollow">6958</a>, <a href="https://bugs.torproject.org/16935" rel="nofollow">16935</a>, <a href="https://bugs.torproject.org/19971" rel="nofollow">19971</a>: Add support for already torified keyserver<br />
 communication using modern GnuPG<br />
 * The minimum supported Thunderbird version is 45.0 and the maximum is 52.*<br />
 * Update default keyserver to OnionBalance <a href="https://sks-keyservers.net/overview-of-pools.php" rel="nofollow">hidden service pool</a>
</p></blockquote>

<p>We offer two ways of installing TorBirdy: by visiting our <a href="https://dist.torproject.org/torbirdy/torbirdy-0.2.2.xpi" rel="nofollow">website</a> (<a href="https://dist.torproject.org/torbirdy/torbirdy-0.2.2.xpi.asc" rel="nofollow">GPG signature</a>; signed by [geshifilter-code]&lt;a href="<a href="https://www.torproject.org/docs/signing-keys.html.en&quot;&gt;0xB01C8B006DA77FAA&lt;/a&gt;[/geshifilter-code" rel="nofollow">https://www.torproject.org/docs/signing-keys.html.en"&gt;0xB01C8B006DA77FA…</a>]) or by visiting the <a href="https://addons.mozilla.org/en-us/thunderbird/addon/torbirdy/" rel="nofollow">Mozilla Add-ons page</a> for TorBirdy. </p>

<p>Please note that there may be a delay -- which can range from a few hours to days -- before the extension is reviewed by Mozilla and updated on the Add-ons page. </p>

<p>The <a href="https://packages.debian.org/sid/xul-ext-torbirdy" rel="nofollow">TorBirdy package</a> for Debian GNU/Linux will be uploaded shortly by Ulrike Uhlig.</p>

---
_comments:

<a id="comment-255797"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-255797" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 03, 2017</p>
    </div>
    <a href="#comment-255797">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-255797" class="permalink" rel="bookmark">Great to see that TorBirdy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great to see that TorBirdy is still taken care of, thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-255883"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-255883" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 03, 2017</p>
    </div>
    <a href="#comment-255883">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-255883" class="permalink" rel="bookmark">Thank you!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-256218"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256218" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 03, 2017</p>
    </div>
    <a href="#comment-256218">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256218" class="permalink" rel="bookmark">shouldn&#039;t this be part of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>shouldn't this be part of Thunderbird and Enigmail ,aka upstream instead ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-256267"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256267" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">April 03, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-256218" class="permalink" rel="bookmark">shouldn&#039;t this be part of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-256267">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256267" class="permalink" rel="bookmark">Do you mean the extension</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you mean the extension itself or the settings we are changing? The former is unlikely but the latter is a long-term goal.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-256385"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256385" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 04, 2017</p>
    </div>
    <a href="#comment-256385">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256385" class="permalink" rel="bookmark">Anyone have good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Anyone have good "about:config" rules for Thunderbird lock down</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-256503"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256503" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">April 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-256385" class="permalink" rel="bookmark">Anyone have good</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-256503">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256503" class="permalink" rel="bookmark">TorBirdy already does that,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TorBirdy already does that, if that was the question? See <a href="https://gitweb.torproject.org/torbirdy.git/tree/components/torbirdy.js#n29" rel="nofollow">https://gitweb.torproject.org/torbirdy.git/tree/components/torbirdy.js#…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-256456"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256456" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 04, 2017</p>
    </div>
    <a href="#comment-256456">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256456" class="permalink" rel="bookmark">I updated the birdie from</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I updated the birdie from inside tb but the refferred page was not reacheable without a security exception from TB</p>
<p>Your connection is not secure</p>
<p>The owner of support.mozilla.org has configured their website improperly. To protect your information from being stolen, Tor Browser has not connected to this website.</p>
<p>Learn more…</p>
<p>support.mozilla.org uses an invalid security certificate.</p>
<p>The certificate is not trusted because the issuer certificate is unknown.<br />
The server might not be sending the appropriate intermediate certificates.<br />
An additional root certificate may need to be imported.</p>
<p>Error code: SEC_ERROR_UNKNOWN_ISSUER</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-256644"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256644" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-256456" class="permalink" rel="bookmark">I updated the birdie from</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-256644">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256644" class="permalink" rel="bookmark">Lamers from Mozilla teaching</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Lamers from Mozilla teaching others how to deal with security can't configure their own servers properly, lol:<br />
This server's certificate chain is incomplete.<br />
<a href="https://www.ssllabs.com/ssltest/analyze.html?d=support.mozilla.org" rel="nofollow">https://www.ssllabs.com/ssltest/analyze.html?d=support.mozilla.org</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-256458"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256458" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 04, 2017</p>
    </div>
    <a href="#comment-256458">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256458" class="permalink" rel="bookmark">PS  This is the address that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>PS  This is the address that gives a certificate error.  Even after the exception it was not reacheable</p>
<p><a href="https://support.mozilla.org/1/firefox/45.8.0/Linux/en-US/unsigned-addons" rel="nofollow">https://support.mozilla.org/1/firefox/45.8.0/Linux/en-US/unsigned-addons</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-256506"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256506" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">April 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-256458" class="permalink" rel="bookmark">PS  This is the address that</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-256506">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256506" class="permalink" rel="bookmark">Seems to be an issue with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Seems to be an issue with support.mozilla.org... Maybe try from <a href="https://addons.mozilla.org/en-us/thunderbird/addon/torbirdy/" rel="nofollow">https://addons.mozilla.org/en-us/thunderbird/addon/torbirdy/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-256662"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-256662" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to sukhbir</p>
    <a href="#comment-256662">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-256662" class="permalink" rel="bookmark">When I used tb addons to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I used tb addons to search for tor-birdie it did not find anything, although the main add-on page opened up.  When I clicked on "check for upgrades" it found an upgrade and once restarted it was on 0.2.2<br />
The web-page itself seemed to be having certification issues but it could be a transition problem.  I was just reporting the issue, my upgrade worked.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-257019"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257019" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 05, 2017</p>
    </div>
    <a href="#comment-257019">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257019" class="permalink" rel="bookmark">Thanks, for all your work on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks, for all your work on Torbirdy.</p>
<p>Has someone thought of activating the new Enigmail feature that allows to encrypt the subject line, References, etc. (<em>"Memory Hole Protected E-mail Headers"</em>[1]), too? It works fine for me, maybe it's time too push this feature a bit by enabling it by default;-)</p>
<p>---------------------------------------</p>
<p><span class="geshifilter"><code class="php geshifilter-php">extensions<span style="color: #339933;">.</span>enigmail<span style="color: #339933;">.</span>protectHeaders &nbsp; &nbsp; &nbsp; <span style="color: #009900; font-weight: bold;">true</span></code></span></p>
<p><cite>Protect sensitive headers of encrypted messages, such as the subject. The original header is moved into the encrypted message and replaced by a dummy value (such as "Encrypted Message"). This is part of the Memory Hole standard that is currently being developed. </cite></p>
<p><span class="geshifilter"><code class="php geshifilter-php">extensions<span style="color: #339933;">.</span>enigmail<span style="color: #339933;">.</span>protectedSubjectText &nbsp;&nbsp; &nbsp; &nbsp; Encrypted Message</code></span><br />
<cite><br />
Text to use as replacement for the subject, following the Memory Hole standard. If nothing is defined, then "Encrypted Message" is used. </cite>[2, 3]</p>
<p>[1] <a href="https://github.com/ModernPGP/memoryhole" rel="nofollow">https://github.com/ModernPGP/memoryhole</a><br />
[2] <a href="https://enigmail.wiki/Advanced_Operations" rel="nofollow">https://enigmail.wiki/Advanced_Operations</a><br />
[3] <a href="https://privacy-handbuch.de/handbuch_32w.htm" rel="nofollow">https://privacy-handbuch.de/handbuch_32w.htm</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-257482"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257482" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">April 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-257019" class="permalink" rel="bookmark">Thanks, for all your work on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-257482">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257482" class="permalink" rel="bookmark">Thanks for reporting this!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for reporting this! Someone opened a ticket with this comment (<a href="https://trac.torproject.org/projects/tor/ticket/21880" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/21880</a>) so we will continue the discussion there.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
