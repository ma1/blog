title: New Release: Tor Browser 11.5.10 (Android)
---
pub_date: 2022-11-29
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.10 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.10 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.10/).

This is an Android-only release which fixes crashes on Android 12+ devices caused by the targetSdkVersion update in 11.5.9.

The full changelog since [Tor Browser 11.5.9](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-11.5/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt) is:

- Android
  - Update NoScript to 11.4.13
  - [Bug tor-browser#41481](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41481): Tor Browser 11.5.9 for Android crashes on launch on Android 12+ after targetSdkVersion update
