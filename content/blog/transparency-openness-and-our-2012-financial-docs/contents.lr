title: Transparency, openness, and our 2012 financial docs
---
pub_date: 2013-08-20
---
author: phobos
---
tags:

anonymity
privacy
transparency
free software
Form 990
IRS
Form PC
openness
freedom
---
categories: human rights
---
_html_body:

<p>After completing the standard audit, our 2012 state and federal <a href="https://www.torproject.org/about/financials.html.en" rel="nofollow">tax filings</a> are available. Our <a href="https://blog.torproject.org/blog/2012-annual-report" rel="nofollow">2012 Annual Report</a> is also available. We publish all of our related tax documents because we believe in transparency. All US non-profit organizations are required by law to make their tax filings available to the public on request by US citizens. We want to make them available for all.<br />
Part of our transparency is simply publishing the tax documents for your review. The other part is publishing what we're <a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors" rel="nofollow">working on</a> in detail. We hope you'll join us in furthering our mission (a) to develop, improve and distribute free, publicly available tools and programs that promote free speech, free expression, civic engagement and privacy rights online; (b) to conduct scientific research regarding, and to promote the use of and knowledge about, such tools, programs and related issues around the world; (c) to educate the general public around the world about privacy rights and anonymity issues connected to Internet use.<br />
All of this means you can look through our <a href="https://gitweb.torproject.org/" rel="nofollow">source code</a>, including our <a href="https://www.torproject.org/docs/documentation#DesignDoc" rel="nofollow">design documents</a>, and all open tasks, enhancements, and bugs available on our <a href="https://trac.torproject.org/" rel="nofollow">tracking system</a>. Our <a href="https://research.torproject.org/" rel="nofollow">research</a> reports are available as well. From a technical perspective, all of this free software, documentation, and code allows you and others to assess the safety and trustworthiness of our research and development. On another level, we have a 10 year track record of doing high quality work, saying what we're going to do, and doing what we said.<br />
Internet privacy and anonymity is more important and rare than ever. Please help keep us going through <a href="https://www.torproject.org/getinvolved/volunteer.html.en" rel="nofollow">getting involved</a>, <a href="https://www.torproject.org/donate/donate.html.en" rel="nofollow">donations</a>, or advocating for a free Internet with privacy, anonymity, and keeping control of your identity.</p>

