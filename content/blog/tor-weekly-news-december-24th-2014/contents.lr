title: Tor Weekly News — December 24th, 2014
---
pub_date: 2014-12-24
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the fifty-first issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Stem 1.3 is out</h1>

<p>“After months down in the engine room”, Damian Johnson <a href="https://blog.torproject.org/blog/stem-release-13" rel="nofollow">announced</a> version 1.3 of <a href="https://stem.torproject.org/" rel="nofollow">Stem</a>, the Tor controller library written in Python. Among the many improvements in this release, Damian singled out the new set of controller methods for working with hidden services, as well as the 40% increase in descriptor parsing speed.</p>

<p>Please see the <a href="https://stem.torproject.org/change_log.html#version-1-3" rel="nofollow">changelog</a> for full details of all the new features.</p>

<h1>Miscellaneous news</h1>

<p>The team of researchers working on the collection of <a href="https://bugs.torproject.org/13509" rel="nofollow">hidden service statistics</a> asked relay operators for help by enabling these statistics on their relays in the coming days and weeks. They included a step-by-step <a href="https://lists.torproject.org/pipermail/tor-relays/2014-December/005953.html" rel="nofollow">tutorial</a> for enabling this feature, which has recently been merged into Tor’s main branch.</p>

<p>Building on Andrea Shepard’s recently-merged work on <a href="https://bugs.torproject.org/9262" rel="nofollow">global cell scheduling</a>, Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/008001.html" rel="nofollow">announced</a> that the KIST socket management algorithm proposed <a href="http://www.robgjansen.com/publications/kist-sec2014.pdf" rel="nofollow">earlier this year</a> to reduce congestion in the Tor network is now “somewhat implemented” for Linux.  You can follow the testing and reviews on the associated <a href="https://bugs.torproject.org/12890" rel="nofollow">ticket</a>.</p>

<p>Nick also asked for <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/008007.html" rel="nofollow">feedback</a> on the proposal to increase the interval at which Tor relays report their bandwidth usage statistics from fifteen minutes to four hours <a href="https://bugs.torproject.org/13988" rel="nofollow"></a>: “Will this break anything you know about?”</p>

<p>Moritz Bartl <a href="https://lists.torproject.org/pipermail/tor-relays/2014-December/005958.html" rel="nofollow">invited</a> Tor relay operators to a <a href="https://events.ccc.de/congress/2014/wiki/Session:Tor_Relay_Operators_Meetup" rel="nofollow">meet-up</a> at the upcoming Chaos Communication Congress in Hamburg: “We will do quick presentations on recent and future activities around Torservers.net, talk about events relevant to the Tor relay community, and what lies ahead.”</p>

<p>Thanks to Thomas White for keeping the community <a href="https://lists.torproject.org/pipermail/tor-talk/2014-December/036067.html" rel="nofollow">updated</a> following a brief period of suspicious activity around his exit relays and Onionoo application mirrors!</p>

<h1>This week in Tor history</h1>

<p><a href="https://lists.torproject.org/pipermail/tor-news/2013-December/000026.html" rel="nofollow">A year ago this week</a>, Tor Browser hit version 3.5, bringing with it a pioneering <a href="https://blog.torproject.org/blog/deterministic-builds-part-one-cyberwar-and-global-compromise" rel="nofollow">deterministic build system</a> that set a new standard in software distribution security, and has since drawn interest from many other projects, including the <a href="https://wiki.debian.org/ReproducibleBuilds" rel="nofollow">Debian operating system</a>. It also laid the long-obsolete Vidalia graphical controller to rest, replacing it with the faster, sleeker Tor Launcher. The privacy-preserving browser is now approaching version 4.5, and users can look forward to a <a href="https://bugs.torproject.org/9387" rel="nofollow">security slider</a> offering finer-grained tuning of security preferences, as well as features that restore some of Vidalia’s <a href="https://bugs.torproject.org/8641" rel="nofollow">circuit-visualization capabilities</a>.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

