title: New Release: Tor Browser 11.0.4
---
pub_date: 2022-01-11
---
author: boklm
---
categories:

applications
releases
---
summary: Tor Browser 11.0.4 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.4 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.4/)

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-02/) to Firefox.

Tor Browser 11.0.4 updates Firefox to 91.5.0esr and gives our landing page the usual Tor Browser look and feel back, removing the parts of our year end donation campaign.

Additionally, we update NoScript to the latest release (11.2.14) and bundle the Noto Sans Gurmukhi and Sinhala fonts for our Linux users again after the underlying font rendering issue got resolved.

## Full changelog

The full changelog since [Tor Browser 11.0.3](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Windows + OS X + Linux
  - Update Firefox to 91.5.0esr
  - Update NoScript to 11.2.14
  - [Bug tor-browser-build#40405](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40405): Rotate deusexmachina IP address
  - [Bug tor-browser#40756](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40756): Fix up wrong observer removals
  - [Bug torbutton#40758](https://gitlab.torproject.org/tpo/applications/torbutton/-/issues/40758): Remove YEC takeover from about:tor
- Linux
  - [Bug tor-browser-build#40399](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40399): Bring back Noto Sans Gurmukhi and Sinhala fonts

## Known issues

Tor Browser 11.0.4 comes with a number of known issues (please check the following list before submitting a new bug report):

- [Bug tor-browser#40679](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40679): Missing features on first-time launch in esr91 on MacOS
- [Bug tor-browser#40321](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40321): AV1 videos shows as corrupt files in Windows 8.1
- [Bug tor-browser#40693](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40693): Potential Wayland dependency
