title: May 2010 Progress Report
---
pub_date: 2010-06-16
---
author: phobos
---
tags:

progress report
advocacy
tor releases
conference
speeches
---
categories:

advocacy
community
releases
reports
---
_html_body:

<p><strong> New releases </strong></p>

<p>On May 26, Tor Browser Bundle for Microsoft Windows is updated to include the newer Vidalia 0.2.9. This ﬁxes some issues with character set handling, and adds Vietnamese as a new language.</p>

<p>On May 31, we released Tor Browser Bundle for Linux 1.0.6. It contains the following updates:</p>

<ul>
<li>Add arch to tarball name so there's no collision</li>
<li>Add libpng for Arch Linux</li>
<li>Add HTTPS Everywhere extension</li>
<li>Update Qt to 4.6.2</li>
<li>Update Vidalia to 0.2.9</li>
<li>Update NoScript to 1.9.9.80</li>
</ul>

<p>On June 1st, we released Tor Browser Bundle for Linux 1.0.7. It uses an older glibc for better compatibility with older linux distributions.</p>

<p>On May 20, we released Vidalia 0.2.9. Fixes include Qt 4.6.2 compatibility, new cert, and some new translations.  You can download it at <a href="https://www.torproject.org/vidalia/" rel="nofollow">https://www.torproject.org/vidalia/</a>. Packages are slowly being updated to include this version of Vidalia.<br />
The full changelog is: </p>

<ul>
<li>Remove the GoDaddy CA certificate bundle since we changed the certificate used to authenticate connections to geoips.vidalia-project.net for downloading GeoIP information from a commercial GoDaddy certificate to a free CACert certificate.</li>
<li>Define -D_WIN32_WINNT=0x0501 on Windows builds so that MiniUPnPc will build with the latest versions of MinGW.</li>
<li>Modify miniupnpc.c from MiniUPnPc's source so that it will build on Mac OS X 10.4.</li>
<li>Work around Qt's new behavior for the QT_WA macro so that Vidalia will work correctly again on Windows with Qt &gt;= 4.6.</li>
</ul>

<p>On May 2nd, we released an updated stable version of Tor, 0.2.1.26.<br />
The detailed list of changes is:</p>

<p>  o Major bugfixes:<br />
    - Teach relays to defend themselves from connection overload. Relays<br />
      now close idle circuits early if it looks like they were intended<br />
      for directory fetches. Relays are also more aggressive about closing<br />
      TLS connections that have no circuits on them. Such circuits are<br />
      unlikely to be re-used, and tens of thousands of them were piling<br />
      up at the fast relays, causing the relays to run out of sockets<br />
      and memory. Bugfix on 0.2.0.22-rc (where clients started tunneling<br />
      their directory fetches over TLS).<br />
    - Fix SSL renegotiation behavior on OpenSSL versions like on Centos<br />
      that claim to be earlier than 0.9.8m, but which have in reality<br />
      backported huge swaths of 0.9.8m or 0.9.8n renegotiation<br />
      behavior. Possible fix for some cases of bug 1346.<br />
    - Directory mirrors were fetching relay descriptors only from v2<br />
      directory authorities, rather than v3 authorities like they should.<br />
      Only 2 v2 authorities remain (compared to 7 v3 authorities), leading<br />
      to a serious bottleneck. Bugfix on 0.2.0.9-alpha. Fixes bug 1324.</p>

<p>  o Minor bugfixes:<br />
    - Finally get rid of the deprecated and now harmful notion of "clique<br />
      mode", where directory authorities maintain TLS connections to<br />
      every other relay.</p>

<p>  o Testsuite fixes:<br />
    - In the util/threads test, no longer free the test_mutex before all<br />
      worker threads have finished. Bugfix on 0.2.1.6-alpha.<br />
    - The master thread could starve the worker threads quite badly on<br />
      certain systems, causing them to run only partially in the allowed<br />
      window. This resulted in test failures. Now the master thread sleeps<br />
      occasionally for a few microseconds while the two worker-threads<br />
      compete for the mutex. Bugfix on 0.2.0.1-alpha.</p>

<p>On May 19, we released an updated OrBot (Tor for Android), version 0.0.6, which contains Tor 0.2.2.13-alpha.</p>

<p>On May 26, we released an updated Orbot (Tor for Android), version 0.0.7, which contains a number of usability fixes reported by users.  See the bugfixes <a href="https://trac.torproject.org/projects/tor/query?status=closed&amp;component=Android+(Orbot)-Backend+/+Core&amp;order=priority&amp;col=id&amp;col=summary&amp;col=status&amp;col=type&amp;col=priority&amp;col=milestone&amp;col=component&amp;owner=" rel="nofollow">in trac</a>.</p>

<p><strong>Design, develop, and implement enhancements that make Tor a better tool for users in censored countries.</strong></p>

<p>On May 4, China's Great Firewall began blocking connections to the public Tor relays.  They also updated their blocking to include bridge relays published via email and https websites.  Further research into the blocking mechanisms from inside China show they are simply blocking IP Address and TCP port combinations. Bridge relays that have been seeded into various social networks in China continue to work well.</p>

<p>Tor on the Android OS, called Orbot, continues progress.  Work continues on a privacy-preserving web browser, Orweb, and other supporting applications to make Tor on Android more useful for daily users.  Nathan got a Tor relay running on a Moons e-7001 “iRobot” tablet, <a href="http://guardianproject.info/2010/05/25/tor-on-a-tablet" rel="nofollow">tor on a tablet</a>.</p>

<p><strong>Grow the Tor network and user base. Outreach.</strong></p>

<ul>
<li>Jacob, Roger, Runa, and Linus talked to various NorduNet and SUNET people in Sweden.  As a result, we have a new directory authority and bandwidth authority located within NorduNET.</li>
<li>Erinn and Karen attended the Global Voices Summit 2010 in Santiago, Chile.  There were a number of discussions about how to use Tor safely and its usage in activism.  Erinn held a detailed Tor training for 5-10 activists in Spanish. <a href="http://summit2010.globalvoicesonline.org" rel="nofollow">http://summit2010.globalvoicesonline.org</a></li>
<li>Roger gave a few talks as AUSCERT, <a href="http://www.auscert.org.au/" rel="nofollow">http://www.auscert.org.au/</a></li>
<li>Roger was interviewed by The Australian about Internet censorship and privacy.  <a href="http://www.theaustralian.com.au/australian-it/call-to-join-tor-network-to-fight-censorship/story-e6frgakx-1225870756466" rel="nofollow">http://www.theaustralian.com.au/australian-it/call-to-join-tor-network-…</a></li>
<li>Roger was interviewed by ComputerWorld Australia about the relationship between China and Tor.  <a href="http://www.computerworld.com.au/article/347273/auscert_2010_china_set_net_blackout_tiananmen_square_anniversary/" rel="nofollow">http://www.computerworld.com.au/article/347273/auscert_2010_china_set_n…</a></li>
<li>Jacob gave the keynote speech at CONFidence in Krakow, Poland, <a href="http://2010.confidence.org.pl/" rel="nofollow">http://2010.confidence.org.pl/</a>.</li>
<li>Jacob, Christian, and others attended pH neutral in Berlin, Germany.  <a href="http://ph-neutral.darklab.org/" rel="nofollow">http://ph-neutral.darklab.org/</a></li>
<li>Karen met with a sub-committee of the US Senate Intelligence Committee to discuss the fallacy of cyberwar.</li>
</ul>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong><br />
Continued to work on Linux and Mac OS X tor browser bundles.  The Mac version of TBB is going to use a sandboxing technology borrowed from IronFox.  This should help minimize the footprint and security concerns about running TBB on OS X computers.</p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong><br />
From the 0.2.1.26 release notes, teach relays to defend themselves from connection overload. Relays now close idle circuits early if it looks like they were intended for directory fetches. Relays are also more aggressive about closing TLS connections that have no circuits on them. Such circuits are unlikely to be re-used, and tens of thousands of them were piling up at the fast relays, causing the relays to run out of sockets and memory. Bugfix on 0.2.0.22-rc (where clients started tunneling their directory fetches over TLS).</p>

<p><strong>Translation work, ultimately a browser-based approach.</strong></p>

<ul>
<li>Added the Android orbot application to the translation portal.</li>
<li>By user request, add Serbian to the available languages.</li>
<li> Translation updates for the following languages:  Polish, Arabic, Greek, Serbian, Russian, Swedish, Chinese, Norwegian, Japanese, German, Spanish, Portugese, French, Dutch, Romanian, and Farsi.</li>
</ul>

---
_comments:

<a id="comment-6190"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6190" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 18, 2010</p>
    </div>
    <a href="#comment-6190">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6190" class="permalink" rel="bookmark">You can no longer connect to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can no longer connect to filesurf.ru unless you get rid of all entryNodes and exitNodes entries.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-6204"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6204" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 20, 2010</p>
    </div>
    <a href="#comment-6204">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6204" class="permalink" rel="bookmark">On OSX, with Vidalia</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>On OSX, with Vidalia 0.2.2.13-alpha</p>
<p>  ExitNodes {uk},{us},{se}<br />
  StrictNodes 1</p>
<p>works only, if I copy</p>
<p>  /Applications/Vidalia.app/Contents/Resources/geoip</p>
<p>to</p>
<p>  /Applications/Vidalia.app/share/tor/geoip</p>
<p>Hope this helps. ;-)</p>
<p>Cheers,<br />
SylverRat</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-6237"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6237" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 24, 2010</p>
    </div>
    <a href="#comment-6237">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6237" class="permalink" rel="bookmark">Is here any way how can i</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is here any way how can i could open only relay what i want? For example i want relay from USA, can i get it now? Thx</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-6239"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6239" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 24, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-6237" class="permalink" rel="bookmark">Is here any way how can i</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-6239">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6239" class="permalink" rel="bookmark">https://www.torproject.org/fa</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/faq#ChooseEntryExit" rel="nofollow">https://www.torproject.org/faq#ChooseEntryExit</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-6264"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6264" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 26, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-6264">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6264" class="permalink" rel="bookmark">The point is that the GeoIP</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The point is that the GeoIP file is in the wrong place. The above user's suggestion works, or you can add this to torrc:</p>
<p>GeoIPFile /Applications/Vidalia.app/Contents/Resources/geoip</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-6271"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6271" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 26, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-6271">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6271" class="permalink" rel="bookmark">The point is that the GeoIP</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The point is that the GeoIP file is in the wrong place. The above user's suggestion works, or you can add this to torrc:</p>
<p>GeoIPFile /Applications/Vidalia.app/Contents/Resources/geoip</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-6244"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6244" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 24, 2010</p>
    </div>
    <a href="#comment-6244">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6244" class="permalink" rel="bookmark">vidalia-bundle-0.2.1.26-0.2.9</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>vidalia-bundle-0.2.1.26-0.2.9.exe</p>
<p>Can not be installed on Windows 2000 Professional, a popup window always says<br />
not found a function entry in WS2_32.dll</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-6253"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6253" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 25, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-6244" class="permalink" rel="bookmark">vidalia-bundle-0.2.1.26-0.2.9</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-6253">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6253" class="permalink" rel="bookmark">You are correct.  We don&#039;t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You are correct.  We don't support Windows 2000 due to lack of proper support for a growing number of dlls and subsystems.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-6561"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6561" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 27, 2010</p>
    </div>
    <a href="#comment-6561">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6561" class="permalink" rel="bookmark">the version of firefox that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the version of firefox that is included in the windows tor bundle is out of date and has some security vulnerabilities, is it possible to update the portable firefox to the latest version somehow?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-6576"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6576" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 28, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-6561" class="permalink" rel="bookmark">the version of firefox that</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-6576">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6576" class="permalink" rel="bookmark">A new release is forthcoming.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A new release is forthcoming.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
