title: New Alpha Release: Tor Browser 11.5a10 (Android)
---
pub_date: 2022-05-26
---
author: pierov
---
categories:

applications
releases
---
summary: Tor Browser 11.5a10 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a10 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a10/).

Tor Browser 11.5a10 updates Firefox to 99.0b3 and includes bugfixes and stability improvements. 

We also use the opportunity to update various components of Tor Browser, bumping Tor to 0.4.7.7, NoScript to 11.4.5, OpenSSL to 1.1.1o, while switching to Go 1.18.1 for building our Go-related projects.

As usual, please report any issues you find in this alpha version, so we can fix them for the next upcoming major stable release (11.5).

The full changelog since [Tor Browser 11.5a7](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=tbb-11.5a10-build4) is:

- Android
  - Update Fenix to 99.03b
  - Update NoScript to 11.4.5
  - Update Tor to 0.4.7.7
  - Update OpenSSL to 1.1.1o
  - [Bug fenix#40212](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40212): Tor Browser crashing on launch
  - [Bug tor-browser-build#40433](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40433): Bump LLVM to 13.0.1 for android builds
  - [Bug tor-browser-build#40469](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40469): Update zlib to 1.2.12 (CVE-2018-25032)
  - [Bug tor-browser-build#40470](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40470): Fix zlib build issue for android
  - [Bug tor-browser-build#40485](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40485): Resolve Android reproducibility issues
  - [Bug tor-browser#40682](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40682): Set network.proxy.allow_bypass to false
  - [Bug tor-browser#40830](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40830): cherry-picked Bugzilla 1760674 on GV 99 TBA 11.5
- Build System
  - Android
    - Update Go to 1.18.1

