title: Tor Weekly News — March 4th, 2015
---
pub_date: 2015-03-04
---
author: karsten
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the ninth issue in 2015 of Tor Weekly News, the weekly newsletter that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 4.0.4 and 4.5-alpha-4 are out</h1>

<p>The Tor Browser team announced new releases in the stable and alpha branches of the privacy-preserving browser. <a href="https://blog.torproject.org/blog/tor-browser-404-released" rel="nofollow">Version 4.0.4</a> contains updates to the bundled versions of Firefox, OpenSSL, HTTPS-Everywhere and NoScript (disabling the new NoScript option to make temporary permissions permanent); it also prevents meek from issuing a second update notification.</p>

<p>Meanwhile, following on from the <a href="https://blog.torproject.org/blog/ux-sprint-2015-wrapup" rel="nofollow">recent Tor UX sprint</a>, <a href="https://blog.torproject.org/blog/tor-browser-45a4-released" rel="nofollow">version 4.5-alpha-4</a> incorporates many interface improvements designed to make life easier for Tor users. The connection configuration wizard has been reordered to avoid confusion between network proxies and bridge relays, while a reorganized Torbutton menu now offers a “New circuit for this site” option that removes the need for the user to close all open pages in order to change circuits for one destination. If users do decide to use the “New identity” option, they will now be warned that this involves losing currently-open tabs and windows.</p>

<p>Please see the announcements for full details of the changes, as well as instructions for download and verification.</p>

<h1>Reddit donates $82,765.95 to The Tor Project, Inc.</h1>

<p>One year ago, the <a href="http://www.redditblog.com/2014/02/decimating-our-ads-revenue.html" rel="nofollow">online community Reddit announced</a> that it would be donating 10% of its advertising revenue in 2014 to ten non-profits, to be selected by the Reddit community. Voting took place over the last week, and The Tor Project, Inc. placed tenth in the <a href="http://www.redditblog.com/2015/02/announcing-winners-of-reddit-donate.html" rel="nofollow">final ranking</a>, in good company with other charities like the Electronic Frontier Foundation, Wikimedia Foundation, and the Free Software Foundation, each of which being eligible for a $82,765.95 donation.</p>

<p>Community donations like this are a big step on the way to solving the problem of overreliance on single funding streams — often from national governments — that affects open-source security projects, as <a href="http://jilliancyork.com/2015/02/06/there-are-other-funding-options-than-the-usg/" rel="nofollow">recently discussed by Jillian York</a> of the Electronic Frontier Foundation (who came <a href="https://twitter.com/reddit/status/571011860260098048" rel="nofollow">first</a> in Reddit’s donation drive). Many thanks to Reddit (the company) and Reddit (the community) for their magnificent gesture of support for privacy and anonymity online — and if you’d like to join them, please take a look at the <a href="https://www.torproject.org/donate/donate" rel="nofollow">Tor Project website</a> for ways to contribute!</p>

<h1>2015 Winter Tor meeting</h1>

<p>A group of around 70 dedicated Tor contributors is <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2015WinterDevMeeting" rel="nofollow"> meeting this week in Valencia</a>, Spain to discuss plans, milestones, deadlines, and other important matters.</p>

<p>This meeting is kindly hosted together with the <a href="https://openitp.org/festival/circumvention-tech-festival.html" rel="nofollow">OpenITP circumvention tech festival</a> with public outreach and community events joined or co-hosted by Tor members.</p>

<p><a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2015WinterDevMeeting/Notes" rel="nofollow">Notes</a> are available from the sessions happening on Monday and Tuesday for those who couldn’t make it this time.</p>

<h1>Monthly status reports for February 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of February has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2015-February/000763.html" rel="nofollow">Juha Nurmi</a> released his report first, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-February/000764.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-February/000765.html" rel="nofollow">Philipp Winter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-February/000766.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000768.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000769.html" rel="nofollow">Pearl Crescent</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000770.html" rel="nofollow">Damian Johnson</a>.</p>

<p>Mike Perry reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000767.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>George Kadianakis <a href="https://blog.torproject.org/blog/some-statistics-about-onions" rel="nofollow">reports preliminary results</a> from "a project to study and quantify hidden services traffic." George emphasizes that they "are collecting data from just a few volunteer relays" and that "extrapolating from such a small sample is difficult." Taken with a grain of salt, they "estimate that about 30,000 hidden services announce themselves to the Tor network every day, using about 5 terabytes of data daily." They "also found that hidden service traffic is about 3.4% of total Tor traffic." George, together with Karsten Loesing, wrote a short <a href="https://research.torproject.org/techreports/extrapolating-hidserv-stats-2015-01-31.pdf" rel="nofollow">technical report</a> with more details on their results and methods.</p>

<p>Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2015-February/008328.html" rel="nofollow">announces</a> an important step in stabilizing the Tor 0.2.6.x release series: all work on that series will proceed on the “maint-0.2.6” branch, while work on the next release series 0.2.7.x will happen on the “master” branch. This step allows developers to focus on one branch for fixing bugs and another branch for developing new features.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, Karsten Loesing, and qbi.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

