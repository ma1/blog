title: Stockholm May 2014 Trip Report
---
pub_date: 2014-05-30
---
author: phobos
---
tags:

egypt
stockholm
sweden
stockholm internet forum
gender equality
panel talks
burkina faso
uganda
kenya
jordan
---
categories:

global south
human rights
---
_html_body:

<p>I was invited to speak on a panel at Sida about security, tools, and how they can be used in the world. The panel was generally for Sida staff to let them learn, ask questions, and interact with us in an informal way. A big thanks to Sida for providing the space and infrastructure support to allow us to all congeal for a day. My presentation from the panel is available[8].</p>

<p>Despite not being invited last year, I was invited to the Stockholm Internet Forum[1] (SIF14) this year to help talk about Tor, privacy, a cyber-panopticon[2] panel, empowering women and tech, and generally meeting with various organizations about funding partnerships. I spent most of my time split between talking to various orgs about partnerships and hanging out with the cool dfri[3] people. I met Hillevi Engström[7] and introduced her and her attending staff to Tor. The cyper-panopticon panel was held during the unconference sessions on the first day. We generally raised topics of surveillance, chilling effects, encryption, and the nature of abusive relationships between some governments and their populaces. Some pictures and videos are available[4]--if anyone has more pictures or video, let me know via email. The panel wasn't officially recorded by SIF14.</p>

<p>The better parts of the panels were the hallway/mingle sessions where everyone got to talk about real topics. I talked at length about the situation in Burkina Faso[5], the Uganda and LGBT situation, and spoke to a few women from Jordan, Egypt, Ghana, Ivory Coast, Algiers, Morocco, Uganda, and Kenya about the state of women's access to and education in technology, especially in writing code to gain employment. Everyone wants to help equalize the situation for women in both economic and rights in these countries. A huge debt of gratitude to Sarah Cortes[6], a volunteer, researcher, and strong feminist for paying her own way to Sweden to represent Tor at Sida and at SIF14, explaining the details to most of the women at the conference, and for introducing Tor to a huge number of organizations in a very positive way. It's unfortunate the conference was dominated by discussions about a few white men and surveillance. Learning more about surveillance, Snowden, and women whistle-blowers would have been interesting. However, more time given to the topics of gender equality such as the "crime of being a woman online" and what we can do to empower all women with access,  opportunity, education, and increased technical skills would have been more interesting at a conference composed of 50% women. What have you done to fight the patriarchy today?</p>

<p>I met and spoke with a few senior officials of the Middle East-North Africa region from the Foreign Ministry of Sweden about Tor, privacy, and supporting women in the region.</p>

<p>I spent the rest of the week meeting with some people from potential partners, dfri, and other activists from around the world. The general feedback from potential funding organizations is that we have a lot of work to do and the rest of the world is quickly catching up and surpassing our capabilities. At the same time, there is a lot of interest in partnering with us to accomplish many goals in improving privacy, censorship circumvention research, and improving the situation of women online.</p>

<p>All in all, a successful trip and great time in Sweden.</p>

<p>1. <a href="http://www.stockholminternetforum.se/" rel="nofollow">http://www.stockholminternetforum.se/</a><br />
2. <a href="https://cyberpanopticonsif.wordpress.com/" rel="nofollow">https://cyberpanopticonsif.wordpress.com/</a><br />
3. <a href="https://www.dfri.se/" rel="nofollow">https://www.dfri.se/</a><br />
4. <a href="http://andrewlewman.smugmug.com/SIF14-Panopticon-Panel/" rel="nofollow">http://andrewlewman.smugmug.com/SIF14-Panopticon-Panel/</a><br />
5. <a href="https://en.wikipedia.org/wiki/Burkina_Faso" rel="nofollow">https://en.wikipedia.org/wiki/Burkina_Faso</a><br />
6. <a href="http://sarahcortes.is/" rel="nofollow">http://sarahcortes.is/</a><br />
7. <a href="http://www.government.se/sb/d/13510" rel="nofollow">http://www.government.se/sb/d/13510</a><br />
8. <a href="https://svn.torproject.org/svn/projects/presentations/2014-05-26-Sida-Presentation.pdf" rel="nofollow">https://svn.torproject.org/svn/projects/presentations/2014-05-26-Sida-P…</a></p>

<p>Originally sent to the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000538.html" rel="nofollow">Tor-reports</a> mailing list.</p>

---
_comments:

<a id="comment-62190"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62190" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2014</p>
    </div>
    <a href="#comment-62190">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62190" class="permalink" rel="bookmark">Ironic it takes an arabic</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ironic it takes an arabic man (good looks notwithstanding *wink wink*) to stand up for women on the internet at a conference about white men in the news. </p>
<p>at least someone is fighting the patriarchy</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62402"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62402" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62190" class="permalink" rel="bookmark">Ironic it takes an arabic</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62402">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62402" class="permalink" rel="bookmark">He looks Mediterranean not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>He looks Mediterranean not Arabic.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-62191"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62191" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2014</p>
    </div>
    <a href="#comment-62191">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62191" class="permalink" rel="bookmark">RELEASE THE FUCKING FILES</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>RELEASE THE FUCKING FILES ALREADY! 1% OF THE SUPPOSED NSA DOCS ARE RELEASED! WHERE ARE THE REST?</p>
<p>THE LEAK KEEPERS ARE PROFITING OFF THIS UNBELIEVABLY. THEIR SECRECY IS JUST LIKE THE NSA THEY SUPPOSEDLY HATE.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62403"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62403" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62191" class="permalink" rel="bookmark">RELEASE THE FUCKING FILES</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62403">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62403" class="permalink" rel="bookmark">Oh Tarzie!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh Tarzie!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-62192"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62192" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2014</p>
    </div>
    <a href="#comment-62192">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62192" class="permalink" rel="bookmark">....cue assange screaming</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>....cue assange screaming "what about me, what about me, guys i'm still here, pay attention to me!"...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62435"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62435" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62192" class="permalink" rel="bookmark">....cue assange screaming</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62435">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62435" class="permalink" rel="bookmark">Hahaha. that made me laugh.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hahaha. that made me laugh. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-62218"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62218" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2014</p>
    </div>
    <a href="#comment-62218">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62218" class="permalink" rel="bookmark">tor Browser persian/</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tor Browser persian/</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-62355"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62355" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 31, 2014</p>
    </div>
    <a href="#comment-62355">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62355" class="permalink" rel="bookmark">Sweden is known</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sweden is known internationally as the "Saudi Arabia of Europe". It has the largest Arab population outside of the Arab Middle East.</p>
<p>It is no wonder that Sweden conducts massive surveillance on its population (both citizens and non-citizens residing in it). It then shares the surveillance data with the USA.</p>
<p>I wonder who the users are in Sweden. Could most of Tor users are Islamist or Islamist-sympathizers who wish to commit jihad in the Middle East, Europe or Sweden?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62527"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62527" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62355" class="permalink" rel="bookmark">Sweden is known</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62527">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62527" class="permalink" rel="bookmark">Based on the increasing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Based on the increasing popularity of Japanese animation in Europe, it could be that a significant number of Swedish Tor users are reading manga.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-62374"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62374" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 31, 2014</p>
    </div>
    <a href="#comment-62374">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62374" class="permalink" rel="bookmark">I support the Tor goal of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I support the Tor goal of total internet anomymity, not the stated personal agendas.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62797"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62797" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 05, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62374" class="permalink" rel="bookmark">I support the Tor goal of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62797">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62797" class="permalink" rel="bookmark">...says a rich white guy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>...says a rich white guy (likely). Fight the patriarchy!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-62400"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62400" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
    <a href="#comment-62400">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62400" class="permalink" rel="bookmark">Hello. And Bye.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello. And Bye.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62496"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62496" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62400" class="permalink" rel="bookmark">Hello. And Bye.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62496">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62496" class="permalink" rel="bookmark">T-shirt.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>T-shirt.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-62404"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62404" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
    <a href="#comment-62404">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62404" class="permalink" rel="bookmark">Please enable whatever</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please enable whatever functionality is necessary to enable direct linking to comments.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-62463"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-62463" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 01, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-62404" class="permalink" rel="bookmark">Please enable whatever</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-62463">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-62463" class="permalink" rel="bookmark">You can cobble it together</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can cobble it together yourself -- e.g. click on 'reply' here, and there's a number in the URL, which you can use to create <a href="https://blog.torproject.org/blog/stockholm-may-2014-trip-report#comment-62404" rel="nofollow">https://blog.torproject.org/blog/stockholm-may-2014-trip-report#comment…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
