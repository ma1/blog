title: Tor Browser 4.0.7 is released
---
pub_date: 2015-04-07
---
author: mikeperry
---
tags:

tor browser bundle
tor browser
tbb
tbb-4.0
---
categories:

applications
releases
---
_html_body:

<p>Unfortunately, the 4.0.7 release has <a href="https://trac.torproject.org/projects/tor/ticket/15637" rel="nofollow">a bug</a> that makes it think of itself as 4.0.6, causing an update loop. This version mismatch will also cause the incremental update to 4.0.8 to fail to properly apply. The browser will then download the full update at that point, which should succeed, but at the expense of both user delay and wasted Tor network bandwidth.</p>

<p>For this reason, we have decided to pull 4.0.7 from the website at the moment, and instead prepare 4.0.8 as soon as possible.</p>

<p>Thank you for your patience.</p>

<p>Tor Browser Project page and also from our <a href="https://www.torproject.org/dist/torbrowser/4.0.7/" rel="nofollow">distribution directory</a>.</p>

<p>This release contains an update to the included Tor software, to fix two crash bugs. One bug affects only people using the bundled tor binary to run hidden services, and the other bug allows a malicious website or Tor exit node to crash the underlying tor client by inducing it to load a resource from a hidden service with a malformed descriptor. These bugs do not allow remote code execution, but because they can be used by arbitrary actors to perform a denial of service, we are issuing a security update to address them.</p>

<p>There will be no corresponding 4.5-alpha release for this fix, to allow us to focus on stabilizing that series for release in ~2 weeks.</p>

<p><b>Note to MacOS users:</b> This is the last planned release that will run on 32 bit MacOS versions. Users of Mac OS 10.8 (Mountain Lion) and newer versions will be automatically updated to the 64 bit Tor Browser 4.5 when it is stabilized in April, and we expect this transition to be smooth for those users. However, the update process for 10.6 and 10.7 users will unfortunately not be automatic. For more details, see the <a href="https://blog.torproject.org/blog/end-life-plan-tor-browser-32-bit-macs" rel="nofollow">original end-of-life blog post.</a></p>

<p>Here is the complete changelog since 4.0.6:</p>

<ul>
<li>All Platforms
<ul>
<li>Update Tor to 0.2.5.12
   </li>
<li>Update NoScript to 2.6.9.21
 </li>
</ul>
</li>
</ul>

<p>--&gt;</p>

---
_comments:

<a id="comment-92065"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92065" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2015</p>
    </div>
    <a href="#comment-92065">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92065" class="permalink" rel="bookmark">About Tor Browser shows FF</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>About Tor Browser shows FF 31.6.0 (Tor Browser 4.0.6)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92067"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92067" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2015</p>
    </div>
    <a href="#comment-92067">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92067" class="permalink" rel="bookmark">FYI:
I have to do manually</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FYI:</p>
<p>I have to do manually last two upgrades, this one included.</p>
<p>Now Tor Browser 4.0.7 is showing Tor Browser 4.0.6 in about:tor page.</p>
<p>This is about:buildconfig</p>
<p><div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">about<span style="color: #339933;">:</span>buildconfig<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Build Machine<span style="color: #339933;">&lt;/</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>p<span style="color: #339933;">&gt;</span>gitian<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Build platform<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">target<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">x86_64<span style="color: #339933;">-</span>unknown<span style="color: #339933;">-</span>linux<span style="color: #339933;">-</span>gnu<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Build tools<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Compiler  Version     Compiler flags<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">gcc    gcc version 4<span style="color: #339933;">.</span>9<span style="color: #339933;">.</span>1 <span style="color: #009900;">&#40;</span>GCC<span style="color: #009900;">&#41;</span>     <span style="color: #339933;">-</span>Wall <span style="color: #339933;">-</span>Wpointer<span style="color: #339933;">-</span>arith <span style="color: #339933;">-</span>Wdeclaration<span style="color: #339933;">-</span>after<span style="color: #339933;">-</span>statement <span style="color: #339933;">-</span>Werror<span style="color: #339933;">=</span>return<span style="color: #339933;">-</span>type <span style="color: #339933;">-</span>Werror<span style="color: #339933;">=</span>int<span style="color: #339933;">-</span>to<span style="color: #339933;">-</span>pointer<span style="color: #339933;">-</span>cast <span style="color: #339933;">-</span>Wtype<span style="color: #339933;">-</span>limits <span style="color: #339933;">-</span>Wempty<span style="color: #339933;">-</span>body <span style="color: #339933;">-</span>Wsign<span style="color: #339933;">-</span>compare <span style="color: #339933;">-</span>Wno<span style="color: #339933;">-</span>unused <span style="color: #339933;">-</span>Wcast<span style="color: #339933;">-</span>align <span style="color: #339933;">-</span>frandom<span style="color: #339933;">-</span>seed<span style="color: #339933;">=</span>tor <span style="color: #339933;">-</span>std<span style="color: #339933;">=</span>gnu99 <span style="color: #339933;">-</span>fgnu89<span style="color: #339933;">-</span>inline <span style="color: #339933;">-</span>fno<span style="color: #339933;">-</span>strict<span style="color: #339933;">-</span>aliasing <span style="color: #339933;">-</span>fno<span style="color: #339933;">-</span>math<span style="color: #339933;">-</span>errno <span style="color: #339933;">-</span>pthread <span style="color: #339933;">-</span>pipe<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">c<span style="color: #339933;">++</span>    gcc version 4<span style="color: #339933;">.</span>9<span style="color: #339933;">.</span>1 <span style="color: #009900;">&#40;</span>GCC<span style="color: #009900;">&#41;</span>     <span style="color: #339933;">-</span>Wall <span style="color: #339933;">-</span>Wpointer<span style="color: #339933;">-</span>arith <span style="color: #339933;">-</span>Woverloaded<span style="color: #339933;">-</span><a href="http://www.php.net/virtual"><span style="color: #990000;">virtual</span></a> <span style="color: #339933;">-</span>Werror<span style="color: #339933;">=</span>return<span style="color: #339933;">-</span>type <span style="color: #339933;">-</span>Werror<span style="color: #339933;">=</span>int<span style="color: #339933;">-</span>to<span style="color: #339933;">-</span>pointer<span style="color: #339933;">-</span>cast <span style="color: #339933;">-</span>Wtype<span style="color: #339933;">-</span>limits <span style="color: #339933;">-</span>Wempty<span style="color: #339933;">-</span>body <span style="color: #339933;">-</span>Wsign<span style="color: #339933;">-</span>compare <span style="color: #339933;">-</span>Wno<span style="color: #339933;">-</span>invalid<span style="color: #339933;">-</span>offsetof <span style="color: #339933;">-</span>Wcast<span style="color: #339933;">-</span>align <span style="color: #339933;">-</span>frandom<span style="color: #339933;">-</span>seed<span style="color: #339933;">=</span>tor <span style="color: #339933;">-</span>fno<span style="color: #339933;">-</span>exceptions <span style="color: #339933;">-</span>fno<span style="color: #339933;">-</span>strict<span style="color: #339933;">-</span>aliasing <span style="color: #339933;">-</span>fno<span style="color: #339933;">-</span>rtti <span style="color: #339933;">-</span>fno<span style="color: #339933;">-</span>exceptions <span style="color: #339933;">-</span>fno<span style="color: #339933;">-</span>math<span style="color: #339933;">-</span>errno <span style="color: #339933;">-</span>std<span style="color: #339933;">=</span>gnu<span style="color: #339933;">++</span>0x <span style="color: #339933;">-</span>pthread <span style="color: #339933;">-</span>pipe <span style="color: #339933;">-</span>DNDEBUG <span style="color: #339933;">-</span>DTRIMMED <span style="color: #339933;">-</span>g <span style="color: #339933;">-</span>Os <span style="color: #339933;">-</span>freorder<span style="color: #339933;">-</span>blocks <span style="color: #339933;">-</span>fomit<span style="color: #339933;">-</span>frame<span style="color: #339933;">-</span>pointer<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Configure arguments<span style="color: #339933;">&lt;/</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>p<span style="color: #339933;">&gt;--</span>enable<span style="color: #339933;">-</span>application<span style="color: #339933;">=</span>browser <span style="color: #339933;">--</span>enable<span style="color: #339933;">-</span>optimize <span style="color: #339933;">--</span>enable<span style="color: #339933;">-</span>official<span style="color: #339933;">-</span>branding <span style="color: #339933;">--</span>enable<span style="color: #339933;">-</span>tor<span style="color: #339933;">-</span>browser<span style="color: #339933;">-</span>update <span style="color: #339933;">--</span>enable<span style="color: #339933;">-</span>update<span style="color: #339933;">-</span>packaging <span style="color: #339933;">--</span>disable<span style="color: #339933;">-</span>verify<span style="color: #339933;">-</span>mar <span style="color: #339933;">--</span>disable<span style="color: #339933;">-</span>strip <span style="color: #339933;">--</span>disable<span style="color: #339933;">-</span>install<span style="color: #339933;">-</span>strip <span style="color: #339933;">--</span>disable<span style="color: #339933;">-</span>tests <span style="color: #339933;">--</span>disable<span style="color: #339933;">-</span>debug <span style="color: #339933;">--</span>disable<span style="color: #339933;">-</span>maintenance<span style="color: #339933;">-</span>service <span style="color: #339933;">--</span>disable<span style="color: #339933;">-</span>crashreporter <span style="color: #339933;">--</span>disable<span style="color: #339933;">-</span>webrtc <span style="color: #339933;">--</span>with<span style="color: #339933;">-</span>tor<span style="color: #339933;">-</span>browser<span style="color: #339933;">-</span>version<span style="color: #339933;">=</span>4<span style="color: #339933;">.</span>0<span style="color: #339933;">.</span>6 <span style="color: #339933;">--</span>enable<span style="color: #339933;">-</span>update<span style="color: #339933;">-</span>channel<span style="color: #339933;">=</span>release<span style="color: #339933;">&lt;/</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>p<span style="color: #339933;">&gt;</span></div></li></ol></pre></div></p>
<p>NoScript version is ok</p>
<p>Very good piece of software, anyways<br />
Cheers</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-92089"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92089" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
    <a href="#comment-92089">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92089" class="permalink" rel="bookmark">The download section only</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The download section only shows Version 4.0.6</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-92095"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92095" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-92089" class="permalink" rel="bookmark">The download section only</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-92095">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92095" class="permalink" rel="bookmark">Correct. Read the text of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Correct. Read the text of the blog post above.</p>
<p>Hopefully Tor Browser 4.0.8 will be out real soon now, and resolve this confusion.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-92093"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92093" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
    <a href="#comment-92093">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92093" class="permalink" rel="bookmark">A question:
when i browse an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A question:<br />
when i browse an internet address or post something ,leave a comment and etc using Tor Browser ,are they recorded on my hard disk? If Yes how can i wipe them?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-92096"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92096" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-92093" class="permalink" rel="bookmark">A question:
when i browse an</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-92096">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92096" class="permalink" rel="bookmark">One of the goals of Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>One of the goals of Tor Browser is "disk avoidance" -- that is, to never put anything on the disk, even temporarily, that has to do with sensitive data like what websites you visit.</p>
<p><a href="https://www.torproject.org/projects/torbrowser/design/#disk-avoidance" rel="nofollow">https://www.torproject.org/projects/torbrowser/design/#disk-avoidance</a></p>
<p>So, "no (unless there's a bug)".</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-92105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92105" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 09, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-92105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92105" class="permalink" rel="bookmark">Does sandboxie compromise</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does sandboxie compromise this function?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
