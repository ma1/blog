title: Tails 0.22.1 is out
---
pub_date: 2014-02-09
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.22.1, is out.</p>

<p>All users must upgrade as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_0.22/" rel="nofollow">numerous security issues</a>.</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Security fixes
<ul>
<li>Upgrade the web browser to 24.3.0esr, that fixes a few serious security issues.</li>
<li>Upgrade the system NSS to 3.14.5, that fixes a few serious security issues.</li>
<li>Workaround a browser size fingerprinting issue by using small icons in the web browser's navigation toolbar.</li>
<li>Upgrade Pidgin to 2.10.8, that fixes a number of serious security issues.</li>
</ul>
</li>
<li>Major improvements
<ul>
<li>Check for upgrades availability using Tails Upgrader, and propose to apply an incremental upgrade whenever possible.</li>
<li>Install Linux 3.12 (3.12.6-2).</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Fix the keybindings problem introduced in 0.22.</li>
<li>Fix the Unsafe Browser problem introduced in 0.22.</li>
<li>Use IE's icon in Windows camouflage mode.</li>
<li>Handle some corner cases better in Tails Installer.</li>
<li>Use the correct browser homepage in Spanish locales.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Update Torbutton to 1.6.5.3.</li>
<li>Do not start Tor Browser automatically, but notify when Tor<br />
is ready.</li>
<li>Import latest Tor Browser prefs.</li>
<li>Many user interface improvements in Tails Upgrader.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li>The memory erasure on shutdown <a href="https://labs.riseup.net/code/issues/6460" rel="nofollow">does not work on some hardware</a>.</li>
<li><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding</a> known issues.</li>
</ul>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/index.en.html" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for March 18.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

<p><strong>Support and feedback</strong></p>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

