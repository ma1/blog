title: Tor 0.2.0.35-stable released
---
pub_date: 2009-06-26
---
author: phobos
---
tags:

stable release
bug fixes
hidden service fixes
---
categories:

onion services
releases
---
_html_body:

<p>Tor 0.2.0.35 fixes a big bug that was causing Tor relays with dynamic<br />
IP addresses to disappear from the network. It also fixes a rare crash<br />
bug on fast exit relays.</p>

<p><a href="https://www.torproject.org/easy-download" rel="nofollow">https://www.torproject.org/easy-download</a></p>

<p>Changes in version 0.2.0.35 - 2009-06-24<br />
<strong>Security fix:</strong></p>

<ul>
<li>Avoid crashing in the presence of certain malformed descriptors.<br />
      Found by lark, and by automated fuzzing.</li>
<li>Fix an edge case where a malicious exit relay could convince a<br />
      controller that the client's DNS question resolves to an internal IP<br />
      address. Bug found and fixed by "optimist"; bugfix on 0.1.2.8-beta.</li>
</ul>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Finally fix the bug where dynamic-IP relays disappear when their<br />
      IP address changes: directory mirrors were mistakenly telling<br />
      them their old address if they asked via begin_dir, so they<br />
      never got an accurate answer about their new address, so they<br />
      just vanished after a day. For belt-and-suspenders, relays that<br />
      don't set Address in their config now avoid using begin_dir for<br />
      all direct connections. Should fix bugs 827, 883, and 900.</li>
<li>Fix a timing-dependent, allocator-dependent, DNS-related crash bug<br />
      that would occur on some exit nodes when DNS failures and timeouts<br />
      occurred in certain patterns. Fix for bug 957.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>When starting with a cache over a few days old, do not leak<br />
      memory for the obsolete router descriptors in it. Bugfix on<br />
      0.2.0.33; fixes bug 672.</li>
<li>Hidden service clients didn't use a cached service descriptor that<br />
      was older than 15 minutes, but wouldn't fetch a new one either,<br />
      because there was already one in the cache. Now, fetch a v2<br />
      descriptor unless the same descriptor was added to the cache within<br />
      the last 15 minutes. Fixes bug 997; reported by Marcus Griep.</li>
</ul>

<p>The original announcement can be found at <a href="http://archives.seul.org/or/announce/Jun-2009/msg00000.html" rel="nofollow">http://archives.seul.org/or/announce/Jun-2009/msg00000.html</a></p>

---
_comments:

<a id="comment-1650"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1650" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 26, 2009</p>
    </div>
    <a href="#comment-1650">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1650" class="permalink" rel="bookmark">Cannot upgrade or uninstall version 0.2.0.31 on OS X</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I currently have Tor 0.2.0.31 installed on OS X 10.4.11.</p>
<p>When I run the installer for the latest release (0.2.0.35) the installer says there is already a newer version installed. If I continue with the install anyway, it fails. There is no error message but the new version does not run.</p>
<p>After restoring the old version of Tor (0.2.0.31) from backup, everything is OK. Then I tried to uninstall the old version using the supplied script, but it fails, giving me:</p>
<p><a href="mailto:root@sebago" rel="nofollow">root@sebago</a>&gt; ./uninstall_tor_bundle.sh<br />
. tor process appears to already be stopped<br />
. Killing currently running privoxy process, pid is 196<br />
./uninstall_tor_bundle.sh: line 123: ./package_list.txt: No such file or directory<br />
. Removing created user _tor<br />
delete: Invalid Path<br />
. Cleaning up<br />
rm: fts_read: No such file or directory<br />
. Finished</p>
<p>Next, I restored again from backup and tried to manually uninstall following the documentation. However, some of the files and directories that are supposed to be deleted do not exist on my installation. Other files and directories that obviously belong to Tor are not listed in the documentation of what to delete.</p>
<p>After deleting everything I could find that looks like it belongs to Tor/Vadalia/Privoxy/Torbutton I ran the installer for version 0.2.0.35 again and it says the install was successful, but after restarting and trying to start Vidalia, it doesn't run.</p>
<p>Now I have 0.2.31 running again after restoring again from backup, but I am at a total loss as to how to properly uninstall or upgrade!!</p>
<p>Please help!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1677"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1677" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Alexios (not verified)</span> said:</p>
      <p class="date-time">June 28, 2009</p>
    </div>
    <a href="#comment-1677">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1677" class="permalink" rel="bookmark">Problems with DirPort</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I keep getting this message: "Your server has not managed to confirm that its DirPort is reachable. Please check your firewalls, ports, address, etc"</p>
<p>I was hoping that, by getting the latest version of Vidalia/Tor, this would solve the problem; unfortunately, it hasn't. To be honest, everything else is working fine as far as connections/ports are concerned; however, with the Directory Port, I've tried every configuration with my wireless tp-link router, changing things on DNS, as well as going through different ports, but nothing seems to work. I'm at wit's end here, can anyone offer any help or guidance?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1680"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1680" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 28, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1677" class="permalink" rel="bookmark">Problems with DirPort</a> by <span>Alexios (not verified)</span></p>
    <a href="#comment-1680">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1680" class="permalink" rel="bookmark">Re: Problems with DirPort</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeah. That's a problem with 0.2.0.x. It's a cosmetic problem though -- nothing<br />
is actually going wrong, I think.</p>
<p>It's fixed in Tor 0.2.1.x.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1731"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1731" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 01, 2009</p>
    </div>
    <a href="#comment-1731">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1731" class="permalink" rel="bookmark">The installation of 0.2.0.35 fails on Mac OS X 10.5.7</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In the postflight script in the Tor.pkg there is a line that fails:</p>
<p>su $USER /Applications/Firefox.app/Contents/MacOS/firefox-bin /Library/Torbutton/torbutton-$TORBUTTON_VERSION.xpi</p>
<p>It worked again when it was changed as in the corresponding package in 0.2.0.34 (use firefox instead of the firefox-bin executable):</p>
<p>su $USER /Applications/Firefox.app/Contents/MacOS/firefox /Library/Torbutton/torbutton-$TORBUTTON_VERSION.xpi</p>
<p>The same results for firefox versions 3.0 and 3.5 for mac.</p>
<p>Hope this helps</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1737"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1737" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 02, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1731" class="permalink" rel="bookmark">The installation of 0.2.0.35 fails on Mac OS X 10.5.7</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1737">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1737" class="permalink" rel="bookmark">I get the same message.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I get the same message.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1750"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1750" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2009</p>
    </div>
    <a href="#comment-1750">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1750" class="permalink" rel="bookmark">Installation of  0.2.0.35 fail on Mac OS X 10.5.7</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The installer say in log:<br />
Installer[4953]: run postflight script for Tor<br />
postflight[4997]: usage: chown [-fhv] [-R [-H | -L | -P]] owner[:group] file ...<br />
postflight[4997]:        chown [-fhv] [-R [-H | -L | -P]] :group file ...<br />
runner[4956]: postflight[4997]:<br />
runner[4956]: postflight[4997]: /Applications/Firefox.app/Contents/MacOS/firefox-bin: /Applications/Firefox.app/Contents/MacOS/firefox-bin: cannot execute binary file<br />
runner[4956]: postflight[4997]:<br />
Installer[4953]: Install failed: (...) : run postflight script for Tor.</p>
<p>Please correct this !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1761"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1761" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 06, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1750" class="permalink" rel="bookmark">Installation of  0.2.0.35 fail on Mac OS X 10.5.7</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1761">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1761" class="permalink" rel="bookmark">have the same issue as above</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>have the same issue as above poster. any fix for this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1780"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1780" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 10, 2009</p>
    </div>
    <a href="#comment-1780">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1780" class="permalink" rel="bookmark">proxy problems</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why the tor proxy nao aceited by my firefox?<br />
what I'm doing wrong? or what is missing?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1784"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1784" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 10, 2009</p>
    </div>
    <a href="#comment-1784">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1784" class="permalink" rel="bookmark">when is tor going to update</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>when is tor going to update the version of privoxy distributed with tor.<br />
your still using 3.0.6 when the latest version is 3.0.13.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1787"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1787" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Andres (not verified)</span> said:</p>
      <p class="date-time">July 11, 2009</p>
    </div>
    <a href="#comment-1787">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1787" class="permalink" rel="bookmark">Thing dont work under OSX 10.5.7</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>pretty sad, today try to install tor on my OSX 10.5.7, with no succes.<br />
First 0.2.1.17-rc , Error in the Setup, but i try to run in anyway, using Firefox and Torbutton, error conectin to proxy. Use the documented uninstall procedure to test a downgrade to "Stable" 0.2.0.35. Same error on install and ( saying a newer version exist) so your script is LAME. pretty bad all i can say about my experience.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1791"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1791" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 11, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1787" class="permalink" rel="bookmark">Thing dont work under OSX 10.5.7</a> by <span>Andres (not verified)</span></p>
    <a href="#comment-1791">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1791" class="permalink" rel="bookmark">re: Thing dont work under OSX 10.5.7</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually, this is a bug in the Apple installer.  The necessary packages installed fine, and would require a reboot, but because the Firefox plugin for Tor failed to install, Apple aborts the entire installation and calls it all bad.</p>
<p>Dealing with the various issues with the Apple Installer is why we've been working on a drag and drop install routine for the Vidalia-Bundle for OSX.  We're also working on a Tor Browser Bundle for OSX to make using Tor even simpler.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1798"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1798" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://www.ipw3n.hostcell.net">Anonymous (not verified)</a> said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
    <a href="#comment-1798">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1798" class="permalink" rel="bookmark">so what can I do until then?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>you said you are working on a drag and drop which is great, but what can I use as a workaround right now</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1801"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1801" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1798" class="permalink" rel="bookmark">so what can I do until then?</a> by <a rel="nofollow" href="http://www.ipw3n.hostcell.net">Anonymous (not verified)</a></p>
    <a href="#comment-1801">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1801" class="permalink" rel="bookmark">re: so what can I do until then?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your install worked fine.  Reboot and everything should work as expected.</p>
<p>You may have to manually install Torbutton in firefox.  You can get it from the Mozilla Add-on site, or our site, <a href="https://torproject.org/torbutton" rel="nofollow">https://torproject.org/torbutton</a>, or it's also located in /Library/Torbutton on your machine.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1799"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1799" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
    <a href="#comment-1799">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1799" class="permalink" rel="bookmark">I had the install problem</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I had the install problem with Tor under OS X 10.5.7 as well.  Then I tried installing it again and it appeared to work.  But when I try using my Tor-enabled Firefox 3.5, it thinks I'm connecting to a bad proxy.  No Tor for me.</p>
<p>This is bad, is there some earlier version I can use?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1802" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1799" class="permalink" rel="bookmark">I had the install problem</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1802" class="permalink" rel="bookmark">re: I had the install problem</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>reboot and everything will work fine.  Privoxy only starts on boot.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1915"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1915" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>etc (not verified)</span> said:</p>
      <p class="date-time">July 22, 2009</p>
    </div>
    <a href="#comment-1915">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1915" class="permalink" rel="bookmark">i tried installing the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i tried installing the vidalia/tor/privoxy/torbutton bundle, and when i did i was warned that there was already a newer version of it installed. there wasnt, so i ignored it and installed anyway. Now everything should be installed, but there is no vidalia app in my applications folder.</p>
<p>wahappen?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1916"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1916" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>etc (not verified)</span> said:</p>
      <p class="date-time">July 22, 2009</p>
    </div>
    <a href="#comment-1916">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1916" class="permalink" rel="bookmark">err this is on tiger btw</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>err this is on tiger btw</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2613"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2613" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2009</p>
    </div>
    <a href="#comment-2613">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2613" class="permalink" rel="bookmark">I installed the tor browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I installed the tor browser and all that and it seems to work fine except for a major problem. All the main sites like yahoo and google are in Deutsch (German) and not in English. I can't seem to change it. Does anyone know how I can fix this? Please e-mail me if you know what's wrong. <a href="mailto:jakelogan911@gmail.com" rel="nofollow">jakelogan911@gmail.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2777"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2777" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 07, 2009</p>
    </div>
    <a href="#comment-2777">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2777" class="permalink" rel="bookmark">install problem</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I run the installer, it says that firefox is not installed even though it is the default browser. Any tips for this?</p>
<p>Thank you</p>
<p>Kim</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4354"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4354" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2010</p>
    </div>
    <a href="#comment-4354">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4354" class="permalink" rel="bookmark">I CAN&#039;T SEEM TO UNINSTALL</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I CAN'T SEEM TO UNINSTALL THE VIDALIA BUNDLE FROM MY PROGRAM FILES.  OBVIOUSLY, THE UNINSTALL DOESN'T WORK THAT IS LOCATED ON THE START MENU.  I'VE TRIED EVERYTHING.  HOW CAN I REMOVE THESE FILES? I HAVE VISTA</p>
<p><a href="mailto:VICTORIA.JORDASH@YAHOO.COM" rel="nofollow">VICTORIA.JORDASH@YAHOO.COM</a></p>
<p>THANK YOU</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4368"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4368" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 18, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4354" class="permalink" rel="bookmark">I CAN&#039;T SEEM TO UNINSTALL</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4368">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4368" class="permalink" rel="bookmark">Follow the directions at</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Follow the directions at <a href="http://www.torproject.org/faq#HowUninstallTor" rel="nofollow">http://www.torproject.org/faq#HowUninstallTor</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-15074"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15074" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 19, 2012</p>
    </div>
    <a href="#comment-15074">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15074" class="permalink" rel="bookmark">I installed Vidalia, when I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I installed Vidalia, when I start it up, I press "start Tor" but it doesn't get further then "connecting to a relay directory" ... I tried everything, can't get it started. I have Win7, and i try to use it with firefox 11.0 (app installed (tor button)(bundle))</p>
</div>
  </div>
</article>
<!-- Comment END -->
