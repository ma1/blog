title: New release: Tor 0.4.1.6
---
pub_date: 2019-09-19
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>We have a new stable release today. If you build Tor from source, you can download the source code for 0.4.1.6 from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available within the next several weeks, with a new Tor Browser in the next week or two.</p>
<p>This release backports several bugfixes to improve stability and correctness. Anyone experiencing build problems or crashes with 0.4.1.5, or experiencing reliability issues with single onion services, should upgrade.</p>
<h2>Changes in version 0.4.1.6 - 2019-09-19</h2>
<ul>
<li>Major bugfixes (crash, Linux, Android, backport from 0.4.2.1-alpha):
<ul>
<li>Tolerate systems (including some Android installations) where madvise and MADV_DONTDUMP are available at build-time, but not at run time. Previously, these systems would notice a failed syscall and abort. Fixes bug <a href="https://bugs.torproject.org/31570">31570</a>; bugfix on 0.4.1.1-alpha.</li>
<li>Tolerate systems (including some Linux installations) where madvise and/or MADV_DONTFORK are available at build-time, but not at run time. Previously, these systems would notice a failed syscall and abort. Fixes bug <a href="https://bugs.torproject.org/31696">31696</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor features (stem tests, backport from 0.4.2.1-alpha):
<ul>
<li>Change "make test-stem" so it only runs the stem tests that use tor. This change makes test-stem faster and more reliable. Closes ticket <a href="https://bugs.torproject.org/31554">31554</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (build system, backport form 0.4.2.1-alpha):
<ul>
<li>Do not include the deprecated &lt;sys/sysctl.h&gt; on Linux or Windows systems. Fixes bug <a href="https://bugs.torproject.org/31673">31673</a>; bugfix on 0.2.5.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.4.2.1-alpha):
<ul>
<li>Add more stub functions to fix compilation on Android with link- time optimization when --disable-module-dirauth is used. Previously, these compilation settings would make the compiler look for functions that didn't exist. Fixes bug <a href="https://bugs.torproject.org/31552">31552</a>; bugfix on 0.4.1.1-alpha.</li>
<li>Suppress spurious float-conversion warnings from GCC when calling floating-point classifier functions on FreeBSD. Fixes part of bug <a href="https://bugs.torproject.org/31687">31687</a>; bugfix on 0.3.1.5-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller protocol):
<ul>
<li>Fix the MAPADDRESS controller command to accept one or more arguments. Previously, it required two or more arguments, and ignored the first. Fixes bug <a href="https://bugs.torproject.org/31772">31772</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (guards, backport from 0.4.2.1-alpha):
<ul>
<li>When tor is missing descriptors for some primary entry guards, make the log message less alarming. It's normal for descriptors to expire, as long as tor fetches new ones soon after. Fixes bug <a href="https://bugs.torproject.org/31657">31657</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, backport from 0.4.2.1-alpha):
<ul>
<li>Change log level of message "Hash of session info was not as expected" to LOG_PROTOCOL_WARN. Fixes bug <a href="https://bugs.torproject.org/12399">12399</a>; bugfix on 0.1.1.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (rust, backport from 0.4.2.1-alpha):
<ul>
<li>Correctly exclude a redundant rust build job in Travis. Fixes bug <a href="https://bugs.torproject.org/31463">31463</a>; bugfix on 0.3.5.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (v2 single onion services, backport from 0.4.2.1-alpha):
<ul>
<li>Always retry v2 single onion service intro and rend circuits with a 3-hop path. Previously, v2 single onion services used a 3-hop path when rendezvous circuits were retried after a remote or delayed failure, but a 1-hop path for immediate retries. Fixes bug <a href="https://bugs.torproject.org/23818">23818</a>; bugfix on 0.2.9.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (v3 single onion services, backport from 0.4.2.1-alpha):
<ul>
<li>Always retry v3 single onion service intro and rend circuits with a 3-hop path. Previously, v3 single onion services used a 3-hop path when rend circuits were retried after a remote or delayed failure, but a 1-hop path for immediate retries. Fixes bug <a href="https://bugs.torproject.org/23818">23818</a>; bugfix on 0.3.2.1-alpha.</li>
<li>Make v3 single onion services fall back to a 3-hop intro, when all intro points are unreachable via a 1-hop path. Previously, v3 single onion services failed when all intro nodes were unreachable via a 1-hop path. Fixes bug <a href="https://bugs.torproject.org/23507">23507</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Documentation (backport from 0.4.2.1-alpha):
<ul>
<li>Use RFC 2397 data URL scheme to embed an image into tor-exit- notice.html so that operators no longer have to host it themselves. Closes ticket <a href="https://bugs.torproject.org/31089">31089</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-284161"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284161" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>downloader (not verified)</span> said:</p>
      <p class="date-time">September 23, 2019</p>
    </div>
    <a href="#comment-284161">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284161" class="permalink" rel="bookmark">I have a bug on Android…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have a bug on Android. While downloading a file "pause and cancel" doesn't work.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284165"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284165" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">September 24, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284161" class="permalink" rel="bookmark">I have a bug on Android…</a> by <span>downloader (not verified)</span></p>
    <a href="#comment-284165">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284165" class="permalink" rel="bookmark">Hi! That sounds like a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi! That sounds like a browser problem. If possible, you should open a bug against whatever Android tor application you're using.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-284226"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284226" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>NoneDePlume (not verified)</span> said:</p>
      <p class="date-time">September 30, 2019</p>
    </div>
    <a href="#comment-284226">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284226" class="permalink" rel="bookmark">Hi, what standards can be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, what standards can be set using HardwareAccel 1?  As in OpenSSL standards?</p>
<p>Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
