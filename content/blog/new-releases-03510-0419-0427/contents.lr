title: New releases: 0.3.5.10, 0.4.1.9, and 0.4.2.7 (with security fixes!)
---
pub_date: 2020-03-18
---
author: nickm
---
tags: stable release
---
categories: releases
---
summary: We have new releases today. If you build Tor from source, you can download the source code for 0.4.2.7 from the download page on the website. Packages should be available within the next several days, including a new Tor Browser.
---
_html_body:

<p>We have new releases today. If you build Tor from source, you can download the source code for 0.4.2.7 from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available within the next several days, including a new Tor Browser.</p>
<p>This is the third stable release in the 0.4.2.x series. It backports numerous fixes from later releases, including a fix for TROVE-2020-002, a major denial-of-service vulnerability that affected all released Tor instances since 0.2.1.5-alpha. Using this vulnerability, an attacker could cause Tor instances to consume a huge amount of CPU, disrupting their operations for several seconds or minutes. This attack could be launched by anybody against a relay, or by a directory cache against any client that had connected to it. The attacker could launch this attack as much as they wanted, thereby disrupting service or creating patterns that could aid in traffic analysis. This issue was found by OSS-Fuzz, and is also tracked as CVE-2020-10592.</p>
<p>We do not have reason to believe that this attack is currently being exploited in the wild, but nonetheless we advise everyone to upgrade as soon as packages are available.</p>
<p>We're also releasing updates for our older supported series.  You can find the source code for 0.3.5.10 and 0.4.1.9 from our distribution site at <a href="http://dist.torproject.org/">http://dist.torproject.org/</a>. You can also read the <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.3.5.10">0.3.5.10 ChangeLog</a> and the <a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.4.1.9">0.4.1.9 ChangeLog</a>.</p>
<p>There's also a new alpha, described in the previous blog post.</p>
<h2>Changes in version 0.4.2.7 - 2020-03-18</h2>
<ul>
<li>Major bugfixes (security, denial-of-service, backport from 0.4.3.3-alpha):
<ul>
<li>Fix a denial-of-service bug that could be used by anyone to consume a bunch of CPU on any Tor relay or authority, or by directories to consume a bunch of CPU on clients or hidden services. Because of the potential for CPU consumption to introduce observable timing patterns, we are treating this as a high-severity security issue. Fixes bug <a href="https://bugs.torproject.org/33119">33119</a>; bugfix on 0.2.1.5-alpha. Found by OSS-Fuzz. We are also tracking this issue as TROVE-2020-002 and CVE-2020-10592.</li>
</ul>
</li>
<li>Major bugfixes (circuit padding, memory leak, backport from 0.4.3.3-alpha):
<ul>
<li>Avoid a remotely triggered memory leak in the case that a circuit padding machine is somehow negotiated twice on the same circuit. Fixes bug <a href="https://bugs.torproject.org/33619">33619</a>; bugfix on 0.4.0.1-alpha. Found by Tobias Pulls. This is also tracked as TROVE-2020-004 and CVE-2020-10593.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (directory authority, backport from 0.4.3.3-alpha):
<ul>
<li>Directory authorities will now send a 503 (not enough bandwidth) code to clients when under bandwidth pressure. Known relays and other authorities will always be answered regardless of the bandwidth situation. Fixes bug <a href="https://bugs.torproject.org/33029">33029</a>; bugfix on 0.1.2.5-alpha.</li>
</ul>
</li>
<li>Minor features (continuous integration, backport from 0.4.3.2-alpha):
<ul>
<li>Stop allowing failures on the Travis CI stem tests job. It looks like all the stem hangs we were seeing before are now fixed. Closes ticket <a href="https://bugs.torproject.org/33075">33075</a>.</li>
</ul>
</li>
<li>Minor bugfixes (bridges, backport from 0.4.3.1-alpha):
<ul>
<li>Lowercase the configured value of BridgeDistribution before adding it to the descriptor. Fixes bug <a href="https://bugs.torproject.org/32753">32753</a>; bugfix on 0.3.2.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, backport from 0.4.3.2-alpha):
<ul>
<li>If we encounter a bug when flushing a buffer to a TLS connection, only log the bug once per invocation of the Tor process. Previously we would log with every occurrence, which could cause us to run out of disk space. Fixes bug <a href="https://bugs.torproject.org/33093">33093</a>; bugfix on 0.3.2.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion services v3, backport from 0.4.3.3-alpha):
<ul>
<li>Fix an assertion failure that could result from a corrupted ADD_ONION control port command. Found by Saibato. Fixes bug <a href="https://bugs.torproject.org/33137">33137</a>; bugfix on 0.3.3.1-alpha. This issue is also tracked as TROVE-2020-003.</li>
</ul>
</li>
<li>Minor bugfixes (rust, build, backport from 0.4.3.2-alpha):
<ul>
<li>Fix a syntax warning given by newer versions of Rust that was creating problems for our continuous integration. Fixes bug <a href="https://bugs.torproject.org/33212">33212</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Testing (Travis CI, backport from 0.4.3.3-alpha):
<ul>
<li>Remove a redundant distcheck job. Closes ticket <a href="https://bugs.torproject.org/33194">33194</a>.</li>
<li>Sort the Travis jobs in order of speed: putting the slowest jobs first takes full advantage of Travis job concurrency. Closes ticket <a href="https://bugs.torproject.org/33194">33194</a>.</li>
<li>Stop allowing the Chutney IPv6 Travis job to fail. This job was previously configured to fast_finish (which requires allow_failure), to speed up the build. Closes ticket <a href="https://bugs.torproject.org/33195">33195</a>.</li>
<li>When a Travis chutney job fails, use chutney's new "diagnostics.sh" tool to produce detailed diagnostic output. Closes ticket <a href="https://bugs.torproject.org/32792">32792</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-287094"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287094" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2020</p>
    </div>
    <a href="#comment-287094">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287094" class="permalink" rel="bookmark">Why can&#039;t you publish this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why can't you publish this vulnerability AFTER you upload all necessary packages?<br />
Not all people compile from source. In fact they just apt update only.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287095"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287095" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">March 18, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287094" class="permalink" rel="bookmark">Why can&#039;t you publish this…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287095">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287095" class="permalink" rel="bookmark">The team that makes the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The team that makes the source code is not the same team that makes the packages-- in fact, most of our packages are made by volunteers.  By the time all the packagers have the source, the source is generally out there.  Many other projects have found that trying to keep a vulnerability secret until everybody can build packages tends to result in more leaks than than they've desired.</p>
<p>We've been talking about when, if ever, we'd try to get the source out to "packagers only" ahead of public disclosure -- if we did something like that, it would probably be reserved for something even worse than DoS: maybe if we found a remote code execution bug or something.</p>
<p>Have a look at our <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/SecurityPolicy" rel="nofollow">current draft security policy</a> for more info about how we handle this stuff.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287110"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287110" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>-_- (not verified)</span> said:</p>
      <p class="date-time">March 20, 2020</p>
    </div>
    <a href="#comment-287110">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287110" class="permalink" rel="bookmark">How was the denial-of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How was the denial-of-service vulnerability fixed? What did you do to fix it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287123"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287123" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">March 21, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287110" class="permalink" rel="bookmark">How was the denial-of…</a> by <span>-_- (not verified)</span></p>
    <a href="#comment-287123">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287123" class="permalink" rel="bookmark">We&#039;ll post more detailed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We'll post more detailed information once the fix has been out for a while.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287112"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287112" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Dee (not verified)</span> said:</p>
      <p class="date-time">March 20, 2020</p>
    </div>
    <a href="#comment-287112">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287112" class="permalink" rel="bookmark">Since many years ago we were…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since many years ago we were connecting to Tor using InvizBox routers. Since you publish this update all the InvizBox and (OpenWrt) similar routers are crashing. The update in the routers' firmware will take even months, until then all us have no other choice to stay offline.<br />
I cannot find the words to describe this tremendous mess in the middle of a pandemic with all the people locked up in their homes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287145"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287145" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 23, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287112" class="permalink" rel="bookmark">Since many years ago we were…</a> by <span>Dee (not verified)</span></p>
    <a href="#comment-287145">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287145" class="permalink" rel="bookmark">Write a bug report to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Write a bug report to OpenWrt asking them to update their package:<br />
<a href="https://openwrt.org/bugs" rel="nofollow">https://openwrt.org/bugs</a><br />
<a href="https://github.com/openwrt/packages/issues?q=is%3Aissue+is%3Aopen+tor" rel="nofollow">https://github.com/openwrt/packages/issues?q=is%3Aissue+is%3Aopen+tor</a></p>
<p>As of today (2020-03-24),<br />
OpenWrt 19.07.2 (current stable) package repository feed is at tor version 0.4.2.5 (2019-12-09).<br />
OpenWrt 18.06.8 (old stable) package repository feed is at tor version 0.4.1.6 (2019-09-19).</p>
<p><a href="https://openwrt.org/packages/pkgdata/tor" rel="nofollow">https://openwrt.org/packages/pkgdata/tor</a><br />
<a href="https://downloads.openwrt.org/releases/19.07.2/packages/" rel="nofollow">https://downloads.openwrt.org/releases/19.07.2/packages/</a><br />
<a href="https://downloads.openwrt.org/releases/18.06.8/packages/" rel="nofollow">https://downloads.openwrt.org/releases/18.06.8/packages/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287338"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287338" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 01, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287145" class="permalink" rel="bookmark">Write a bug report to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287338">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287338" class="permalink" rel="bookmark">Update: The tor package in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Update: The tor package in OpenWrt 19.07 has been updated to 0.4.2.7, so now, go and ask your InvizBox and similar router developers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
