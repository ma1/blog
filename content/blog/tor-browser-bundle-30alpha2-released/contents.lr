title: Tor Browser Bundle 3.0alpha2 Released
---
pub_date: 2013-07-01
---
author: mikeperry
---
tags:

tor browser bundle
security
tor browser
tbb
gitian
deterministic builds
tbb-3.0
---
categories:

applications
releases
---
_html_body:

<p>The second alpha release in the <a href="https://blog.torproject.org/category/tags/tbb-30" rel="nofollow">3.0 series</a> of the Tor Browser Bundle is now available from the <a href="https://archive.torproject.org/tor-package-archive/torbrowser/3.0a2" rel="nofollow">Tor Package Archive</a>.</p>

<p>In addition to providing important security updates to Firefox and Tor, these release binaries should now be exactly reproducible from the source code by anyone. They have been independently reproduced by at least 3 public builders using independent machines, and the Tor Package Archive contains all three builder's GPG signatures of the sha256sums.txt file in the package directory.</p>

<p>To build your own identical copies of these bundles from source code, check out <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/" rel="nofollow">the official repository</a> and <a href="http://stackoverflow.com/questions/10303665/how-to-clone-a-specific-version-of-a-git-repository/10304054#10304054" rel="nofollow">use</a> git tag <b>tbb-3.0alpha2-release</b> (commit c0242c24bed086cc9c545c7bf2d699948792c1e3). <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/README.build" rel="nofollow">These instructions</a> should explain things from there. If you notice any differences from the official bundles, I would love to hear about it!</p>

<p>I will be writing a two part blog series explaining why this is important, and describing the technical details of how it was accomplished in the coming week or two. For now, <a href="https://mailman.stanford.edu/pipermail/liberationtech/2013-June/009257.html" rel="nofollow">a brief explanation</a> can be found on the <a href="https://mailman.stanford.edu/mailman/listinfo/liberationtech" rel="nofollow">Liberation Technologies</a> mailing list archive.</p>

<h1>ChangeLog</h1>

<ul>
<li><b>All Platforms:</b></li>
<ul>
<li>Update Firefox to 17.0.7esr</li>
<li>Update Tor to 0.2.4.14-alpha</li>
<li>Include Tor's GeoIP file</li>
<ul>
<li>This should fix custom torrc issues with country-based node restrictions</li>
</ul>
<li>Fix several build determinism issues</li>
<li>Include ChangeLog in bundles</li>
</ul>
<li><b>Windows:</b></li>
<ul>
<li>Fix many crash issues by disabling Direct2D support for now.</li>
</ul>
<li><b>Mac:</b></li>
<ul>
<li>Bug 8987: Disable TBB's 'Saved Application State' disk records on OSX 10.7+</li>
</ul>
<li><b>Linux:</b></li>
<ul>
<li>Use Ubuntu's 'hardening-wrapper' to build our Linux binaries</li>
</ul>
</ul>

<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob_plain/HEAD:/Bundle-Data/Docs/ChangeLog.txt" rel="nofollow">complete 3.0 ChangeLog now lives here</a>.</p>

<h1>Major Known Issues</h1>

<ol>
<li>Windows XP users may still experience crashes due to <a href="https://trac.torproject.org/projects/tor/ticket/9084" rel="nofollow">Bug 9084</a>.</li>
<li>Transifex issues are still causing problems with missing translation text in some bundles</li>
</ol>

---
_comments:

<a id="comment-27458"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27458" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 01, 2013</p>
    </div>
    <a href="#comment-27458">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27458" class="permalink" rel="bookmark">&quot;In addition to providing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"In addition to providing important security updates to Firefox and Tor, these release binaries should now be exactly reproducible from the source code by anyone."</p>
<p>That is huge. Thanks and congratulations.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-28777"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-28777" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 14, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-27458" class="permalink" rel="bookmark">&quot;In addition to providing</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-28777">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-28777" class="permalink" rel="bookmark">Indeed it is.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Indeed it is.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-27496"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27496" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 02, 2013</p>
    </div>
    <a href="#comment-27496">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27496" class="permalink" rel="bookmark">What&#039;s the best way to use a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What's the best way to use a system-wide, already running tor process for the new TBB?</p>
<p>Also, congratulations on the deterministic builds!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27557"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27557" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 03, 2013</p>
    </div>
    <a href="#comment-27557">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27557" class="permalink" rel="bookmark">Hi, I&#039;m using a version of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, I'm using a version of Ubuntu Karmic discontinued. So far not had problems with it running TOR,</p>
<p>But from ALFA version 3.0 I get this error:</p>
<p>  <strong><div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Launching Tor Browser Bundle <span style="color: #b1b100;">for</span> Linux in <span style="color: #339933;">/</span>home<span style="color: #339933;">/</span>sistem<span style="color: #339933;">/</span>src<span style="color: #339933;">/</span>tor<span style="color: #339933;">-</span>browser_es<span style="color: #339933;">-</span>ES<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">XPCOMGlueLoad error <span style="color: #b1b100;">for</span> <a href="http://www.php.net/file"><span style="color: #990000;">file</span></a> <span style="color: #339933;">/</span>home<span style="color: #339933;">/</span>sistem<span style="color: #339933;">/</span>src<span style="color: #339933;">/</span>tor<span style="color: #339933;">-</span>browser_es<span style="color: #339933;">-</span>ES<span style="color: #339933;">/</span>App<span style="color: #339933;">/</span>Firefox<span style="color: #339933;">/</span>libxpcom<span style="color: #339933;">.</span>so<span style="color: #339933;">:&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">libxul<span style="color: #339933;">.</span>so<span style="color: #339933;">:</span> cannot open shared object <a href="http://www.php.net/file"><span style="color: #990000;">file</span></a><span style="color: #339933;">:</span> No such <a href="http://www.php.net/file"><span style="color: #990000;">file</span></a> or directory<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">Couldn<span style="color: #0000ff;">'t load XPCOM.&lt;br /&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #0000ff;">Tor Browser exited abnormally.  Exit code: 255</span></div></li></ol></pre></div> </strong></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27559"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27559" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 03, 2013</p>
    </div>
    <a href="#comment-27559">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27559" class="permalink" rel="bookmark">That really is huge. The</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That really is huge. The people directly responsible for that should get recognition.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27576"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27576" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 03, 2013</p>
    </div>
    <a href="#comment-27576">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27576" class="permalink" rel="bookmark">I have a feeling that the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have a feeling that the Big Brother brought entire tor network down today.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27607"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27607" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 03, 2013</p>
    </div>
    <a href="#comment-27607">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27607" class="permalink" rel="bookmark">I&#039;m not sure how to act as a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm not sure how to act as a relay with 3.0 if Vidalia isn't included. Are there instructions somewhere?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27660"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27660" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2013</p>
    </div>
    <a href="#comment-27660">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27660" class="permalink" rel="bookmark">Hi Mike,
appears</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Mike,</p>
<p>appears that<br />
$4B3FED31069ED28808DF32570BF58058E1915F47<br />
IP 37.143.8.189<br />
hands out forged certificates for Wikipedia. </p>
<p>Issued to:<br />
CN *.wikipedia.org<br />
Serial number B0:06:A1:A3</p>
<p>Issued by:<br />
main.authority.com</p>
<p>Issued On:<br />
2013-06-24</p>
<p>Fingerprint:<br />
SHA1 FB:33:6A:CC:0B:EE:CA:28:78:79:A1:2B:FF:2F:B2:A2:D3:F1:F0:34</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27661"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27661" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2013</p>
    </div>
    <a href="#comment-27661">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27661" class="permalink" rel="bookmark">Hi Mike,
just rechecked</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Mike,</p>
<p>just rechecked 37.143.8.189 and got the correct Wikipedia certificate.<br />
Seems exit nodes might have changed a moment before I checked Vidalia and therefore the forged certificate cannot be attributed to 37.143.8.189.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27664"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27664" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2013</p>
    </div>
    <a href="#comment-27664">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27664" class="permalink" rel="bookmark">Hi Mike,
this time I can</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Mike,</p>
<p>this time I can confirm that it is indeed  37.143.8.189 who hands out the forged Wikipedia certificate. This time I checked while the connections to Wikipedia were still open, pending certificate approval,<br />
and the exit node was 37.143.8.189. </p>
<p>Previously after the certificate notification had shown up for the first time, I re-checked the exit node with the dot exit notation which gave me a correct Wikipedia certificate.<br />
So it appeared the initial attribution of the forged certificate to this exit node was by mistake, but the recurrent certificate notification gave me the opportunity to confirm that the initial attribution was correct.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27665"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27665" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2013</p>
    </div>
    <a href="#comment-27665">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27665" class="permalink" rel="bookmark">I&#039;m using Windows XP and I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm using Windows XP and I confirm that Tor Browser Bundle 3.0 Alpha 2 crashes on startup due to bug 9084.  :(</p>
<p>Is _vsnprintf_s required only by the Tor patches ? Because the "vanilla" Firefox built by Mozilla works fine on my machine.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27706"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27706" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2013</p>
    </div>
    <a href="#comment-27706">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27706" class="permalink" rel="bookmark">What about adding a brief</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about adding a brief explanation of who alpha releases are intended for?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27722"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27722" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2013</p>
    </div>
    <a href="#comment-27722">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27722" class="permalink" rel="bookmark">&quot;Remove Vidalia; Use the new</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Remove Vidalia; Use the new Tor Launcher Firefox Addon instead"</p>
<p>Please provide at least one Bundle version with Vidalia.<br />
Thank you</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27723"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27723" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 04, 2013</p>
    </div>
    <a href="#comment-27723">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27723" class="permalink" rel="bookmark">May semi off-topic but TAILS</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>May semi off-topic but TAILS has closed her forum:<br />
disc volume name changed without mounting the disc.</p>
<p>Have anyone seen this strange behaviour,too  ??</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27744"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27744" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 05, 2013</p>
    </div>
    <a href="#comment-27744">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27744" class="permalink" rel="bookmark">It crashes on win8-64 a few</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It crashes on win8-64 a few seconds after start.<br />
IRC says it does not work on Win8.</p>
<p>If that is "officially" so, mention it here, please.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27747"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27747" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 05, 2013</p>
    </div>
    <a href="#comment-27747">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27747" class="permalink" rel="bookmark">looking for tor for my tablet</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>looking for tor for my tablet</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-27976"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-27976" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 07, 2013</p>
    </div>
    <a href="#comment-27976">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-27976" class="permalink" rel="bookmark">A small usability issue:
I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A small usability issue:</p>
<p>I was browsing a website with the 3.0a2 bundle, and after a while, my tor exit changed to one that had been banned from the website. When I used the tor button to get a new identity, the browser was restarted without any of the tabs that I had open.</p>
<p>My initial reaction was that changing my tor identity shouldn't close all my tabs. However, after thinking about it, changing identities without closing the page may reduce my anonymity, which would mean that it might be necessary for the new identity function to remain the way it is.</p>
<p>If possible, could a warning that changing the identity will close all tabs be added to the button? Or maybe an option to reduce anonymity and keep the tabs?</p>
<p>Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-28919"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-28919" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 15, 2013</p>
    </div>
    <a href="#comment-28919">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-28919" class="permalink" rel="bookmark">How is relaying configured</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How is relaying configured for the 3.0 release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-29279"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-29279" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 18, 2013</p>
    </div>
    <a href="#comment-29279">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-29279" class="permalink" rel="bookmark">thanku</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanku</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-29720"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-29720" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 21, 2013</p>
    </div>
    <a href="#comment-29720">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-29720" class="permalink" rel="bookmark">used the EN .exe tor bundle</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>used the EN .exe tor bundle 3.0 alpha 2. Returns error that firefox is already running and I need to terminate the process, first. Task manager shows no other instances running when I click the link, but starts an instance, itself. Running windows 7 ultimate on a virtualbox vm version 4.2.16. Installed browsers are comodo ice dragon, comodo fire dragon (part of the premium internet security package) and opera.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-30412"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-30412" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 25, 2013</p>
    </div>
    <a href="#comment-30412">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-30412" class="permalink" rel="bookmark">je télécharge browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>je télécharge browser bundle et lorsque je lance l'ordinateur un msg me dit qu'il est obsolète.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-30764"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-30764" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2013</p>
    </div>
    <a href="#comment-30764">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-30764" class="permalink" rel="bookmark">ooooo</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ooooo</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-31105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-31105" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2013</p>
    </div>
    <a href="#comment-31105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-31105" class="permalink" rel="bookmark">I have a prob with Facebook</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have a prob with Facebook login they tell me that I'm loging from other countries how can I solve this problem?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-31243"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-31243" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 30, 2013</p>
    </div>
    <a href="#comment-31243">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-31243" class="permalink" rel="bookmark">hello tor is the best</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello tor is the best</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-31807"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-31807" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 03, 2013</p>
    </div>
    <a href="#comment-31807">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-31807" class="permalink" rel="bookmark">Why would this take screen</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why would this take screen shots when running? It freezes and greys out and windows 7 says it is not responding. Zemana Antilogger pops up and says it is trying to take a screen shot, but it did this hanging before I installed Zemana.</p>
<p>I got my own screen shot of the 3.0alpha2 hanging with Zemana popup and more info (1999 date? why?) I'll do a fresh download and try again.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-31864"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-31864" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 04, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-31807" class="permalink" rel="bookmark">Why would this take screen</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-31864">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-31864" class="permalink" rel="bookmark">The same with a fresh</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The same with a fresh download. The 3.0alpha2 browser window will hang for some time, turning grey with a (not responding) notice and then after about a minute or more resume normally. During this time Zemana Antilogger will popup saying firefox.exe is trying to take a screen capture.</p>
<p>Can anyone duplicate this? I've run and run antimalware programs by following the directions at majorgeeks.com. Nothing found. So could it be a bug? It can't be a conflict with Zemana because it was hanging before installing Zemana.</p>
<p>From the Event Log:</p>
<p>The program firefox.exe version 17.0.7.0 stopped interacting with Windows and was closed. To see if more information about the problem is available, check the problem history in the Action Center control panel.</p>
<p>That could be because I tried blocking it in Zemana though. Apparently there are no events when it just hangs for a while and it is allowed by Zemana. Any suggestions?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-32313"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-32313" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 05, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-31864" class="permalink" rel="bookmark">The same with a fresh</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-32313">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-32313" class="permalink" rel="bookmark">Firefox shouldn&#039;t be taking</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Firefox shouldn't be taking screenshots! It sounds like your system has been compromised. Proceed with caution.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-32793"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-32793" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 06, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-32313" class="permalink" rel="bookmark">Firefox shouldn&#039;t be taking</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-32793">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-32793" class="permalink" rel="bookmark">Well, the only way to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, the only way to proceed with caution would be to unplug or do a fresh install.</p>
<p>I also have the 6/23 version of TBB installed and used that before 3.0alpha2<br />
but never went to any .onion sites with it I don't think and never ever went to any Freedom Hosting sites. If that Firefox vulnerability infected the machine, maybe it was able to take screen captures of any browsers running with Tor connection, even   3.0alpha2 despite its browser being secure. </p>
<p>I know nothing about this stuff so any tests or ways to check would be welcome or I'd be glad to upload files for examination. The TBB screen would also freeze with a (not responding) notice. Zemana only caught it that one day and hasn't alerted since. Lucky I got that screen shot. I'll upload it too if asked.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div>
