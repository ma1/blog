title: New Release: Tor Browser 10.0.11 (Windows Only)
---
pub_date: 2021-02-07
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.0
---
categories: applications
---
_html_body:

<p>Tor Browser 10.0.11 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.0.11/">distribution directory</a>.</p>
<p>This version updates Firefox to 78.7.1esr for Windows. This release includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-06/">security updates</a> to Firefox.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0-desktop">Desktop</a> Tor Browser 10.0.10 is:</p>
<ul>
<li>Windows
<ul>
<li>Update Firefox to 78.7.1esr</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291081"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291081" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 07, 2021</p>
    </div>
    <a href="#comment-291081">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291081" class="permalink" rel="bookmark">Why not put it on your…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why not put it on your normal download page? Why the distribution directory, which is confusing for a lot of users. Surely, unsuspecting users will still be downloading the bad version (10.0.10) with the critical security issue from your normal download page? That's not good.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291084"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291084" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 07, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291081" class="permalink" rel="bookmark">Why not put it on your…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291084">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291084" class="permalink" rel="bookmark">The web site needed some…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The web site needed some modifications, and publishing the automatic update (and this blog post) was more important than updating the Download page. In particular, a newly installed version 10.0.10 would automatically update to 10.0.11, so the consequence was small.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291083"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291083" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>opaipa (not verified)</span> said:</p>
      <p class="date-time">February 07, 2021</p>
    </div>
    <a href="#comment-291083">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291083" class="permalink" rel="bookmark">Hi there!
Why didn&#039;t you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi there!<br />
Why didn't you compile 0.4.4.7-binaries for it?</p>
<p>At least distribution directory is still pointing to<br />
- tor-win32-0.4.4.6.zip and<br />
- tor-win64-0.4.4.6.zip</p>
<p>CY</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291085"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291085" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 07, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291083" class="permalink" rel="bookmark">Hi there!
Why didn&#039;t you…</a> by <span>opaipa (not verified)</span></p>
    <a href="#comment-291085">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291085" class="permalink" rel="bookmark">Hello!
This was an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello!</p>
<p>This was an emergency update for Windows, so we didn't include any additional changes/updates.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291087"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291087" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 07, 2021</p>
    </div>
    <a href="#comment-291087">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291087" class="permalink" rel="bookmark">It seem like I got increased…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It seem like I got increased "CPU Usage" playing video at Youtube (360p). Does any one observe such performance degradation?<br />
(Also I will try to test and reproduce and post here updates concerning the issue)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291088"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291088" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nathen (not verified)</span> said:</p>
      <p class="date-time">February 07, 2021</p>
    </div>
    <a href="#comment-291088">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291088" class="permalink" rel="bookmark">sounds good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>sounds good</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291090"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291090" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>V Srinivasulu (not verified)</span> said:</p>
      <p class="date-time">February 07, 2021</p>
    </div>
    <a href="#comment-291090">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291090" class="permalink" rel="bookmark">Lovely</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Lovely</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291131"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291131" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>User (not verified)</span> said:</p>
      <p class="date-time">February 11, 2021</p>
    </div>
    <a href="#comment-291131">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291131" class="permalink" rel="bookmark">As of today (Feb.11)…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As of today (Feb.11) pluggable transport (obfs4) ceases to function on multiple (win10 x64 and x86) installations. Only direct connection to Tor through entry guard works (partially - not all the time).<br />
The only thing these installations had in common was the latest Windows Update SSU (KB4592449) and .NET security update (KB4601056) - removing these through system restore didn't improve the situation though (SSU's cannot generally be uninstalled though, so I have a hunch saying that SSU KB4592449 can be the cause). </p>
<p>Are the pluggable transports malfunctioning in v10.0.11 (starting today?), under general attack today or are we seeing or are we seeing an unwanted impact from Microsoft Updates?</p>
<p>Copy of log:</p>
<p>2/10/21, 21:19:22.789 [NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
2/10/21, 21:19:22.790 [NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
2/10/21, 21:19:22.790 [NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
2/10/21, 21:19:22.790 [NOTICE] Opening Socks listener on 127.0.0.1:9150<br />
2/10/21, 21:19:22.790 [NOTICE] Opened Socks listener on 127.0.0.1:9150<br />
2/10/21, 21:19:22.790 [WARN] CreateProcessA() failed: The system cannot find the file specified.<br />
2/10/21, 21:19:22.791 [WARN] Pluggable Transport process terminated with status code 0<br />
2/10/21, 21:19:22.791 [WARN] Failed to start process: (null)<br />
2/10/21, 21:19:22.791 [WARN] Managed proxy at 'TorBrowser\Tor\PluggableTransports\obfs4proxy.exe' failed at launch.<br />
2/10/21, 21:19:23.540 [WARN] We were supposed to connect to bridge '[obfs4 bridge IP]' using pluggable transport 'obfs4', but we can't find a pluggable transport proxy supporting 'obfs4'. This can happen if you haven't provided a ClientTransportPlugin line, or if your pluggable transport proxy stopped running.<br />
2/10/21, 21:19:23.540 [WARN] Problem bootstrapping. Stuck at 0% (starting): Starting. (Can't connect to bridge; PT_MISSING; count 1; recommendation warn; host [obfs4 bridge])<br />
2/10/21, 21:19:23.700 [WARN] We were supposed to connect to bridge '[obfs4 bridge 2 IP]' ... etc..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291176"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291176" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291131" class="permalink" rel="bookmark">As of today (Feb.11)…</a> by <span>User (not verified)</span></p>
    <a href="#comment-291176">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291176" class="permalink" rel="bookmark">CreateProcessA() failed: The…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Regarding: "CreateProcessA() failed: The system cannot find the file specified."</p>
<p>Did you receive any warnings about anti-virus quarantining or deleting any files?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291185"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291185" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-291185">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291185" class="permalink" rel="bookmark">Unfortunately, Windows…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unfortunately, Windows Security quarantines it without warnings sometimes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-291250"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291250" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Eman (not verified)</span> said:</p>
      <p class="date-time">February 25, 2021</p>
    </div>
    <a href="#comment-291250">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291250" class="permalink" rel="bookmark">Russian letters in the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Russian letters in the address bar lead to crash. Can you fix this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291398" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">March 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291250" class="permalink" rel="bookmark">Russian letters in the…</a> by <span>Eman (not verified)</span></p>
    <a href="#comment-291398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291398" class="permalink" rel="bookmark">Can you provide an example?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you provide an example?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
