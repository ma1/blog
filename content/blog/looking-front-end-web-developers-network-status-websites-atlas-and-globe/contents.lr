title: Looking for front-end web developers for network status websites Atlas and Globe
---
pub_date: 2014-07-17
---
author: karsten
---
tags:

network status
Atlas
Globe
Onionoo
web development
---
categories: network
---
_html_body:

<p>Hello front-end web developers!</p>

<p>We are looking for somebody to fork and extend one of the two main Tor network status websites <a href="https://atlas.torproject.org/" rel="nofollow">Atlas</a> or <a href="https://globe.torproject.org/" rel="nofollow">Globe</a>.</p>

<p>Here's some background: both the Atlas and the Globe website use the <a href="https://onionoo.torproject.org/" rel="nofollow">Onionoo service</a> as their data back-end and make that data accessible to mere humans.  The Onionoo service is maintained by Karsten.  Atlas was written by Arturo as proof-of-concept for the Onionoo service and later maintained (but not extended) by Philipp.  Globe was forked from Atlas by Christian who improved and maintained it for half a year, but who unfortunately disappeared a couple of weeks ago.  That leaves us with no actively maintained network status website, which is bad.</p>

<p>Want to help out?</p>

<p>Here's how: Globe has been criticized for having too much whitespace, which makes it less useful on smaller screens.  But we hear that the web technology behind Globe is superior to the one behind Atlas (we're no front-end web experts, so we can't say for sure).  A fine next step could be to fork Globe and tidy up its design to work better on smaller screens.  And there are plenty of steps after that if you look through the tickets in the Globe and Atlas component of our <a href="https://trac.torproject.org/" rel="nofollow">bug tracker</a>.  Be sure to present your fork on the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev" rel="nofollow">tor-dev@ mailing list</a> early to get feedback.  You can just run it on your own server for now.</p>

<p>The long-term goal would be to have one or more people working on a new network status website to replace Atlas and Globe.  We'd like to wait with that step until such a new website is maintained for a couple of weeks or even months though.  And even then, we may keep Atlas and Globe running for a couple more months.  But eventually, we'd like to shut them down in favor of an actively maintained website.</p>

<p>Let us know if you're interested, and we're happy to provide more details and discuss ideas with you.</p>

---
_comments:

<a id="comment-65238"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65238" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2014</p>
    </div>
    <a href="#comment-65238">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65238" class="permalink" rel="bookmark">This looks like important</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This looks like important work.  One request: to the extent possible, minimize its dependence on JavaScript.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-65309"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65309" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 18, 2014</p>
    </div>
    <a href="#comment-65309">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65309" class="permalink" rel="bookmark">&gt; But eventually, we&#039;d like</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; But eventually, we'd like to shut them down in favor of an actively maintained website.</p>
<p>Is not easiest way to find hoster, who would keep current Globe and Atlas alive?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-65598"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65598" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">July 22, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-65309" class="permalink" rel="bookmark">&gt; But eventually, we&#039;d like</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-65598">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65598" class="permalink" rel="bookmark">The problem isn&#039;t the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The problem isn't the hosting but to find somebody who has enough time to develop either Atlas or Globe.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-65310"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65310" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 18, 2014</p>
    </div>
    <a href="#comment-65310">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65310" class="permalink" rel="bookmark">How useful is a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How useful is a javascript-based front-end given that a significant minority of tor browser users choose to disable javascript?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-65533"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65533" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 21, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-65310" class="permalink" rel="bookmark">How useful is a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-65533">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65533" class="permalink" rel="bookmark">Yes, this is a fine</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, this is a fine question. See also<br />
<a href="https://trac.torproject.org/projects/tor/ticket/10407" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/10407</a></p>
<p>I think the answer is that somebody (perhaps you!) should make one of these tools work adequately well without javascript, and then it can still be fancier if it wants when javascript is available.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-65368"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65368" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 19, 2014</p>
    </div>
    <a href="#comment-65368">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65368" class="permalink" rel="bookmark">Q: How can I anonymize my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Q: How can I anonymize my normal FF's user agent and make stealthy like Tor's? I tried many addons they all failed the test! Please help!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-65534"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65534" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 21, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-65368" class="permalink" rel="bookmark">Q: How can I anonymize my</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-65534">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65534" class="permalink" rel="bookmark">Tor Browser does many things</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser does many things that you can't make your Firefox do:<br />
<a href="https://www.torproject.org/projects/torbrowser/design/" rel="nofollow">https://www.torproject.org/projects/torbrowser/design/</a><br />
including direct patches to the Firefox code.</p>
<p>So, the simple answer is to use Tor Browser, not your normal Firefox.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-65583"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65583" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 22, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-65583">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65583" class="permalink" rel="bookmark">Thanks for your answer.
I&#039;m</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for your answer.<br />
I'm only looking to change my user agent string to one that is common (windows7, FF) but so far all my attempts failed, my question is how can I change it successfully like Tor does?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-65398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65398" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 19, 2014</p>
    </div>
    <a href="#comment-65398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65398" class="permalink" rel="bookmark">Are the default bridges in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are the default bridges in TorBrowser the same of every user/download?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-65536"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65536" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 21, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-65398" class="permalink" rel="bookmark">Are the default bridges in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-65536">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65536" class="permalink" rel="bookmark">Yes. We need to make sure</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes. We need to make sure that everybody's download is the same, so they have a prayer of checking the hash / signature of it.</p>
<p>The default bridges work pretty well everywhere except China and sometimes Iran, where they learn about them after a while and block them.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-65399"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65399" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 19, 2014</p>
    </div>
    <a href="#comment-65399">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65399" class="permalink" rel="bookmark">Tor has a website list,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor has a website list, <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/www-team" rel="nofollow">https://lists.torproject.org/cgi-bin/mailman/listinfo/www-team</a></p>
<p>Why not ask them?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-65486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65486" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2014</p>
    </div>
    <a href="#comment-65486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65486" class="permalink" rel="bookmark">Thanks for this great</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for this great opportunity, I have forked the repo will try my best to come up with a better layout as a start.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-65599"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65599" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">July 22, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-65486" class="permalink" rel="bookmark">Thanks for this great</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-65599">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65599" class="permalink" rel="bookmark">Great. Feel free to ask</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great. Feel free to ask <a href="mailto:tor-dev@lists.torproject.org" rel="nofollow">tor-dev@lists.torproject.org</a> for feedback and make sure to share your progress early and often!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-67269"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-67269" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 02, 2014</p>
    </div>
    <a href="#comment-67269">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-67269" class="permalink" rel="bookmark">I think that the design</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think that the design issues are the minor ones, i've just forked the repo and the code it looks great it really is, but it depends completly on javascript. The best course of action would, in my opinion, to move the rendering to the server side that is using node. </p>
<p>Maybe we can use the ember client side application if the user has javascript enabled otherwise just display the data from the server.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-67293"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-67293" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 03, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-67269" class="permalink" rel="bookmark">I think that the design</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-67293">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-67293" class="permalink" rel="bookmark">Also check out</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Also check out <a href="https://trac.torproject.org/projects/tor/ticket/10407#comment:2" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/10407#comment:2</a></p>
<p>Please help!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
