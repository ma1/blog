title: New Release Candidate: Tor 0.4.3.4-rc
---
pub_date: 2020-04-13
---
author: nickm
---
tags: release candidate
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.3.4-rc from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely later this week.</p>
<p>This is a release candidate: unless we find new significant bugs in it, the stable release for the 0.4.3.x series will be substantially the same as this release.</p>
<p>Tor 0.4.3.4-rc is the first release candidate in its series. It fixes several bugs from earlier versions, including one affecting DoS defenses on bridges using pluggable transports.</p>
<h2>Changes in version 0.4.3.4-rc - 2020-04-13</h2>
<ul>
<li>Major bugfixes (DoS defenses, bridges, pluggable transport):
<ul>
<li>Fix a bug that was preventing DoS defenses from running on bridges with a pluggable transport. Previously, the DoS subsystem was not given the transport name of the client connection, thus failed to find the GeoIP cache entry for that client address. Fixes bug <a href="https://bugs.torproject.org/33491">33491</a>; bugfix on 0.3.3.2-alpha.</li>
</ul>
</li>
<li>Minor feature (sendme, flow control):
<ul>
<li>Default to sending SENDME version 1 cells. (Clients are already sending these, because of a consensus parameter telling them to do so: this change only affects what clients would do if the consensus didn't contain a recommendation.) Closes ticket <a href="https://bugs.torproject.org/33623">33623</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (testing):
<ul>
<li>The unit tests now support a "TOR_SKIP_TESTCASES" environment variable to specify a list of space-separated test cases that should not be executed. We will use this to disable certain tests that are failing on Appveyor because of mismatched OpenSSL libraries. Part of ticket <a href="https://bugs.torproject.org/33643">33643</a>.</li>
</ul>
</li>
<li>Minor bugfixes (--disable-module-relay):
<ul>
<li>Fix an assertion failure when Tor is built without the relay module, and then invoked with the "User" option. Fixes bug <a href="https://bugs.torproject.org/33668">33668</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (--disable-module-relay,--disable-module-dirauth):
<ul>
<li>Set some output arguments in the relay and dirauth module stubs, to guard against future stub argument handling bugs like 33668. Fixes bug <a href="https://bugs.torproject.org/33674">33674</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (build system):
<ul>
<li>Correctly output the enabled module in the configure summary. Before that, the list shown was just plain wrong. Fixes bug <a href="https://bugs.torproject.org/33646">33646</a>; bugfix on 0.4.3.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (client, IPv6):
<ul>
<li>Stop forcing all non-SocksPorts to prefer IPv6 exit connections. Instead, prefer IPv6 connections by default, but allow users to change their configs using the "NoPreferIPv6" port flag. Fixes bug <a href="https://bugs.torproject.org/33608">33608</a>; bugfix on 0.4.3.1-alpha.</li>
<li>Revert PreferIPv6 set by default on the SocksPort because it broke the torsocks use case. Tor doesn't have a way for an application to request the hostname to be resolved for a specific IP version, but torsocks requires that. Up until now, IPv4 was used by default so torsocks is expecting that, and can't handle a possible IPv6 being returned. Fixes bug <a href="https://bugs.torproject.org/33804">33804</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (key portability):
<ul>
<li>When reading PEM-encoded key data, tolerate CRLF line-endings even if we are not running on Windows. Previously, non-Windows hosts would reject these line-endings in certain positions, making certain key files hard to move from one host to another. Fixes bug <a href="https://bugs.torproject.org/33032">33032</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Flush stderr, stdout, and file logs during shutdown, if supported by the OS. This change helps make sure that any final logs are recorded. Fixes bug <a href="https://bugs.torproject.org/33087">33087</a>; bugfix on 0.4.1.6.</li>
<li>Stop closing stderr and stdout during shutdown. Closing these file descriptors can hide sanitiser logs. Fixes bug <a href="https://bugs.torproject.org/33087">33087</a>; bugfix on 0.4.1.6.</li>
</ul>
</li>
<li>Minor bugfixes (onion services v3):
<ul>
<li>Relax severity of a log message that can appear naturally when decoding onion service descriptors as a relay. Also add some diagnostics to debug any future bugs in that area. Fixes bug <a href="https://bugs.torproject.org/31669">31669</a>; bugfix on 0.3.0.1-alpha.</li>
<li>Block a client-side assertion by disallowing the registration of an x25519 client auth key that's all zeroes. Fixes bug <a href="https://bugs.torproject.org/33545">33545</a>; bugfix on 0.4.3.1-alpha. Based on patch from "cypherpunks".</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Disable our coding standards best practices tracker in our git hooks. (0.4.3 branches only.) Closes ticket <a href="https://bugs.torproject.org/33678">33678</a>.</li>
</ul>
</li>
<li>Testing:
<ul>
<li>Avoid conflicts between the fake sockets in tor's unit tests, and real file descriptors. Resolves issues running unit tests with GitHub Actions, where the process that embeds or launches the tests has already opened a large number of file descriptors. Fixes bug <a href="https://bugs.torproject.org/33782">33782</a>; bugfix on 0.2.8.1-alpha. Found and fixed by Putta Khunchalee.</li>
</ul>
</li>
<li>Testing (CI):
<ul>
<li>In our Appveyor Windows CI, copy required DLLs to test and app directories, before running tor's tests. This ensures that tor.exe and test*.exe use the correct version of each DLL. This fix is not required, but we hope it will avoid DLL search issues in future. Fixes bug <a href="https://bugs.torproject.org/33673">33673</a>; bugfix on 0.3.4.2-alpha.</li>
<li>On Appveyor, skip the crypto/openssl_version test, which is failing because of a mismatched library installation. Fix for 33643.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-287686"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287686" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2020</p>
    </div>
    <a href="#comment-287686">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287686" class="permalink" rel="bookmark">When are we going to see…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When are we going to see these changes in tor's stable release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287723"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287723" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">April 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287686" class="permalink" rel="bookmark">When are we going to see…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287723">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287723" class="permalink" rel="bookmark">Around early May, unless we…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Around early May, unless we turn up major new bugs.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287707"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287707" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jackz (not verified)</span> said:</p>
      <p class="date-time">April 18, 2020</p>
    </div>
    <a href="#comment-287707">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287707" class="permalink" rel="bookmark">Hello
How can we help TOR to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello<br />
How can we help TOR to stay strong in COVID time?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287724"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287724" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">April 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287707" class="permalink" rel="bookmark">Hello
How can we help TOR to…</a> by <span>Jackz (not verified)</span></p>
    <a href="#comment-287724">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287724" class="permalink" rel="bookmark">If you have money to spare,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the offer!  If you have money to spare, you could donate some of it to us, to fund our development and operations.  If not, there are other ways to help listed on our website at <a href="https://community.torproject.org/" rel="nofollow">https://community.torproject.org/</a> .  There are also neat ideas at our old website: <a href="https://2019.www.torproject.org/getinvolved/volunteer.html.en" rel="nofollow">https://2019.www.torproject.org/getinvolved/volunteer.html.en</a> .</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287711"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287711" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Testers... (not verified)</span> said:</p>
      <p class="date-time">April 18, 2020</p>
    </div>
    <a href="#comment-287711">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287711" class="permalink" rel="bookmark">What level of technical…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What level of technical prowess would you recommend as a minimum for running development versions of TBB?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287741"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287741" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Dave Williams (not verified)</span> said:</p>
      <p class="date-time">April 20, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287711" class="permalink" rel="bookmark">What level of technical…</a> by <span>Testers... (not verified)</span></p>
    <a href="#comment-287741">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287741" class="permalink" rel="bookmark">I really wouldn&#039;t consider…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I really wouldn't consider there to be any "minimum" technical ability to run a development version of the browser bundle. The browser bundle is already the "user-friendly" approach to Tor, so if you want to run the devleopment version, go ahead and do it. It's highly unlikely, though, that you'll notice much of a difference most of the time... but it's even more unlikely that running the dev version of the browser bundle will cause any problems for you. The development version is still a (fairly large) step removed from compiling and running the code from the actual git repository at whatever random commit you happen to clone it at. I've done that dozens of times by the way myself. To do that, you'll want to be proficient in using Linux on the command line, and unless you know at least some C there's really not going to be much of a reason why you would bother to mess with compiling the code straight from the repo anyway. That's interacting with Tor directly, at its core. The browser bundle - even the dev version - is several very significant steps away from that in the direction of user-friendliness. If you want to run the development browser bundle, go ahead and do so, regardless of your technical experience =)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287900"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287900" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>mnj (not verified)</span> said:</p>
      <p class="date-time">May 19, 2020</p>
    </div>
    <a href="#comment-287900">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287900" class="permalink" rel="bookmark">Tor Browser 9.0.10 is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser 9.0.10 is crashing on windows 7</p>
</div>
  </div>
</article>
<!-- Comment END -->
