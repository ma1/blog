title: Tor Weekly News — April 8th, 2015
---
pub_date: 2015-04-08
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the fourteenth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor 0.2.5.12 and 0.2.6.7 are out</h1>

<p>Roger Dingledine <a href="https://blog.torproject.org/blog/tor-02512-and-0267-are-released" rel="nofollow">announced</a> new releases in both the stable and alpha series of the core Tor software. Tor 0.2.5.12 and 0.2.6.7 both contain fixes for two security bugs that could be used either to crash onion services, or clients trying to visit onion services. The releases also make it harder for attackers to overwhelm onion services by launching lots of introductions. For full details, please see the release announcement.</p>

<p>The bugs fixed in these releases are not thought to affect the anonymity of Tor clients or onion services. However, they could be annoying if exploited, so onion service operators should upgrade as soon as possible, while Tor Browser users will be updated with the upcoming Tor Browser stable release.</p>

<h1>Tor Summer of Privacy — apply now!</h1>

<p>Some of Tor’s most active contributors and projects got their start thanks to Google’s <a href="https://developers.google.com/open-source/soc/?csw=1" rel="nofollow">Summer of Code</a>, in which the Tor Project has successfully participated for a number of years. This year, Google have decided to focus on encouraging newer, smaller projects, so rather than miss out on the benefits of this kind of intense coding program, Tor is launching its own Summer of Privacy, as Kate Krauss announced on the <a href="https://blog.torproject.org/blog/tor-summer-privacy-apply-now-0" rel="nofollow">Tor blog</a>.</p>

<p>The format is the same as before: students have the opportunity to work on new or existing open-source privacy projects, with financial assistance from the Tor Project and expert guidance from some of the world’s most innovative privacy and security engineers.</p>

<p>If that appeals to you (or someone you know), then see Kate’s announcement and the <a href="https://trac.torproject.org/projects/tor/wiki/org/TorSoP" rel="nofollow">official TSoP page</a> for more information on the program and how to apply. Applications close on the 17th of this month, so don’t leave it too late!</p>

<h1>Should onion services disclose how popular they are?</h1>

<p>Even on the non-private web, it is not possible by default to determine how popular a certain website is. Search engines and third-party tracking toolbars might be able to estimate the number of visitors a website gets, but otherwise the information is only available to the site’s operators or to groups who are able to measure DNS requests (as well as anyone in a position to eavesdrop on those two).</p>

<p>On the tor-dev mailing list, George Kadianakis posted a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-April/008597.html" rel="nofollow">detailed exploration</a> of this issue considered from the perspective of Tor onion services. If improvements and additions to the onion service design would as a side effect give an observer an idea of how popular a certain service is, should this be considered a security risk?</p>

<p>Some of the arguments put forward for the inclusion of popularity-leaking features are that they enable the collection of useful statistics; that they allow further optimization of the onion service design; and that concealing onion service popularity might not be necessary or even possible.</p>

<p>On the other hand, disclosing popularity might help an adversary decide where to aim its attacks; it may not actually offer significant performance or research benefits; and it may surprise onion service users and operators who assume that onionspace popularity is no easier to discover than on the non-private web.</p>

<p>“I still am not 100% decided here, but I lean heavily towards the ‘popularity is private information and we should not reveal it if we can help it’ camp, or maybe in the ‘there needs to be very concrete positive outcomes before even considering leaking popularity’”, writes George. “Hence, my arguments will be obviously biased towards the negatives of leaking popularity. I invite someone from the opposite camp to articulate better arguments for why popularity-hiding is something worth sacrificing.”</p>

<p>Please see George’s analysis for in-depth explanations of all these points and more, and feel free to contribute with your own thoughts.</p>

<h1>More monthly status reports for March 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of March continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000789.html" rel="nofollow">Georg Koppen</a> (for work on Tor Browser), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000790.html" rel="nofollow">David Goulet</a> and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000794.html" rel="nofollow">George Kadianakis</a> (working on onion services), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000791.html" rel="nofollow">Griffin Boyce</a> (with news on secure software distribution, onion service setup, and Tails), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000792.html" rel="nofollow">Sherief Alaa</a> (with updates about support and Arabic localization), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000795.html" rel="nofollow">Leiah Jansen</a> (working on communication and graphic design), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000799.html" rel="nofollow">Sebastian Hahn</a> (improving testability and fixing website issues), and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000801.html" rel="nofollow">Sukhbir Singh</a> (for work on TorBirdy and Tor Messenger).</p>

<p>Mike Perry reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000793.html" rel="nofollow">Tor Browser team</a>, while George Kadianakis did so for <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000796.html" rel="nofollow">SponsorR work</a>, Israel Leiva for the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000797.html" rel="nofollow">GetTor project</a>, and Colin C. for the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000798.html" rel="nofollow">Tor help desk</a>.</p>

<h1>Miscellaneous news</h1>

<p>Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-April/004298.html" rel="nofollow">announced</a> version 15 beta 1 of Orbot, which is “functionality complete”. “The main area for testing is using the Apps VPN mode while switching networks and/or in bad coverage, as well as using it in combination with Meek or Obfs4, for example. Also, the implementation is bit different between Android 4.x and 5.x, so please report any difference you might see there.”</p>

<p>Nathan also <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-April/004300.html" rel="nofollow">shared</a> Amogh Pradeep’s analysis of the network calls made in the latest version of the Firefox for Android source code, “to get our Orfox effort started again”.</p>

<h1>This week in Tor history</h1>

<p>A year ago this week, Nathan Freitas <a href="https://lists.torproject.org/pipermail/tor-talk/2014-April/032574.html" rel="nofollow">reported</a> that the number of Orbot users in Turkey had quadrupled in the previous month, after an order by the Turkish government to block access to several popular social media websites led to a <a href="https://metrics.torproject.org/userstats-relay-country.html?graph=userstats-relay-country&amp;start=2014-01-08&amp;end=2014-04-08&amp;country=tr&amp;events=off" rel="nofollow">surge in Tor connections</a>.  This week, the <a href="https://twitter.com/guardianproject/status/585114389826502656" rel="nofollow">same thing happened</a> (albeit more briefly), leading to another <a href="https://metrics.torproject.org/userstats-bridge-country.html?graph=userstats-bridge-country&amp;start=2015-03-15&amp;end=2015-04-08&amp;country=tr" rel="nofollow">increase in Tor use</a> within Turkey.</p>

<p>The best time to prepare for these censorship events is before they happen — and that includes letting people around you know what they should do to ensure their freedom of expression remains uninterrupted. Show them the <a href="https://blog.torproject.org/blog/releasing-tor-animation" rel="nofollow">Tor animation</a> and <a href="https://blog.torproject.org/blog/spread-word-about-tor" rel="nofollow">Tor brochures</a>, help them install <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">Tor Browser</a> and <a href="https://guardianproject.info/apps/orbot/" rel="nofollow">Orbot</a>, and teach them how to configure their social media applications to <a href="https://guardianproject.info/2012/05/02/orbot-your-twitter/" rel="nofollow">connect over Tor</a>.  If you make a habit of browsing over Tor, you may not even have to take any notice when things get blocked!</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, nicoo, and Roger Dingledine.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

