title: Join the Tor Localization Hackathon November 6 - 9
---
pub_date: 2020-10-09
---
author: ggus
---
tags:

localization
community
---
categories: community
---
summary:

Between November 6 and 9, the Tor Project and Localization Lab will host the first edition of Tor Project's localization hackathon, the Tor L10n Hackaton. 
---
_html_body:

<p>Between November 6 and 9, the Tor Project and <a href="https://www.localizationlab.org/">Localization Lab</a> will host the first edition of Tor Project's localization hackathon, the Tor L10n Hackathon. A hackathon is an event where a community hangs out and works together to update, fix, and collaborate on a project. The L10n Hackathon is a totally remote and online event.</p>
<p>In this localization hackathon we're going to work exclusively on the localization of our latest resource, the <a href="https://community.torproject.org/">Tor Community portal</a>. The Community portal is organized into sections: Training, Outreach, Onion Services, Localization, User Research, and Relay Operations. Each section helps users understand how they can get involved in each of these activities to build and strengthen the community supporting the Tor Project.</p>
<p>Localization of Tor Browser and the <a href="https://community.torproject.org/">Community portal</a> are important. Only a minority of internet users are native or second-language speakers of English, however censorship and surveillance are global challenges that affect us all on a daily basis. Ensuring Tor Browser and Tor resources are available in as many languages as possible removes a large barrier to access for those in need of a more secure, anonymous online presence and those who would like to contribute to Tor Project. Tor Browser is currently available to <a href="https://www.torproject.org/download/languages/">download in </a><a href="https://www.torproject.org/download/languages/">28 languages</a>, and our main website can be read in 11 languages. To support users and build community in all of these languages, it's important to also have the Tor website, Support portal and Community portal localized.</p>
<p> </p>
<p>During the event, we will have a live Q&amp;A with Tor Project core contributors and open only for the hackathon participants.</p>
<p>Join us!</p>
<p>To participate in the Tor L10n Hackaton</p>
<ol class="list-number1" start="1">
<li>Read and agree to the Tor Project <a href="https://gitweb.torproject.org/community/policies.git/tree/code_of_conduct.txt">Code of Conduct</a>.</li>
</ol>
<ol class="list-number1" start="2">
<li><a href="https://survey.torproject.org/index.php/453114?lang=en">Register</a> to get event updates and gain access to our l10n communication channel, where we will be collaborating throughout the virtual event.</li>
</ol>
<ol class="list-number1" start="3">
<li>Create an account and join <a href="https://community.torproject.org/localization/becoming-tor-translator/">Transifex</a>.</li>
</ol>
<ol class="list-number1" start="4">
<li>Take a look at the Tor Project <a href="https://wiki.localizationlab.org/index.php/Tor#Localization_Resources">localization resources</a> to get up to speed on the Tor Project terminology, translation style, and tone. Check out our <a href="https://support.torproject.org/glossary/">Glossary</a>!</li>
</ol>
<ol class="list-number1" start="5">
<li>This isn't a requirement, but: If you talk about the the hackathon on social media, we're using #TorL10nHackathon.</li>
</ol>
<p>For more details on how you can contribute, check out the Localization section in the <a href="https://community.torproject.org/localization">Community portal</a> and the <a href="https://wiki.localizationlab.org/index.php/Tor">Localization Lab wiki</a>.</p>
<p>We are a small nonprofit with a big mission, and we sincerely appreciate your help getting our documentation up to speed. We look forward to working with you soon.</p>

---
_comments:

<a id="comment-289923"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289923" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Sraboni Das (not verified)</span> said:</p>
      <p class="date-time">October 15, 2020</p>
    </div>
    <a href="#comment-289923">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289923" class="permalink" rel="bookmark">I would like to join in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would like to join in Hackathon</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290148"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290148" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>john smith  (not verified)</span> said:</p>
      <p class="date-time">October 30, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289923" class="permalink" rel="bookmark">I would like to join in…</a> by <span>Sraboni Das (not verified)</span></p>
    <a href="#comment-290148">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290148" class="permalink" rel="bookmark">Cant wait. Thank you.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Cant wait. Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
