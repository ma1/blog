title: Tor Browser 8.0a6 is released
---
pub_date: 2018-04-20
---
author: boklm
---
tags:

tor browser
tbb
tbb-8.0
---
categories: applications
---
_html_body:

<p>Tor Browser 8.0a6 is now available from the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha">Tor Browser Project page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/8.0a6/">distribution directory</a>.</p>
<p>This release includes newer versions of Tor (<a href="https://blog.torproject.org/tor-0335-rc-released">0.3.3.5-rc</a>), OpenSSL (1.0.2o), HTTPS Everywhere (2018.4.11) and NoScript (5.1.8.5). Among other things we fixed the <a href="https://trac.torproject.org/projects/tor/ticket/21537">issue with secure cookies</a> which were not working on http .onion pages, we made it possible to run Tor Browser <a href="https://trac.torproject.org/projects/tor/ticket/20283">without a /proc filesystem</a> and we updated the GCC we use for building the Windows and Linux versions to 6.4.0.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt">changelog</a> since Tor Browser 8.0a5 is:</p>
<ul>
<li>All platforms
<ul>
<li>Update Tor to 0.3.3.5-rc</li>
<li>Update OpenSSL to 1.0.2o</li>
<li>Update Torbutton to 1.9.9.1
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/25126">Bug 25126</a>: Make about:tor layout responsive</li>
<li>Translations update</li>
</ul>
</li>
<li>Update HTTPS Everywhere to 2018.4.11</li>
<li>Update NoScript to 5.1.8.5</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21537">Bug 21537</a>: Mark .onion cookies as secure</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/21850">Bug 21850</a>: Update about:tbupdate handling for e10s</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/25721">Bug 25721</a>: Backport patches from Mozilla's bug 1448771</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20283">Bug 20283</a>: Tor Browser should run without a `/proc` filesystem.</li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/13893">Bug 13893</a>: Make EMET compatible with Tor Browser</li>
</ul>
</li>
<li>Build System
<ul>
<li>Windows
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/25420">Bug 25420</a>: Update GCC to 6.4.0</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20302">Bug 20302</a>: Fix FTE compilation for Windows with GCC 6.4.0</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/25304">Bug 25304</a>: Update GCC to 6.4.0</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-274953"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274953" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 20, 2018</p>
    </div>
    <a href="#comment-274953">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274953" class="permalink" rel="bookmark">At startup:
[NoScript C] …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>At startup:<br />
[NoScript C] [Exception... "Component returned failure code: 0x80070057 (NS_ERROR_ILLEGAL_VALUE) [nsIMessageSender.sendAsyncMessage]"  nsresult: "0x80070057 (NS_ERROR_ILLEGAL_VALUE)"  location: "JS frame :: chrome://noscript/content/UISync.jsm?1cbisvf28f5vosfn62gs :: sync :: line 205"  data: no]<a href="mailto:sync@chrome" rel="nofollow">sync@chrome</a>://noscript/content/UISync.jsm?1cbisvf28f5vosfn62gs:205:7<br />
scheduleSync/&lt;@chrome://noscript/content/UISync.jsm?1cbisvf28f5vosfn62gs:185:33<br />
<a href="mailto:Thread._delayRunner@chrome" rel="nofollow">Thread._delayRunner@chrome</a>://noscript/content/Thread.js?1cbisvf28f5vosfn62gs:88:7</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274954"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274954" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 20, 2018</p>
    </div>
    <a href="#comment-274954">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274954" class="permalink" rel="bookmark">Houston, we really have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Houston, we really have problems with videos on Windows! Check <a href="https://www.youtube.com/watch?v=rZDiYgQAXqs" rel="nofollow">https://www.youtube.com/watch?v=rZDiYgQAXqs</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274979"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274979" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274954" class="permalink" rel="bookmark">Houston, we really have…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274979">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274979" class="permalink" rel="bookmark">Not just Windows, they don’t…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not just Windows, they don’t play on Linux either.  If I click pause followed by play, the twirly circle goes away and YouTube videos do start.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274986"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274986" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">April 23, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274954" class="permalink" rel="bookmark">Houston, we really have…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274986">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274986" class="permalink" rel="bookmark">Interesting and thanks for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Interesting and thanks for the report. I've opened <a href="https://trac.torproject.org/projects/tor/ticket/25898" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/25898</a> for investigation.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274955"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274955" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>da (not verified)</span> said:</p>
      <p class="date-time">April 20, 2018</p>
    </div>
    <a href="#comment-274955">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274955" class="permalink" rel="bookmark">yo</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>yo</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274956"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274956" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274956">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274956" class="permalink" rel="bookmark">httpse:
item is undefined …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>httpse:<br />
item is undefined  ux.js:44<br />
     moz-extension://5be9576e-e1ea-47f8-a97e-d69d52e62577/pages/options/ux.js:44:5<br />
    runSafeSyncWithoutClone resource://gre/modules/ExtensionUtils.jsm:71:14<br />
    runSafeWithoutClone resource://gre/modules/ExtensionCommon.jsm:133:38<br />
    wrapPromise/</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274957"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274957" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274957">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274957" class="permalink" rel="bookmark">httpse disable/enable and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>httpse disable/enable and all settings on its page are lost (probably, not only this breakage persists):<br />
Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to moz-extension://5be9576e-e1ea-47f8-a97e-d69d52e62577/background-scripts/util.js.Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to moz-extension://5be9576e-e1ea-47f8-a97e-d69d52e62577/pages/options/ux.js.Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to resource://gre/modules/ExtensionUtils.jsm.<br />
Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to resource://gre/modules/ExtensionCommon.jsm.Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental effects to the end user’s experience. For more help <a href="http://xhr.spec.whatwg.org/" rel="nofollow">http://xhr.spec.whatwg.org/</a>  util.js:48:2<br />
XML Parsing Error: syntax error<br />
Location: jar:file:///C:/Tor%20Browser%20Alpha/Browser/TorBrowser/Data/Browser/profile.default/extensions/<a href="mailto:https-everywhere-eff@eff.org.xpi" rel="nofollow">https-everywhere-eff@eff.org.xpi</a>!/rules/default.rulesets<br />
Line Number 1, Column 1:  default.rulesets:1:1<br />
Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to moz-extension://5be9576e-e1ea-47f8-a97e-d69d52e62577/background-scripts/util.js.<br />
Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to jar:file:///C:/Tor%20Browser%20Alpha/Browser/TorBrowser/Data/Browser/profile.default/extensions/<a href="mailto:https-everywhere-eff@eff.org.xpi" rel="nofollow">https-everywhere-eff@eff.org.xpi</a>!/rules/default.rulesets.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274958"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274958" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274958">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274958" class="permalink" rel="bookmark">even torbutton&#039;s options:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>even torbutton's options:<br />
TypeError: window.opener.torbutton_show_torbrowser_manual is not a function[Learn More]  preferences.js:67:21<br />
    torbutton_set_learn_more_links chrome://torbutton/content/preferences.js:67:21<br />
    torbutton_init_security_ui chrome://torbutton/content/preferences.js:47:3<br />
    onload chrome://torbutton/content/preferences.xul:1:1<br />
    gViewController.commands.cmd_showItemPreferences.doCommand chrome://mozapps/content/extensions/extensions.js:1215:9<br />
    gViewController.doCommand chrome://mozapps/content/extensions/extensions.js:1552:5<br />
    initialize/&lt; chrome://mozapps/content/extensions/extensions.js:182:5<br />
Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to chrome://torbutton/content/preferences.js.Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to chrome://torbutton/content/preferences.xul.<br />
Security Error: Content at moz-nullprincipal:{51f67c32-664c-470e-9475-f58699371c72} may not load or link to chrome://mozapps/content/extensions/extensions.js.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274959"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274959" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274959">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274959" class="permalink" rel="bookmark">trying to test about:tor in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>trying to test about:tor in resp design mode and:<br />
TypeError: win.gBrowser is undefined[Learn More]  ReaderParent.jsm:85:1<br />
TypeError: cannot use the given object as a weak map key  RemoteAddonsParent.jsm:1076:7<br />
Unchecked lastError value: Error: Invalid tab ID: 12 ExtensionCommon.jsm:265</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274983"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274983" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">April 23, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274959" class="permalink" rel="bookmark">trying to test about:tor in…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274983">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274983" class="permalink" rel="bookmark">How did you test it? What…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How did you test it? What exactly did you do?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274960"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274960" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274960">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274960" class="permalink" rel="bookmark">Bug 21537: Mark .onion…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bug 21537: Mark .onion cookies as secure<br />
 Consider ignoring secure cookies for .onion addresses </p>
<p>Vague wordings are hiding the truth:<br />
Even secure cookies are sent over HTTP now for .onion domains.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274961"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274961" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274961">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274961" class="permalink" rel="bookmark">10:45:24.900 Protocol error …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>10:45:24.900 Protocol error (noSuchActor): No such actor for ID: server1.conn1.child1/memoryActor12 1 torbutton.js:883</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274962"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274962" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Someone (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274962">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274962" class="permalink" rel="bookmark">Good, thanks
and remember a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good, thanks<br />
and remember a new version of Firefox ESR will be available next month!</p>
<p>    2018-05-09: Firefox 60, Firefox ESR 60, Firefox ESR 52.8<br />
    2018-06-26: Firefox 61, Firefox ESR 60.1, Firefox ESR 52.9<br />
    2018-08-21: Firefox 62, Firefox ESR 60.2</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274998"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274998" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 23, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274962" class="permalink" rel="bookmark">Good, thanks
and remember a…</a> by <span>Someone (not verified)</span></p>
    <a href="#comment-274998">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274998" class="permalink" rel="bookmark">Does Tor plan on riding 52…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does Tor plan on riding 52 ESR over the cliff to try to avoid XP breakage and the dreaded Quantum platform?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-275044"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275044" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">April 26, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274998" class="permalink" rel="bookmark">Does Tor plan on riding 52…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-275044">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275044" class="permalink" rel="bookmark">No. We&#039;ll switch to ESR 60…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No. We'll switch to ESR 60 for desktop once there are no security updates for ESR 52 anymore.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-274966"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274966" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>stu (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274966">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274966" class="permalink" rel="bookmark">great work as always people,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>great work as always people,<br />
thx for trying to keep us safe</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274973"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274973" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>C3PO (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274973">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274973" class="permalink" rel="bookmark">Customize isn&#039;t working…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Customize isn't working again on this version... plz fix this one. Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274977"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274977" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">April 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274973" class="permalink" rel="bookmark">Customize isn&#039;t working…</a> by <span>C3PO (not verified)</span></p>
    <a href="#comment-274977">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274977" class="permalink" rel="bookmark">We have this ticket:https:/…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have this ticket:<br />
<a href="https://trac.torproject.org/projects/tor/ticket/25458" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/25458</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274974"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274974" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>fred (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274974">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274974" class="permalink" rel="bookmark">Toolbar customization does…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Toolbar customization does not work (tor-browser-linux64-8.0a6_en-US.tar.xz) Menu button &gt; Customize. Panel opens briefly then closes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274978"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274978" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">April 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274974" class="permalink" rel="bookmark">Toolbar customization does…</a> by <span>fred (not verified)</span></p>
    <a href="#comment-274978">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274978" class="permalink" rel="bookmark">We have this ticket:https:/…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have this ticket:<br />
<a href="https://trac.torproject.org/projects/tor/ticket/25458" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/25458</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274980"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274980" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>mikiguru.com (not verified)</span> said:</p>
      <p class="date-time">April 22, 2018</p>
    </div>
    <a href="#comment-274980">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274980" class="permalink" rel="bookmark">Nice update</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nice update</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274999"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274999" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Video (not verified)</span> said:</p>
      <p class="date-time">April 23, 2018</p>
    </div>
    <a href="#comment-274999">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274999" class="permalink" rel="bookmark">hi is onionshare and going…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi is onionshare and going to use the v3 onion services? are any tor instant message etc. programs currently or in the near future supporting them? thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-275077"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275077" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">April 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274999" class="permalink" rel="bookmark">hi is onionshare and going…</a> by <span>Video (not verified)</span></p>
    <a href="#comment-275077">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275077" class="permalink" rel="bookmark">Here is a ticket tracking…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here is a ticket tracking the implementation of V3 onion support for Onionshare.<br />
<a href="https://github.com/micahflee/onionshare/issues/677" rel="nofollow">https://github.com/micahflee/onionshare/issues/677</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-275040"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275040" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 26, 2018</p>
    </div>
    <a href="#comment-275040">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275040" class="permalink" rel="bookmark">Bug 13893: Make EMET…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bug 13893: Make EMET compatible with Tor Browser<br />
haha, and Windows 10 too, please!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-275045"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275045" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">April 26, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-275040" class="permalink" rel="bookmark">Bug 13893: Make EMET…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-275045">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275045" class="permalink" rel="bookmark">If you have issues with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you have issues with getting Tor Browser to run on Windows 10, please open a ticket on our bug tracker (<a href="https://trac.torproject.org" rel="nofollow">https://trac.torproject.org</a>), describing what the problem is and steps to reproduce it. So far we are not aware of any Windows 10 related incompatibilities.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-275282"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275282" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Siti (not verified)</span> said:</p>
      <p class="date-time">May 10, 2018</p>
    </div>
    <a href="#comment-275282">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275282" class="permalink" rel="bookmark">the may tor browser very…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the may tor browser very slow for download</p>
</div>
  </div>
</article>
<!-- Comment END -->
