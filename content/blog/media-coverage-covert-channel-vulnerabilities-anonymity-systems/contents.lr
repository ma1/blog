title: Media coverage of "Covert channel vulnerabilities in anonymity systems"
---
pub_date: 2008-02-03
---
author: sjmurdoch
---
tags:

tor
covert channels
attacks
media coverage
---
categories: network
---
_html_body:

<p>Over the past few days there has been some coverage of my <a href="http://www.lightbluetouchpaper.org/2007/12/10/covert-channel-vulnerabilities-in-anonymity-systems/" rel="nofollow">PhD thesis</a>, and its relationship to Tor, on blogs and online news sites. It seems like this wave started with a <a href="http://mcpmag.com/columns/article.asp?editorialsid=2470" rel="nofollow">column</a> by Russ Cooper, which triggered articles in <a href="http://www.pcworld.com/article/id,142094-pg,1/article.html" rel="nofollow">PC World</a> and <a href="http://www.darkreading.com/document.asp?doc_id=144606&amp;WT.svl=news2_3" rel="nofollow">Dark Reading</a>. The media attention came as a bit of a surprise to me, since nobody asked to interview me over this. I'd encourage other journalists writing about Tor to contact someone from the project as we're happy to help give some context.</p>

<p>My thesis is a fairly diverse collection of work, but the articles emphasize the impact of the attacks I discuss on users of anonymity networks like Tor. Actually, my thesis doesn't aim to show that Tor is insecure; the reason I selected Tor as a test case was that it's one of the few (and by far the largest) low-latency system that aims to stand up to observation.  Other, simpler, systems have comparatively well understood weaknesses, and so there is less value in researching them.</p>

<p>Quantifying the security of anonymity systems is a difficult question and still being actively worked on. Comparing different systems is even harder since they make different assumptions on the capabilities of attackers (the “threat model”). The mere chance of attacks doesn't indicate that a system is insecure, since they might make assumptions about the environment that are not met, or are insufficiently reliable for the scenario being considered.</p>

<p>The actual goal of my thesis was try to better understand the strengths and weaknesses of systems like Tor, but more importantly to also to suggest a more general methodology for discovering, and resolving flaws. I proposed that the work from the well-established field of <a href="http://en.wikipedia.org/wiki/Covert_channel" rel="nofollow">covert channels</a> could be usefully applied, and used examples, including Tor, to justify this.</p>

<p>There remains much work to be done before it's possible to be sure how secure anonymity systems are, but hopefully this framework will be a useful one in moving forward. Since in September 2007 I joined the Tor project, I hope I'll also help in other ways too.</p>

---
_comments:

<a id="comment-8"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2008</p>
    </div>
    <a href="#comment-8">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8" class="permalink" rel="bookmark">hello</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hope Tor it will come up with a better anonymity,these days you don't know who to trust.</p>
</div>
  </div>
</article>
<!-- Comment END -->
