title: Workshop: Explorar o Tor em Lisboa, Portugal (Meetup: Explore Tor in Lisbon, Portugal)
---
pub_date: 2018-06-20
---
author: steph
---
tags:

meetup
portugal
---
categories: community
---
summary: Reclaiming your privacy on the internet may seem to be an overwhelming task. You aren't powerless, however. Next week, we're holding a meetup in Lisbon, Portugal to talk about how Tor can help.
---
_html_body:

<p> </p>
<p>[English version below]</p>
<p><span id="gmail-FSGcaller1" style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Recuperar a privacidade na internet pode parecer extremamente difícil, mas na verdade não é: na próxima semana daremos um workshop em Lisboa, Portugal em que explicaremos como é que o <span id="eL_2_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span> pode ajudar nesse sentido.</font></span></p>
<p>Juntem-se a nós para aprenderem como é que o <span id="eL_3_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span> funciona e como é que pode ser usado para melhorar a privacidade pessoal na internet. Vamos explicar como é que o <span id="eL_4_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span> consegue atingir um elevado grau de privacidade para os seu utilizadores, como utilizar o <a href="https://www.torproject.org/download/download"><span id="eL_7_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span> Browser</a>, bem como outras maneiras como o <span id="eL_8_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span> pode melhorar a nossa privacidade. Depois responderemos a questões sobre o <span id="eL_9_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span>, o <span id="eL_10_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span> Browser e outros tópicos relacionados com a privacidade e anonimato na internet. Se o tempo chegar, <span id="eL_11_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">aboradaremos</font></span> também o tema de como correr <a href="https://blog.torproject.org/new-guide-running-tor-relay">um <span id="eL_15_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">relay</font></span> do <span id="eL_16_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span></a>.</p>
<p>O workshop será dado por Kevin <span id="eL_17_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Gallagher</font></span>, um candidato a Doutoramento do Centro para Cibersegurança da Universidade de Nova Iorque e que colaborou no <span id="eL_18_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">meetup</font></span> <a href="https://blog.torproject.org/explore-tor-nyc-meetup-feb-15">Explore <span id="eL_22_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Tor</font></span>, <span id="eL_23_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">NYC</font></span>!</a>, e Francisco Core, um estudante de engenharia informática no Instituto Superior Técnico. O workshop será em Inglês, mas as questões poderão também ser feitas em Português. Poderemos também facultar alguma tradução durante o evento.</p>
<h2><font class="gmail-FSG_texto">Inscrição</font></h2>
<p><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Apesar de preferirmos eventos sem registo prévio, o espaço onde ser realizará requer inscrição para terem noção do número de participantes. Para se inscrever, envie um <span id="eL_24_texto" style="padding-left:1px"><font class="gmail-FSG_erroSintatico">e-mail</font></span> para <span id="eL_25_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico"><a href="mailto:hemeroteca@cm-lisboa.pt">hemeroteca@cm-lisboa.pt</a></font></span>. Não há necessidade de dar informação pessoal durante a inscrição. Se se sentir desconfortável em contactar a biblioteca, pode contactar os organizadores: Kevin <span id="eL_26_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Gallagher</font></span> em <span id="eL_27_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico"><a href="mailto:kevin.gallagher@nyu.edu">kevin.gallagher@nyu.edu</a></font></span> ou Francisco Core em <span id="eL_28_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico"><a href="mailto:francisco.core@protonmail.com">francisco.core@protonmail.com</a></font></span>, referindo que se quer inscrever.</font></span></font></p>
<h2><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Onde e Quando</font></span></font></h2>
<p><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Quinta-feira, 28 de Junho às <span id="eL_29_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">15:30h</font></span><br />
<span id="eL_30_texto" style="padding-left:1px"><font class="gmail-FSG_erroSintatico">na</font></span> Hemeroteca Municipal<br />
Rua <span id="eL_31_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">Lucio</font></span> de Azevedo, <span id="eL_32_texto" style="padding-left:1px"><font class="gmail-FSG_erroOrtografico">21B</font></span><br />
Lisboa, Portugal</font></span></font></span></font></p>
<p>O workshop é gratuito</p>
<hr />
<hr />
<p><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Reclaiming your privacy on the internet may seem to be an overwhelming task. You aren't powerless, however. Next week, we're holding a meetup in Lisbon, Portugal to talk about how Tor can help.</font></span></font></p>
<p>Join us to learn about how Tor works and how it can be used to enhance your personal privacy on the internet. We will discuss how Tor achieves a high degree of privacy for its users, how to use <a href="https://www.torproject.org/download/download">Tor Browser</a>, and other uses of Tor for enhancing privacy. After this, we will answer questions you may have about Tor, Tor Browser, and other topics pertaining to privacy and anonymity on the internet. If time permits, we will also discuss <a href="https://blog.torproject.org/new-guide-running-tor-relay">how to run a Tor relay</a>.</p>
<p>This workshop will be lead by Kevin Gallagher, a PhD candidate at New York University's Center for Cyber Security who has also helped with <a href="https://blog.torproject.org/explore-tor-nyc-meetup-feb-15">Explore Tor, NYC!</a> meetups, and Francisco Core, a computer science student at Instituto Superior Tecnico. The workshop will be primarily in English, but questions will be taken and addressed in both English and Portuguese. Some translation will be available.</p>
<h2><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Signing up</font></span></font></h2>
<p><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Though we prefer events not to be conducted this way, the venue requires that people who wish to attend the event sign up so they have an idea of how many people expect. To sign up, send an e-mail to <a class="moz-txt-link-abbreviated" href="mailto:hemeroteca@cm-lisboa.pt">hemeroteca@cm-lisboa.pt</a>. There is no requirement to provide identifying information when singing up. If you feel uncomfortable contacting the library, you can contact Kevin Gallagher at <a class="moz-txt-link-abbreviated" href="mailto:kevin.gallagher@nyu.edu">kevin.gallagher@nyu.edu</a> or Francisco Core at <a class="moz-txt-link-abbreviated" href="mailto:francisco.core@protonmail.com">francisco.core@protonmail.com</a> to state that you wish to sign up.</font></span></font></p>
<h2><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Where &amp; When</font></span></font></h2>
<p><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto">Thursday, June 28th at 3:30 PM<br />
Hemeroteca Municipal<br />
Rua Lucio de Azevedo, 21B<br />
Lisboa, Portugal</font></span></font></p>
<p>There is no admission fee for this event.</p>
<p><font class="gmail-FSG_texto"><span style="display:inline-block; width:100%"><font class="gmail-FSG_texto"> </font></span></font></p>

---
_comments:

<a id="comment-275866"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275866" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Kevin (not verified)</span> said:</p>
      <p class="date-time">June 26, 2018</p>
    </div>
    <a href="#comment-275866">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275866" class="permalink" rel="bookmark">To my understanding, the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To my understanding, the workshop has filled up. We're working with the library to make more workshops to meet the demand!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276007"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276007" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 29, 2018</p>
    </div>
    <a href="#comment-276007">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276007" class="permalink" rel="bookmark">Been there! Great time,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Been there! Great time, thanks Francisco and Kevin!<br />
We need more Tor presence in Portugal, Tor Project is invited to come down here to our little lovely country ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277549"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277549" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>RiS (not verified)</span> said:</p>
      <p class="date-time">September 24, 2018</p>
    </div>
    <a href="#comment-277549">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277549" class="permalink" rel="bookmark">Olá
Estou no Brasil, e…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Olá<br />
Estou no Brasil, e preciso de um exitnode em Portugal.<br />
Quando faço a alteração no arquivo torcc para ExitNodes {pt} StrictNodes 1, o Tor fica rodando, mas não inicia.<br />
Testei para outros países, e funciona normalmente.<br />
Alguém pode me ajudar?</p>
</div>
  </div>
</article>
<!-- Comment END -->
