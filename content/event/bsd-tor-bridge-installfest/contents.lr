title: *BSD Tor Bridge Installfest
---
author: steph
---
start_date: 2017-10-04
---
body:

6:45pm, LMHQ, 150 Broadway, 20th Floor, Manhattan

There is one glaring weakness about the Tor network: an overwhelming dominance of Linux-based nodes. Since March 2015, [The Tor BSD Diversity Project](https://torbsd.github.io/) has worked to rectify this operating system monoculture.

TDP managed a number of feats, including porting Tor Browser to OpenBSD.

For this hands-on installfest, the goal is to start addressing the massive monoculture in Tor bridges, which serve as private gateways for users blocked from the Tor network.

That monoculture is stark as the [TDP statistics illustrate](http://torbsd.github.io/oostats.html). [Bridge operating system diversity](http://torbsd.github.io/oostats/bridges-bw-by-os.txt) is even worse than for public relays.

Bridges are ideal services to run from a residential network. Many BSD users in New York City maintain fast, underutilized internet connections that can easily help increase diversity. As Tor bridge IPs are not publicly listed, there is little worry about geting any flack from internet service providers.

Popular small embedded systems, from armv7 BeagleBones to amd64 APU boards, are ideal hardware platforms for a residential bridge. Each of the BSD projects provide strong support for an array of small systems.

This meeting will feature a brief introduction to TDP, a quick overview of some diversity statistics, followed by hands-on configuration of hardware on-hand.

To make this installfest worthwhile, come prepared with:

* appropriate hardware to install the BSD of your choice on, with appropriate cables and install media
* an IP address reserved on your private residential network for the Tor bridge

Adequate power and bandwidth will be available, along with other NYC*BUG attendees ready and willing to assist.

*Speaker Bio*

[The Tor BSD Diversity Project](https://torbsd.github.io/) launched in March 2015 to inject more *BSD into the Tor public anonymity network. Since then, TDP accomplished a number of important milestones, including porting Tor Browser to OpenBSD with a current effort to port TB to FreeBSD.

Website:
[*BSD Tor Bridge Installfest, The Tor BSD Diversity Project](http://www.nycbug.org/index.cgi?action=view&id=10654)
