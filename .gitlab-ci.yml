---
variables:
  SITE_URL: blog.torproject.org
  STAGING_URL: blog.staging.torproject.net
  LEKTOR_BUILD_FLAGS: alt-sidebar convert-webp
  LEKTOR_PARTIAL_BUILD: 1
  CI_CHECK_LFS: 1

include:
  - project: tpo/tpa/ci-templates
    file:
      - lektor.yml
      - static-shim-deploy.yml

# override build job to add some package dependencies
# and some post-processing of build artifacts

build:
  before_script:
    - !reference [.apt-init]
    - apt-get install imagemagick jdupes findutils webp -y
  after_script:
    # dedupe images using symlinks to reduce artifacts size
    # this setup ensures that page attachments (eg. lead.png)
    # are given higher priority to be replaced by symlinks
    - find public -maxdepth 1 -type d -exec test -e '{}/lead.png' \; -print0 | xargs -0 jdupes --linksoft -X onlyext:jpg,png -O public/static/images/blog public/static/images/blog/inline-images

# required override for LEKTOR_PARTIAL_BUILD
# this tells the partial-build job how to do its thing

.setup-lektor-partial-build:
  - apt-get install -y webp
  - export LEKTOR_BUILD_FLAGS="${LEKTOR_BUILD_FLAGS} partial-build"

# staging-specific post-processing to fix broken URLs

deploy-staging:
  before_script:
    - !reference [.apt-init]
    - !reference [.prepare-deploy-static]
    # fix absolute links in feeds for staging
    - find $SUBDIR -name feed.xml | xargs -d'\n' sed -i 's#https://blog.torproject.org#https://'"${SITE_URL}"'#g'
    # fix discourseEmbedUrl for staging
    - >-
      find $SUBDIR -name index.html -exec grep -q 'discourseEmbedUrl:' '{}' \; -print | \
        while read -d $'\n' f; do
          sed -i '/discourseEmbedUrl:/,/$/s/blog\.torproject\.org/'"${SITE_URL}"'/' "$f";
        done

# override deploy-prod rules to allow
# automatic sheduled deployments to prod for agenda

deploy-prod:
  rules:
    - if: '$L10N_STAGING'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "schedule"'
      when: on_success
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
      when: manual
